<?php

require_once "phing/Task.php";

class RecreateDatabase extends Task
{
	private $dbLogin = null;
	private $dbPass  = null;
	private $dbName  = null;
	private $dbHost  = 'localhost';
	private $userName  = null;
	private $userPass  = null;
	private $dropUser  = null;


	public function setDbLogin($str) { $this->dbLogin = $str; }
	public function setDbPass($str)  { $this->dbPass  = $str; }
	public function setDbName($str)  { $this->dbName  = $str; }
    public function setDbHost($str)  { $this->dbHost  = $str; }

	public function setUserName($str)  { $this->userName  = $str; }
	public function setUserPass($str)  { $this->userPass  = $str; }
	public function setDropUser($str)  { $this->dropUser  = $str; }
	/**
	 *  Recreates a database and user
	 * 
	 */
	public function main()
	{
		
		$db = new  mysqli($this->dbHost,$this->dbLogin,$this->dbPass);
		if (!$db)
		{
		    die('Could not connect: ' . mysqli_error() . "\n");
		}
		$this->log("Connection to $this->dbHost succeeded.");
		
		
		$sql="DROP DATABASE IF EXISTS `".$this->dbName."`";
		$db->query($sql) or die('DROP DATABASE FAILED :'.mysqli_errno($db) . ': ' . mysqli_error($db). "\n");
		$this->log("Database {$this->dbName} dropped.");

		$sql='CREATE DATABASE `'.$this->dbName.'`';
		$db->query($sql) or die('CREATE DATABASE FAILED :'.mysqli_errno($db) . ': ' . mysqli_error($db). "\n");
		$this->log("Database {$this->dbName} created.");
		
		if ($this->dropUser){
			$this->log("Dropping user {$this->userName} if exists.");
			
			$sql='DROP USER \''.$this->userName.'\'@\'%\'';
			if (!$db->query($sql)){
				echo 'Unable to drop user:'.mysqli_errno($db) . ': ' . mysqli_error($db). "\n";
			}

			$sql='DROP USER \''.$this->userName.'\'@\'localhost\'';
			if (!$db->query($sql)){
				$this->log('Unable to drop user:'.mysqli_errno($db) . ': ' . mysqli_error($db), Project::MSG_WARN);
			}
		}
			
		$this->log("Creating user {$this->userName}");
		$sql="GRANT SELECT,INSERT,UPDATE,DELETE,EXECUTE,DROP,CREATE,LOCK TABLES,ALTER ON `".$this->dbName."`.* to '".$this->userName."'@'%' IDENTIFIED BY '".$this->userPass."'";
		$db->query($sql) or die(mysqli_errno($db) . ": " . mysqli_error($db). "\n");

		$sql="GRANT SELECT,INSERT,UPDATE,DELETE,EXECUTE,DROP,CREATE,LOCK TABLES,ALTER ON `".$this->dbName."`.* to '".$this->userName."'@'localhost' IDENTIFIED BY '".$this->userPass."'";
		$db->query($sql) or die(mysqli_errno($db) . ": " . mysqli_error($db). "\n");
		
		$db->close();

	}
}
?>
