<?php

require_once "phing/Task.php";

class TruncateTables extends Task
{
	private $dbLogin = null;
	private $dbPass  = null;
	private $dbName  = null;
	private $dbHost  = null;


	public function setDbLogin($str) { $this->dbLogin = $str; }
	public function setDbPass($str)  { $this->dbPass  = $str; }
	public function setDbName($str)  { $this->dbName  = $str; }
	public function setDbHost($str)  { $this->dbHost  = $str; }

	/**
	 * Truncate some of the messaging tables after a release
	 *
	 */
	public function main()
	{
		
		$db = new  mysqli($this->dbHost,$this->dbLogin,$this->dbPass,$this->dbName);
		if (!$db)
		{
		    die('Could not connect: ' . mysqli_error() . "\n");
		}
		echo "Connection to localhost succeeded.\n";

		$sql='delete from messaging_event_log where occurred < DATE_SUB(NOW(),INTERVAL  1 MONTH)';
		$db->query($sql) or die(mysqli_errno($db) . ': ' . mysqli_error($db). "\n");

		$sql='delete from event_log where occurred < DATE_SUB(NOW(),INTERVAL 1 MONTH) and (action_time IS NULL OR action_time < NOW()); ';
		$db->query($sql) or die(mysqli_errno($db) . ': ' . mysqli_error($db). "\n");

		$sql='delete from messaging_audit where campaign_sent_id in (select campaign_sent_id from messaging_campaign_sent where sent_date<DATE_SUB(NOW(),INTERVAL 3 MONTH))';
		$db->query($sql) or die(mysqli_errno($db) . ': ' . mysqli_error($db). "\n");

		$sql='delete from messaging_campaign_sent where sent_date<DATE_SUB(NOW(),INTERVAL 3 MONTH)';
		$db->query($sql) or die(mysqli_errno($db) . ': ' . mysqli_error($db). "\n");

		$db->close();

	}
}
?>