<?php

require_once "phing/Task.php";
require_once "TestBase.php";

class TestLEP2 extends TestBase
{
	private $lep2Dir = null;
	private $appDir = null;

	public function setAppDir($str) { $this->appDir = $str; }
	public function setLep2Dir($str) { $this->lep2Dir = $str; }
	
	public function main()
	{
        $rootDir=$this->lep2Dir.'/application/modules';
        if ($this->logResults){
			$reportFileIndicator='report_'.date("d_m_y_h");		
			$reportDir=$this->reportDir.$reportFileIndicator;
			if (!is_dir($reportDir)){
				mkdir($reportDir,0777,true);
			}
        }
 
	    if (is_dir($rootDir)){
			$dh = opendir($rootDir);
			while (($file = readdir($dh)) !== false) 
			{
			    if ($file=='overmonitor' && !file_exists($rootDir.'/'.$file.'/config/app.ini'))
			    {
			        //don't process over tests if it hasn't been setup
			        continue;
			    }
			    $testDir=$rootDir.'/'.$file.'/tests';
				if ($file=='.' || $file=='..' || !is_dir($testDir))continue;
				$dh2 = opendir($testDir);
				while (($testFile = readdir($dh2)) !== false) {
				    if (strpos($testFile,'Test.php')===false)continue;
                    //@TODO Selenium test disabled until windows VM is fixed                    
                    if (strpos(file_get_contents($testDir.'/'.$testFile),'Lep_Test_Selenium')!==false
                        && !$this->seleniumServer) continue;                    
				    
				    $this->runTest($testDir,$testFile);
				}
				closedir($dh2);
			}
			closedir($dh);
	    }
	    
		if($this->doEmailTestReports) {
			 $this->emailReport('LEP');
		}
		

					
		
	}
}