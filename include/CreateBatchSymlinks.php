<?php

require_once "phing/Task.php";

class CreateBatchSymlinks extends Task
{
	private $srcDir = null;
	private $targetDir = null;

	public function setSrcDir($str)  { $this->srcDir  = $str; }
	public function setTargetDir($str)  { $this->targetDir  = $str; }
	
	/**
	 *  Creates the mini site symlinks
	 */
	public function main()
	{
		$this->log("Creating symbolic links in $this->targetDir to $this->srcDir/*");
		if ($dh = opendir($this->srcDir)) {
	        while (($dir = readdir($dh)) !== false) {
	        	list(,$site)=explode('_',$dir);
	      		if( (strpos($dir,".")===false) || (strpos($dir,".")>0) ) { // Skip virtual/hidden dirs
	      			$src=$this->srcDir.'/'.$dir;
	      			$target=$this->targetDir.'/'.$dir;
	      			exec("rm -rf {$target}\n");
					exec("ln -s {$src} {$target}\n");
	      		}
	        }
	        closedir($dh);
    	}
		
	}
}
?>