<?php

/** 
 * Task to run the Drupal installation form from phing 
 *   
 * Based on drupal-curl.php written by dennis iversen (http://www.os-cms.net/content/article/view/1)
 */

require_once "phing/Task.php";

class DrupalInstall extends Task
{
	private $profile = 'default'; // installation profile
	private $site_url = 'http://drupal-dev';
	private $locale = 'en';
	private $db_type = 'mysqli';	// database type
	private $db_path = 'drupaldev';	// database
	private $db_user = 'root';	// database user
	private $db_pass = 'pass'; 	// database pass
	private $db_host = 'localhost';	// database host
	
	// These are only set if we're using Drupal 6+
	private $site_name = 'site_name';	// name of site
	private $site_mail = 'site_mail@site.com';	// site email
	private $account_name = 'admin';	// admin account name
	private $account_mail = 'admin@admin.dk';	// admin account email
	private $account_pass = 'admin';	// admin account pass
	private $date_default_timezone = '-39600';	// date time zone
	private $clean_url = '1';	// use clean url.
	private $update_status_module = '1';	// update status module

	public function setSiteUrl($url) {
		$this->site_url = $url;
	}

	public function setProfile($profile) {
		$this->profile = $profile;
	}
	
	public function setLocale($locale) {
		$this->locale = $locale;
	}

	public function setDbName($db) {
		$this->db_path = $db;
	}

	public function setDbHost($host) {
		$this->db_host = $host;
	}

	public function setDbUser($user) {
		$this->db_user = $user;
	}

	public function setDbPass($pass) {
		$this->db_pass = $pass;
	}
	
	public function setAdminName($name) {
		$this->account_name = $name;
	}
	
	public function setAdminMail($email) {
		$this->account_mail = $email;
	}
	
	public function setAdminPass($pass) {
		$this->account_pass = $pass;
	}

	public function main() {
		// create a url for curling db settings
		$post_vars.= "db_type=$this->db_type&";
		$post_vars = "db_path=$this->db_path&";
		$post_vars.= "db_user=$this->db_user&";
		$post_vars.= "db_pass=$this->db_pass&";
		$post_vars.= "db_host=$this->db_host&";
		$post_vars.= "db_prefix=&";
		$post_vars.= "db_port=&";
		$post_vars.= "op=" .urlencode("Save and continue") . "&";
		$post_vars.= "form_id=install_settings_form";
		
		$url = "$this->site_url/install.php?profile=$this->profile&locale=$this->locale";

		
		$this->log("Installing drupal site at $this->site_url using $this->profile profile");
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vars);
		$html = curl_exec ($ch); // execute the curl command
		curl_close ($ch);
		unset($ch);


		// we need to do the same address again because cookie
		// is only set second time, then we get cookie and move on.

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_COOKIEJAR, "./cookieFileName");
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vars);
		$html = curl_exec ($ch); // execute the curl command
		curl_close ($ch);
		unset($ch);
		//echo $html;

		// install modules part one 80 %
		$this->log('Installing base modules');
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, "./cookieFileName");
		curl_setopt($ch, CURLOPT_URL,"$url&op=do_nojs&id=1");
		$html = curl_exec ($ch); // execute the curl command
		curl_close ($ch);
		unset($ch);
		//echo $html;

		// install last 20 %
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, "./cookieFileName");
		curl_setopt($ch, CURLOPT_URL,"$url&op=do_nojs&id=1");
		$html = curl_exec ($ch); // execute the curl command
		curl_close ($ch);
		unset($ch);
		//echo $html;
		
		// confirm finished
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, "./cookieFileName");
		curl_setopt($ch, CURLOPT_URL,"$url&op=finished&id=1");
		$html = curl_exec ($ch); // execute the curl command
		curl_close ($ch);
		unset($ch);
		//echo $html;

		$this->log('Configuring base site');
		
		// set settings for loading database with site base settings
		$post_vars = "site_name=$this->site_name&";
		$post_vars.= "site_mail=$this->site_mail&";
		$post_vars.= "account[name]=$this->account_name&";
		$post_vars.= "account[mail]=$this->account_mail&";
		$post_vars.= "account[pass][pass1]=$this->account_pass&";
		$post_vars.= "account[pass][pass2]=$this->account_pass&";
		$post_vars.= "date_default_timezone=$this->date_default_timezone&";
		$post_vars.= "clean_url=$this->clean_url&";
		$post_vars.= "form_id=install_configure_form&";
		$post_vars.= "update_status_module[1]=$this->update_status_module";


		// enter these settings in a last curl session
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, "./cookieFileName");
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vars);

		$html = curl_exec ($ch);
		curl_close ($ch);
		//echo $html;
	}
}