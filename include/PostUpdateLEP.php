<?php

require_once "phing/Task.php";

/**
 * Performs cleanup task post LEP release, such as cleaning up cache files
 * , flushing memcache, truncating tables etc.
 *
 */

class PostUpdateLEP extends Task
{
	private $coreUrl = null;
	private $cacheBackend = null;
	private $memcachedHost = null;
	private $memcachedPort = null;
	private $lepBase = null;
	

	public function setCoreUrl($str) { $this->coreUrl = $str; }
	public function setCacheBackend($str) { $this->cacheBackend = $str; }
	public function setMemcachedHost($str) { $this->memcachedHost = $str; }
	public function setMemcachedPort($str) { $this->memcachedPort = $str; }
	public function setLepBase($str) { $this->lepBase = $str; }
	/**
	 */
	public function main()
    {
        $lep2Url=$this->coreUrl;
        $services = array('base','admin','consumer','channel','bucket');
        //clear WSDL cache and service map caches
        //Set the TTL to 1 to force the "refresh" of the WSDL cache
        ini_set('soap.wsdl_cache_ttl', 1);
        foreach ($services as $service)
        {
            $endPoint = "{$lep2Url}/api/{$service}/?resetServiceMap=1";
            if (!@file_get_contents($endPoint))continue;
            try{
                $client = new SoapClient($lep2Url.'/api/'.$service.'/?wsdl');    
            }
            catch (Exception $e)
            {
                echo $e->getMessage()."\n";
            }
        }
	    
	    if ($this->cacheBackend=='Memcached') {
	       //Flush memcached
	       if ($memcacheObj = @memcache_connect($this->memcachedHost, $this->memcachedPort)) {
		      memcache_flush($memcacheObj);  	
	       }
	    }
	    
	    //Send a test job to gearman so that workers terminate, thus
	    //ensuring that they are running the latest code
		/*if (class_exists('GearmanClient')) {
			try {
				$client = new GearmanClient();
				$client->addServer();
				$client->doBackground('test', serialize(array('name' => 'Dave')));
			}
			catch (Exception $e) {
				echo $e->getMessage()."\n";
			}
	    }*/
		
	}
}
