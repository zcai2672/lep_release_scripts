<?php
/**
 * Run drush as a phing task 
 */

require_once "phing/Task.php";

class Drush extends Task
{
	private $root;
	private $site = 'default';
	private $command;
	private $args;
	
	public function setCommand($cmd) {
		$this->command = $cmd;
	}
	
	public function setRoot($root) {
		$this->root = $root;
	}
	
	public function setSite($site) {
		$this->site = $site;
	}
	
	public function setArgs($args) {
		$this->args = $args;
	}
	
	
	public function init() {

	}
	
	public function main() {
		if(!$this->root)
			throw new ConfigurationException('Must set drupal root');
		
		$return = 0;
		passthru("php drush/drush.php --root=$this->root --uri=$this->site $this->command $this->args", $return);
		if($return != 0)
			throw new BuildException('drush encountered an error'); 
	}
}