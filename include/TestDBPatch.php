<?php

require_once "phing/Task.php";
class TestDBPatch extends Task
{
	private $db = null;
	private $identifier  = null;
	private $user  = null;
	private $password  = null;
	private $tmpDir  = null;

	public function setDb($str) { $this->db = $str; }
	public function setUser($str) { $this->user = $str; }
	public function setPassword($str) { $this->password = $str; }
	public function setIdentifier($str)  { $this->identifier  = $str; }
	public function setTmpDir($str)  { $this->tmpDir  = $str; }
	/**
	 *  1)Test whether the patch files are valid
	 */
	public function main()
	{
		if (!$this->tmpDir){
			die('tmpDir not specified');
		}

		exec("mysql -v -u{$this->user} -p{$this->password} --force  {$this->db} < {$this->tmpDir}/{$this->identifier}-clean.sql");
		
		//redirect the standard error (stderr) to the standard output (stdout)
		exec("mysql -v -u{$this->user} -p{$this->password}  {$this->db} < {$this->tmpDir}/{$this->identifier}.sql 2>&1", $output, $err); 
		if ($err && count($output)){
			$devMail='development-team@lifeevents.com.au';
			//$devMail='julianb@lifeevents.com.au';
			//the patch failed
			$output	= print_r($output,1);
			$message=<<<MSG
This is an email from the automated test scripts.
The following patch file failed and requires immediate attention: "{$this->identifier}.sql" ({$this->db})

This is usually due to
	1) Invalid sql in the patch file.
	2) Statements are either missing from the "clean" file or are invalid.

Output from the test follows 
	
$output	

MSG;
			echo "Error!\n";
			mail($devMail,'Database Patch Failure',$message,"From: test-results@lifeevents.com.au\r\n");

		}else{
			echo "No Error";
		}

		
		exec("rm -f {$this->tmpDir}/{$this->identifier}-clean.sql");
		exec("rm -f {$this->tmpDir}/{$this->identifier}.sql");
	}
}
?>
