<?php

require_once "phing/Task.php";

class TestBase extends Task
{
	protected $reposURL = null;
	protected $error=false;
	protected $errorMsg='';
	protected $failures=0;
	protected $errors=0;
	protected $tests=0;
	protected $lastFewChanges=0;
	protected $doEmailTestReports=false;
	protected $emailReportTo="system-support@lifeevents.com.au";	// Where test reports get sent
	protected $emailFailsTo="development-team@lifeevents.com.au";
	protected $logResults=true;
	protected $reportDir="/opt/web/report/";
	protected $seleniumServer;

	public function setReposURL($str) { $this->reposURL = $str; }
	public function setDoEmailTestReports($str) { if($str) { $this->doEmailTestReports = true; } }
	public function setEmailReportTo($str) { $this->emailReportTo = $str; }
	public function setEmailFailsTo($str) { $this->emailFailsTo = $str; }
	public function setLogResults($str) {  $this->logResults = $str; }
	public function setReportDir($str) { $this->reportDir = $str; }
	public function setSeleniumServer($str) { $this->seleniumServer = $str; }
	
	

	function main(){}
	
	protected function runTest($testDir,$testFile)
	{
        list($class,)=explode('.',$testFile);
		if ($this->logResults){
	      $ccaDir=$reportDir.'/cca/'.$class;
		  if (!is_dir($ccaDir)){
			mkdir($ccaDir,0777,true);
		  }
		}
		//redirect the standard error (stderr) to the standard output (stdout)
        //$str="phpunit --verbose --log-xml $reportDir/{$class}.xml --coverage-html $ccaDir $class $testDir/$testFile 2>&1 ";
        echo "Running $testFile\n";
        /*if ($this->logResults){
            $str="phpunit --verbose --log-xml $reportDir/{$class}.xml  $class $testDir/$testFile 2>&1 ";                 	
        }else{
         	$str="phpunit --verbose $class $testDir/$testFile 2>&1 ";                 	
        }*/
        $str="phpunit --verbose $class $testDir/$testFile 2>&1 ";
	    exec($str, $output, $err);
	    var_dump($output, $err,$str);
                    
	    $str=implode("\n",$output);
	               
	    $pattern='/Tests:([^`]*?),|OK \(([^`]*?) test(s)?,/';
	    preg_match_all($pattern, $str, $matches);
		if (trim($matches[1][0])!='' || trim($matches[2][0])!=''){
		  $this->tests+=(trim($matches[1][0])?(int)trim($matches[1][0]):(int)trim($matches[2][0]));
		}

		if ($err){ 
			$this->error=true;	
			preg_match_all('/Failures:([^`]*?)\./', $str, $matches);
			if ($matches[1][0]){
				$this->failures+=(int)trim($matches[1][0]);
			}
			preg_match_all('/Errors:([^`]*?)\./', $str, $matches);
			if ($matches[1][0]){
				$this->errors+=(int)trim($matches[1][0]);
			}
			$this->errorMsg.='Error Code:'.$err."\n";
			$this->errorMsg.=$str;
		}
		unset($output);	                
	    
	}
	
    protected function emailReport($type)
    {	
		$devMail=$this->emailReportTo;
		$msgSubject = "Automated Test Report ($type)";
		if($this->error) {
			$devMail.=', '.$this->emailFailsTo;
			$msgSubject .= " - FAILURE!";
		}
		exec("svn log $this->reposURL -v | head -100",$changes);
		$this->lastFewChanges.=implode("\n",$changes);
		unset($changes);
		
			
$message=<<<MSG
This is an email from the automated test scripts.

Total Tests: $this->tests
Total Failures: $this->failures
Total Errors: $this->errors

Details on errors on the staging server can be found at http://report.staging/

Results of failed tests and list of recent changes to repository follows:

==================================================================

$this->errorMsg

==================================================================

$this->lastFewChanges

MSG;
		mail($devMail,$msgSubject,$message,"From: code-changes@lifeevents.com.au\r\n");
    }
}
