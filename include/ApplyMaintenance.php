
<?php

require_once "phing/Task.php";

class ApplyMaintenance extends Task
{
	private $apply = null;
	private $dbLogin = null;
	private $dbPass  = null;
	private $dbName  = null;
	private $dbHost  = null;


	public function setApply($str) { $this->apply = $str; }
	public function setDbLogin($str) { $this->dbLogin = $str; }
	public function setDbPass($str)  { $this->dbPass  = $str; }
	public function setDbName($str)  { $this->dbName  = $str; }
	public function setDbHost($str)  { $this->dbHost  = $str; }

	/**
	 * Turns on/off the Drupal site offline switch
	 *
	 */
	public function main()
	{

    	$db=new mysqli($this->dbHost,$this->dbLogin,$this->dbPass,$this->dbName);
    	$sql='show tables';
    	$res=$db->query($sql);
    	while ($row=$res->fetch_row()){
    		$table=$row[0];
    		if (strpos($table,'_cache')!==false){
    			$sql='delete from '.$table;
    			var_dump($db->query($sql));
    		}
    				
    		if (strpos($table,'_variable')!==false){
    		    var_dump("Updating $table");
    		    
    			$sql='DELETE FROM '.$table.' WHERE name ="site_offline"';
    			var_dump($db->query($sql));
    
    			$sql='INSERT INTO '.$table.' (name, value) values ("site_offline", "s:1:\"'.($this->apply?1:0).'\";")';
    			var_dump($db->query($sql));
    
    			if ($this->apply){
        			$sql='DELETE FROM '.$table.' WHERE name ="site_offline_message"';
        			var_dump($db->query($sql));
    
    	   		    $sql='INSERT INTO '.$table.' (name, value) values ("site_offline_message", "s:129:\"<h1>Sorry, Quotify.com is out of action for a few minutes while we make our site even better.</h1><h1>We\'ll be back shortly!</h1>\";")';
    			    var_dump($db->query($sql));
    			}
    		}
    	}
	    
	}
}
?>