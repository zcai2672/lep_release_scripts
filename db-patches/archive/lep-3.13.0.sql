ALTER TABLE provider ADD `disable_manual_leads` tinyint(4) DEFAULT '0'  COMMENT 'Set to 1 to prevent provider from getting flicked leads' AFTER `cap_reason`;
ALTER TABLE provider_attribute ADD  KEY `name` (`name`);
CREATE TABLE `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_id` int(11) NOT NULL,
  `key_type` varchar(20) NOT NULL,
  `note` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `key_id` (`key_id`),
  KEY `key_type` (`key_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Avoids performance problems wrt base table notes';
