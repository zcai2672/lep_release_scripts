#Create ACL resources for new service methods
INSERT INTO acl_resource VALUES (112,'DeleteCapRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,112,1,NOW());
INSERT INTO acl_resource VALUES (113,'SaveCapRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,113,1,NOW());
INSERT INTO acl_resource VALUES (114,'GetCapsRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,114,1,NOW());
INSERT INTO acl_resource VALUES (115,'GetHoldDatesRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,115,1,NOW());
INSERT INTO acl_resource VALUES (116,'SaveServiceAreaRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,116,1,NOW());
INSERT INTO acl_resource VALUES (117,'GetServiceAreasRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,117,1,NOW());
INSERT INTO acl_resource VALUES (118,'GetCampaignProfilesRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,118,1,NOW());
INSERT INTO acl_resource VALUES (119,'SaveCampaignProfileRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,119,1,NOW());
INSERT INTO acl_resource VALUES (120,'DeleteCampaignProfileRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,120,1,NOW());
INSERT INTO acl_resource VALUES (121,'GetCampaignRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,121,1,NOW());
INSERT INTO acl_resource VALUES (122,'SaveCampaignRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,122,1,NOW());
INSERT INTO acl_resource VALUES (123,'GetCampaignsRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,123,1,NOW());
INSERT INTO acl_resource VALUES (124,'SaveCampaignContactMediaRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,124,1,NOW());
INSERT INTO acl_resource VALUES (125,'SaveMessageDeliveryOptionsRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,125,1,NOW());
INSERT INTO acl_resource VALUES (126,'SaveContactRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,126,1,NOW());
INSERT INTO acl_resource VALUES (127,'GetContactRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,127,1,NOW());
INSERT INTO acl_resource VALUES (128,'GetContactsRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,128,1,NOW());
INSERT INTO acl_resource VALUES (129,'SaveCampaignProfilesRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,129,1,NOW());
INSERT INTO acl_resource VALUES (130,'DuplicateBillingGroupRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,130,1,NOW());
INSERT INTO acl_resource VALUES (131,'VerifyCampaignLeadStatusRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,131,1,NOW());
INSERT INTO acl_resource VALUES (132,'DuplicateCampaignRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,132,1,NOW());
INSERT INTO acl_resource VALUES (133,'SaveProviderAttributeRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,133,1,NOW());
INSERT INTO acl_resource VALUES (134,'SaveChannelWidgetRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,134,1,NOW());
INSERT INTO acl_resource VALUES (135,'GetChannelWidgetRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,135,1,NOW());
INSERT INTO acl_resource VALUES (136,'SaveLeadPlanRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,136,1,NOW());
INSERT INTO acl_resource VALUES (137,'GetPackagesRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,137,1,NOW());
INSERT INTO acl_resource VALUES (138,'GetLeadPlansRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,138,1,NOW());
INSERT INTO acl_resource VALUES (139,'CancelLeadPlanRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,139,1,NOW());
INSERT INTO acl_resource VALUES (140,'ActivateLeadPlanRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,140,1,NOW());
INSERT INTO acl_resource VALUES (141,'GetCarriersRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,141,1,NOW());
UPDATE acl_resource SET name='SaveHoldDatesRQ' WHERE name='AddHoldDatesRQ';
UPDATE acl_resource SET name='DeleteHoldDatesRQ' WHERE name='RemoveHoldDatesRQ';
DELETE FROM acl_role_permission WHERE acl_resource_id = 28;
DELETE FROM acl_resource WHERE id = 28;

CREATE TABLE `affiliate` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_affiliate_client_id` (`client_id`),
  CONSTRAINT `fk_affiliate_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB CHARSET=latin1;

CREATE TABLE `lead_plan` (
  `id` int(11) NOT NULL auto_increment,
  `package_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `activation_date` timestamp NULL default NULL,
  `expiry_date` timestamp NULL default NULL,
  `status` enum('pending','expired','cancelled','active') NOT NULL default 'pending',
  `auto_renew` tinyint(1) NOT NULL default '1',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Holds lead plan subscriptions';

CREATE TABLE `campaign` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `vertical_id` int(11) NOT NULL,
  `status` enum('inactive','pending','deleted','active') NOT NULL default 'pending',
  `is_held` tinyint(4) NOT NULL default '0',
  `provider_id` int(11) NOT NULL,
  `contact_id` int(11) default NULL,
  `lead_plan_id` int(11) default NULL COMMENT 'Indicates campaign''s current lead plan. No lead plan means a la carte.',
  `created_date` timestamp NULL default CURRENT_TIMESTAMP,
  `campaign_key` varchar(32) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_campaign_provider_id` (`provider_id`),
  KEY `fk_campaign_vertical_id` (`vertical_id`),
  KEY `fk_campaign_contact_id` (`contact_id`),
  KEY `fk_campaign_lead_plan_id` (`lead_plan_id`),
  CONSTRAINT `fk_campaign_lead_plan_id` FOREIGN KEY (`lead_plan_id`) REFERENCES `lead_plan` (`id`),
  CONSTRAINT `fk_campaign_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`),
  CONSTRAINT `fk_campaign_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_campaign_vertical_id` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `package` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `lead_count` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `client_id` int(11) NOT NULL,
  `vertical_id` int(11) default NULL COMMENT 'No vertical defined then package applies to all verticals',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_package_client_id` (`client_id`),
  KEY `fk_package_vertical_id` (`vertical_id`),
  CONSTRAINT `fk_package_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_package_vertical_id` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains all packages in the system';

#Create campaigns for all active providers
INSERT INTO campaign (name,is_held,provider_id,status,vertical_id)
SELECT v.name,0,p.id,'active',pv.vertical_id
FROM provider p
JOIN provider_vertical pv ON pv.provider_id=p.id
JOIN vertical v ON pv.vertical_id=v.id;

#assign lead contacts to newly created campaigns
UPDATE campaign,provider_contact,contact
SET campaign.contact_id=contact.id
WHERE  campaign.provider_id=provider_contact.provider_id
AND provider_contact.contact_id=contact.id
AND contact.type='lead' AND (campaign.contact_id IS NULL OR campaign.contact_id = 0);

#if there are no lead contacts the assign primary contacts
UPDATE campaign,provider_contact,contact
SET campaign.contact_id=contact.id
WHERE  campaign.provider_id=provider_contact.provider_id
AND provider_contact.contact_id=contact.id
AND contact.type='primary' AND (campaign.contact_id IS NULL OR campaign.contact_id = 0);

CREATE TABLE `campaign_profile` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `price` decimal(10,2) default NULL,
  `price_type` enum('%','$') default NULL,
  `preference` decimal(10,2) default 0,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COMMENT='Stores campaign lead prices & profile information';

#Insert campaign profile (options)
INSERT INTO campaign_profile (campaign_id,question_id,option_id,price,price_type,preference)
SELECT campaign.id,provider_question.question_id,provider_question.option_id,
NULL,NULL,provider_question.preference
FROM campaign
JOIN provider_question ON provider_question.provider_id=campaign.provider_id;

#Insert campaign profile (lead prices)
UPDATE campaign_profile,provider_lead_price,campaign
SET campaign_profile.price=provider_lead_price.amount ,campaign_profile.price_type=provider_lead_price.type
WHERE  campaign.provider_id=provider_lead_price.provider_id
AND campaign_profile.campaign_id=campaign.id
AND campaign_profile.question_id=provider_lead_price.question_id
AND campaign_profile.option_id=provider_lead_price.option_id;

ALTER TABLE `campaign_profile` MODIFY `id` int(11) NOT NULL auto_increment,
  ADD PRIMARY KEY  (`id`),
  ADD CONSTRAINT `campaign_profile_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  ADD CONSTRAINT `campaign_profile_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`),
  ADD CONSTRAINT `campaign_profile_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`);

CREATE TABLE `campaign_contact_media` (
  `id` int(11) NOT NULL auto_increment,
  `contact_media_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `contact_media_contact_media_id_fk` (`contact_media_id`),
  KEY `campaign_campaign_id_fk` (`campaign_id`),
  CONSTRAINT `contact_media_contact_media_id_fk` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `campaign_campaign_id_fk` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

RENAME TABLE provider_cap TO cap;
ALTER TABLE cap ADD campaign_id INT(11) NULL;
ALTER TABLE cap ADD CONSTRAINT `fk_cap_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
CREATE UNIQUE INDEX uk_cap1 ON cap (provider_id,campaign_id);
DROP INDEX provider_id ON cap;

#Assign the caps to the newly created campaign
UPDATE cap 
JOIN campaign ON cap.provider_id=campaign.provider_id 
LEFT JOIN (SELECT provider_id FROM provider_vertical GROUP BY provider_id HAVING COUNT(vertical_id)>1) r ON r.provider_id=campaign.provider_id 
SET cap.campaign_id=campaign.id
WHERE r.provider_id IS NULL;

RENAME TABLE provider_onhold TO onhold;
ALTER TABLE onhold ADD campaign_id INT(11) NULL;
ALTER TABLE onhold ADD CONSTRAINT `fk_onhold_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

#Copy over service areas and assign it to a campaign
CREATE TABLE `service_area` (
  `id` INT(11) NOT NULL,
  `provider_id` INT(11) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `question_id` INT(11) default NULL,
  `campaign_id` INT(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO service_area(provider_id,postcode,question_id,created_date,campaign_id)
SELECT psa.provider_id,psa.postcode,psa.question_id
    ,psa.created_date,c.id FROM provider_service_area psa
    JOIN campaign c ON c.provider_id=psa.provider_id;
    
#very slow    
ALTER TABLE `service_area` MODIFY `id` int(11) NOT NULL auto_increment,
  ADD PRIMARY KEY  (`id`),
  ADD UNIQUE KEY `idx_unique_location` (`provider_id`,`campaign_id`,`question_id`,`postcode`),
  ADD KEY `fk_service_area_provider_id` (`provider_id`),
  ADD KEY `fk_service_area_question_id` (`question_id`),
  ADD KEY `idx_postcode` (`postcode`),
  ADD KEY `provider_id_question_id` (`provider_id`,`question_id`),
  ADD KEY `fk_service_area_campaign_id` (`campaign_id`),
  ADD CONSTRAINT `fk_service_area_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_service_area_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_service_area_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;     

RENAME TABLE provider_message_delivery TO message_delivery_option;
ALTER TABLE message_delivery_option ADD campaign_id INT(11) NULL;
ALTER TABLE message_delivery_option ADD CONSTRAINT `fk_message_delivery_option_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

#Assign the delivery option to the newly created campaign
UPDATE message_delivery_option,campaign SET message_delivery_option.campaign_id=campaign.id
WHERE message_delivery_option.provider_id=campaign.provider_id;
ALTER TABLE message_delivery_option DROP FOREIGN KEY fk_provider_message_delivery_provider;
ALTER TABLE message_delivery_option DROP provider_id;

#Perform match set operations
CREATE TABLE `match_set_tmp` engine=innodb SELECT * FROM match_set;
ALTER TABLE `match_set_tmp` ADD campaign_id INT(11) NOT NULL AFTER job_spec_id,ADD lead_plan_id INT(11) NULL AFTER campaign_id;

#Set the campaign id in the match set (according to vertical)
UPDATE match_set_tmp,campaign,job_spec SET match_set_tmp.campaign_id=campaign.id
WHERE match_set_tmp.campaign_id = 0 
AND match_set_tmp.provider_id=campaign.provider_id AND match_set_tmp.job_spec_id=job_spec.id
AND campaign.vertical_id=job_spec.vertical_id;

#Set the campaign id in the match set (for nonexistent vertical)
UPDATE match_set_tmp,campaign SET match_set_tmp.campaign_id=campaign.id
WHERE match_set_tmp.campaign_id = 0 
AND match_set_tmp.provider_id=campaign.provider_id;

#slow
ALTER TABLE `match_set_tmp` ADD CONSTRAINT `fk_match_set_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    ADD CONSTRAINT `fk_match_set_lead_plan_id` FOREIGN KEY (`lead_plan_id`) REFERENCES `lead_plan` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    ADD CONSTRAINT `fk_match_set_job_spec_id` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    ADD CONSTRAINT `fk_match_set_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    ADD KEY `idx_created_date` (`created_date`),
    ADD KEY `idx_lead_delay` (`lead_delay`),
    MODIFY `id` int(11) NOT NULL auto_increment,
    ADD PRIMARY KEY  (`id`);
   
RENAME TABLE match_set TO match_set_old;
RENAME TABLE match_set_tmp TO match_set;

#Copy over 'unmatched' data
CREATE TABLE `unmatched_campaign` engine=innodb SELECT * FROM unmatched_provider;
ALTER TABLE `unmatched_campaign` MODIFY `id` int(11) NOT NULL auto_increment,
                ADD `campaign_id` INT(11) NULL AFTER job_spec_id,
                ADD CONSTRAINT `fk_unmatched_campaign_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
                ADD CONSTRAINT `fk_unmatched_campaign_job_spec_id` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
                ADD CONSTRAINT `fk_unmatched_campaign_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
                ADD PRIMARY KEY  (`id`),
                COMMENT = 'Holds all unmatched campaigns for a job spec.';

ALTER TABLE provider_charge ADD campaign_id INT(11) NULL AFTER provider_id,
    ADD invoiced TINYINT(1) NOT NULL default 0,
    ADD CONSTRAINT `fk_provider_charge_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    MODIFY `type` enum('lead','lead-credit','lead-plan','misc','misc-credit','subscription','subscription-credit','lead-estimate','lead-credit-estimate') NOT NULL,
    MODIFY `key_type` enum('job-spec','lead-plan') default NULL;

ALTER TABLE transaction ADD campaign_id INT(11) NULL AFTER provider_id,
    ADD CONSTRAINT `fk_transaction_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE backfill ADD campaign_id INT(11) NULL AFTER provider_id,
    ADD CONSTRAINT `fk_backfill_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

UPDATE ranking_rule SET name='Lep_Rank_CampaignPreference' WHERE name='Lep_Rank_ProviderPreference';
ALTER TABLE question MODIFY `type` enum('checkbox','date','display','location','location-specific','password','radio','rating','select','slider','text','textarea') NOT NULL;
ALTER TABLE channel MODIFY `status` enum('active','inactive','staging','template') DEFAULT 'staging' NOT NULL;
ALTER TABLE system_message_event ADD KEY `idx_name` (`name`);

ALTER TABLE question ADD KEY `idx_label` (`label`),
                     ADD KEY `idx_status` (`status`),
                     ADD KEY `idx_type` (`type`),
                     ADD KEY `idx_created_date` (`created_date`);
                     
ALTER TABLE vertical ADD KEY `idx_status` (`status`);

ALTER TABLE provider ADD KEY `idx_status` (`status`),
                     ADD `affiliate_id` INT(11) NULL,
                     ADD CONSTRAINT `fk_provider_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
                     ADD KEY `idx_created_date` (`created_date`),
                     ADD KEY `idx_modified_date` (`modified_date`),
                     ADD `client_id` int(11) NULL,
                     ADD CONSTRAINT `fk_provider_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
UPDATE provider SET client_id=1;
ALTER TABLE provider CHANGE `client_id` `client_id` int(11) NOT NULL;

ALTER TABLE profile_group ADD KEY `idx_status` (`status`),
                          ADD KEY `idx_name` (`name`),
                          ADD KEY `idx_created_date` (`created_date`);

ALTER TABLE consumer_request ADD KEY `idx_created_date` (`created_date`),
                             ADD `client_id` int(11) NULL,
                             ADD CONSTRAINT `fk_consumer_request_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
                             ADD `modified_date` TIMESTAMP COMMENT 'Assumed to be date final state was reached' AFTER created_date,
                             ADD KEY `idx_modified_date` (`modified_date`),
                             COMMENT = 'Once a consumer request reaches a final state of either active or inactive, it must not be modified any further.';
UPDATE consumer_request SET client_id=1;
ALTER TABLE consumer_request CHANGE `client_id` `client_id` int(11) NOT NULL;
                             
ALTER TABLE vertical ADD KEY `idx_created_date` (`created_date`);


ALTER TABLE channel ADD KEY `idx_created_date` (`created_date`);

ALTER TABLE message_template ADD KEY `idx_created_date` (`created_date`);

ALTER TABLE message_event ADD KEY `idx_status` (`status`),
                          ADD KEY `idx_created_date` (`created_date`),
                          ADD KEY `idx_trigger_date` (`trigger_date`),
                          ADD `client_id` int(11) NULL,
                          ADD CONSTRAINT `fk_message_event_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
UPDATE message_event SET client_id=1;
ALTER TABLE message_event CHANGE `client_id` `client_id` int(11) NOT NULL;

                     
#Add/Update Client Ids
ALTER TABLE billing_group ADD `client_id` int(11) NOT NULL;
UPDATE billing_group SET client_id=1;
ALTER TABLE billing_group ADD CONSTRAINT `fk_billing_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE channel ADD `client_id` int(11) NOT NULL;
UPDATE channel SET client_id=1;
ALTER TABLE channel ADD CONSTRAINT `fk_channel_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE channel_group ADD `client_id` int(11) NOT NULL;
UPDATE channel_group SET client_id=1;
ALTER TABLE channel_group ADD CONSTRAINT `fk_channel_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE consumer ADD `client_id` int(11) NOT NULL;
UPDATE consumer SET client_id=1;
ALTER TABLE consumer ADD CONSTRAINT `fk_consumer_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE project ADD `client_id` int(11) NOT NULL;
UPDATE project SET client_id=1;
ALTER TABLE project ADD CONSTRAINT `fk_project_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE job_spec ADD `client_id` int(11) NOT NULL;
UPDATE job_spec SET client_id=1;
ALTER TABLE job_spec ADD CONSTRAINT `fk_job_spec_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE message_template ADD `client_id` int(11) NOT NULL;
UPDATE message_template SET client_id=1;
ALTER TABLE message_template ADD CONSTRAINT `fk_message_template_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE message_template_group ADD `client_id` int(11) NOT NULL;
UPDATE message_template_group SET client_id=1;
ALTER TABLE message_template_group ADD CONSTRAINT `fk_message_template_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE profile_group ADD `client_id` int(11) NOT NULL;
UPDATE profile_group SET client_id=1;
ALTER TABLE profile_group ADD CONSTRAINT `fk_profile_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE question ADD `client_id` int(11) NOT NULL;
UPDATE question SET client_id=1;
ALTER TABLE question ADD CONSTRAINT `fk_question_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE remarket ADD `client_id` int(11) NOT NULL;
UPDATE remarket SET client_id=1;
ALTER TABLE remarket ADD CONSTRAINT `fk_remarket_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE tag ADD `client_id` int(11) NULL;
UPDATE tag SET client_id=1;
ALTER TABLE tag ADD CONSTRAINT `fk_tag_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE vertical ADD `client_id` int(11) NULL;
UPDATE vertical SET client_id=1;
ALTER TABLE vertical ADD CONSTRAINT `fk_vertical_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE queue ADD `client_id` int(11) NULL;
UPDATE queue SET client_id=1;
ALTER TABLE queue ADD CONSTRAINT `fk_queue_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE `admin_user_provider` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

ALTER TABLE `user` ADD `status` enum('inactive','active') NOT NULL default 'active';
UPDATE `user` SET `status` = 'active';


ALTER TABLE provider ADD tmp_orig_status VARCHAR(20) NOT NULL;
UPDATE provider SET tmp_orig_status=status;
ALTER TABLE provider ADD type ENUM('provider','backfill','prospect') NOT NULL DEFAULT 'provider' AFTER status;
UPDATE provider SET type='backfill' WHERE status='backfill';
UPDATE provider SET type='prospect' WHERE status='prospect';
UPDATE provider SET status='active' WHERE status IN ('backfill','prospect');

INSERT IGNORE INTO ranking_rule (id,name,description) 
VALUES (10,'Lep_Rank_CapLevel','Rank providers based on their current caps and lead counts. The more restrictive the cap, the lower they are ranked.');
INSERT IGNORE INTO ranking_rule (id,name,description) 
VALUES (11,'Lep_Rank_FreeLeads','Rank providers based on whether they have free leads or not. This is a simple binary ranking algorithm.');


CREATE TABLE `provider_attribute` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `provider_id` (`provider_id`),
  CONSTRAINT `provider_attribute_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Holds client custom attributes of a provider';

ALTER TABLE channel ADD `template_channel_id` int(11) NULL;
ALTER TABLE channel ADD KEY `idx_template_channel_id` (`template_channel_id`);

#New metadata functionality
CREATE TABLE IF NOT EXISTS `metadata` (
  `id` int(11) NOT NULL auto_increment,
  `model_id` int(11) NOT NULL,
  `model` varchar(100) NOT NULL,
  `type` enum('force_pending_reason','force_pending','remote_ip','key1','key2','remote_referrer') NOT NULL,
  `data` text NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `metadata_client_id` (`client_id`),
  KEY `metadata_model_id` (`model_id`),
  KEY `metadata_model` (`model`),
  KEY `metadata_type` (`type`),
  CONSTRAINT `metadata_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10235 DEFAULT CHARSET=latin1;

UPDATE ranking_rule SET name = 'Lep_Rank_TimeLastMatch', description = 'Rank providers based on last time they were matched' WHERE id = 1;

CREATE TABLE `channel_widget` (
  `id` int(11) NOT NULL auto_increment,
  `channel_id` int(11) NOT NULL,
  `layout` varchar(50) NOT NULL,
  `default_question_id` int(11) default NULL,
  `type` enum('lightbox','inline') NOT NULL default 'inline' COMMENT 'Widget invocation type',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `channel_widget_channel_id` (`channel_id`),
  KEY `channel_widget_default_question_id` (`default_question_id`),
  CONSTRAINT `channel_widget_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `channel_widget_default_question_id` FOREIGN KEY (`default_question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;


ALTER TABLE lead_credit_request CHANGE `modified_date` `modified_date` timestamp NULL COMMENT 'Assumed to be date final state was reached',
                COMMENT = 'Once a credit request reaches a final state of either Approved or Declined, it must not be modified any further.';

INSERT IGNORE INTO `registration_filter` VALUES (7, 'Lep_RegistrationFilter_ConsumerIpAddress', 'Checks for duplicate registrations using the same IP address', '2011-1-14 19:27:41');
INSERT IGNORE INTO `registration_filter` VALUES (8, 'Lep_RegistrationFilter_ForceToPending', 'Forces a registration to pending depending on the ForcePending flag', '2011-1-14 19:27:41');
ALTER TABLE client ADD currency_code CHAR(3) NOT NULL COMMENT 'Currency used by the client and its children' AFTER message_template_group_id;
ALTER TABLE client ADD country VARCHAR(100) NOT NULL COMMENT 'Country of client and its children' AFTER message_template_group_id;

ALTER TABLE `user` ADD `affiliate_id` INT(11) NULL AFTER status;
ALTER TABLE `user` ADD CONSTRAINT `fk_user_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE `carrier` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `address_domain` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1 COMMENT='List of mobile carriers';

ALTER TABLE `contact_media` ADD `carrier_id` INT(11) NULL AFTER `type`,
                        ADD CONSTRAINT `fk_contact_media_carrier_id` FOREIGN KEY (`carrier_id`) REFERENCES `carrier` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE package ADD `description` varchar(500) default NULL AFTER `name`;
ALTER TABLE package ADD `weight` int(11) NOT NULL default '0' AFTER client_id;

#Update Location schema and data
ALTER TABLE location
	MODIFY name varchar(255) NOT NULL AFTER id,
	MODIFY alt_label varchar(100) DEFAULT NULL COMMENT 'An alternative name for the location' AFTER label,
	MODIFY latitude double DEFAULT NULL,
	MODIFY longitude double DEFAULT NULL,
	MODIFY postcode varchar(6) DEFAULT NULL AFTER alt_label,
	MODIFY `level` enum('suburb', 'postcode', 'region', 'metro', 'regional', 'state', 'country') DEFAULT NULL AFTER `type`;

UPDATE location SET 
	points_encoded = CONCAT('a:1:{i:0;s:', LENGTH(points_encoded), ':"', points_encoded, '";}'),
	levels_encoded = CONCAT('a:1:{i:0;s:', LENGTH(levels_encoded), ':"', levels_encoded, '";}')
WHERE points_encoded IS NOT NULL;

CREATE TABLE `provider_message_api` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `type` varchar(25) NOT NULL,
  `dispatcher` varchar(100) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_provider_id` (`provider_id`),
  KEY `provider_message_api_provider_id` (`provider_id`),
  CONSTRAINT `provider_message_api_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

ALTER TABLE carrier ADD weight SMALLINT(11) DEFAULT 0 AFTER address_domain;

INSERT IGNORE INTO system_message_event (id,type,name,description,applicable_delivery_methods,token_description)
VALUES (13, 'TestSms', 'Test SMS', 'Fires off a test SMS message to the specified mobile number','sms',"<b>$mobile</b> (Mobile number we are testing against)
<b>$carrier</b> (Mobile number carrier)");

INSERT INTO acl_resource VALUES (142,'GetProviderAttributesRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,142,1,NOW());

ALTER TABLE vertical_ranking_rule CHANGE `weight` `weight` DECIMAL(6,2) NOT NULL COMMENT 'A weight from 1 to 100';

DROP TABLE IF EXISTS campaign_contact;

#3.1
UPDATE match_rule SET description = 'Filter out providers who do not service within 20KM of the specified job location. Use this in conjuction with Lep_Rank_JobProximity to rank providers based on proximity.' WHERE id = 9;  
INSERT INTO acl_resource VALUES (143,'PropagateQuestionRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,143,1,NOW());
INSERT INTO acl_resource VALUES (144,'SaveApplicationIdentityRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,144,1,NOW());
INSERT INTO acl_resource VALUES (145,'GetAclRolesRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,145,1,NOW());
INSERT INTO acl_resource VALUES (146,'GetAclResourcesRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,146,1,NOW());
INSERT INTO acl_resource VALUES (147,'SaveAclRoleRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,147,1,NOW());
INSERT INTO acl_resource VALUES (148,'UpdateCampaignStatusRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,148,1,NOW());

ALTER TABLE acl_role ADD `client_id` int(11) NOT NULL;
UPDATE acl_role SET client_id=1;
ALTER TABLE acl_role ADD CONSTRAINT `fk_acl_role_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

DROP INDEX idx_username ON application_identity;
ALTER TABLE application_identity ADD UNIQUE KEY `uk_username` (`username`);
ALTER TABLE vertical ADD total_slots INT(11) DEFAULT 3 COMMENT 'Total number of match slots applicable to vertical' AFTER created_date;
UPDATE user_role, user,client SET user_role.role='administrator-lem'
WHERE user.id = user_role.user_id AND  client.currency_code!='USD' AND user_role.role='administrator';
UPDATE user_role SET role = 'account' WHERE role = 'external';
UPDATE user_role SET role = 'csr' WHERE role = 'channel-manager';

INSERT INTO acl_resource VALUES (149,'SaveRankingAdjustmentRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,149,1,NOW());

INSERT IGNORE INTO provider_message_api (provider_id, type) 
SELECT 513, 'AlliedPickfords' FROM client WHERE client.currency_code!='USD';

ALTER TABLE `location` ADD INDEX `idx_label` (`label`);
ALTER TABLE `location` ADD INDEX `idx_alt_label` (`alt_label`);
ALTER TABLE `location` ADD INDEX `idx_type` (`type`);


#ALTER table message_audit ADD receipt VARCHAR(100) NULL AFTER data;
