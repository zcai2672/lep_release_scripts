CREATE TABLE `form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` enum('inactive','staging','active') NOT NULL DEFAULT 'inactive',
  `type` enum('feedback','registration') NOT NULL DEFAULT 'registration',
  `client_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `tmp_channel_id` int(11) NOT NULL,
  `tmp_form_id` int(11) DEFAULT NULL,
  `note` text COMMENT 'An internal note against the model',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_form_client_id` (`client_id`),
  CONSTRAINT `fk_form_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `form_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `description` text,
  `page` int(11) NOT NULL DEFAULT '1',
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `default_value` varchar(50) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_form_question_channel` (`form_id`),
  KEY `fk_form_question_question` (`question_id`),
  CONSTRAINT `fk_form_question_channel` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_form_question_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT 'Maps questions to forms';                    

CREATE TABLE `form_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_form_option_form_id` (`form_id`),
  KEY `fk_form_option_option_id` (`option_id`),
  CONSTRAINT `fk_form_option_form_id` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_form_option_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT 'Maps options to forms';


CREATE TABLE `form_vertical` (
  `vertical_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`vertical_id`,`form_id`),
  KEY `fk_form_vertical_vertical_id` (`vertical_id`),
  KEY `fk_form_vertical_form_id` (`form_id`),
  CONSTRAINT `fk_form_vertical_form_id` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_form_vertical_vertical_id` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT 'Maps verticals to forms';

#Create ACL roles and default permissions
INSERT INTO acl_resource (name) VALUES ('AssignFormQuestionsRQ');
INSERT INTO acl_role_permission(permission, acl_resource_id, acl_role_id)
VALUES ('allow', LAST_INSERT_ID(), 1);
INSERT INTO acl_resource (name) VALUES ('PublishFormChangesRQ');
INSERT INTO acl_role_permission(permission, acl_resource_id, acl_role_id)
VALUES ('allow', LAST_INSERT_ID(), 1);
INSERT INTO acl_resource (name) VALUES ('SaveFormRQ');
INSERT INTO acl_role_permission(permission, acl_resource_id, acl_role_id)
VALUES ('allow', LAST_INSERT_ID(), 1);
INSERT INTO acl_resource (name) VALUES ('UpdateFormStatusRQ');
INSERT INTO acl_role_permission(permission, acl_resource_id, acl_role_id)
VALUES ('allow', LAST_INSERT_ID(), 1);
INSERT INTO acl_resource (name) VALUES ('GetFormRQ');
INSERT INTO acl_role_permission(permission, acl_resource_id, acl_role_id)
VALUES ('allow', LAST_INSERT_ID(), 1);
INSERT INTO acl_resource (name) VALUES ('GetFormsRQ');
INSERT INTO acl_role_permission(permission, acl_resource_id, acl_role_id)
VALUES ('allow', LAST_INSERT_ID(), 1);
INSERT INTO acl_resource (name) VALUES ('DuplicateFormRQ');
INSERT INTO acl_role_permission(permission, acl_resource_id, acl_role_id)
VALUES ('allow', LAST_INSERT_ID(), 1);

#TRUNCATE form_vertical;
#TRUNCATE form_option;
#TRUNCATE form_question;
#TRUNCATE form;

#Migrate channel data to the new form schema
#Create a form for each channel and assign the form back to the channel
INSERT INTO form (name, status, client_id, tmp_channel_id) 
SELECT name, status, client_id, id FROM channel;
ALTER TABLE channel ADD `form_id` int(11) DEFAULT NULL,
                    ADD CONSTRAINT `fk_channel_form_id` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`);
ALTER TABLE channel ADD `feedback_form_id` int(11) DEFAULT NULL,
	            ADD CONSTRAINT `fk_channel_feedback_form_id` FOREIGN KEY (`feedback_form_id`) REFERENCES `form` (`id`);
UPDATE channel c, form f SET c.form_id = f.id WHERE c.id = f.tmp_channel_id;

#Assign form to job spec
ALTER TABLE job_spec ADD form_id INT(11) DEFAULT NULL AFTER channel_id,
		     ADD CONSTRAINT `fk_job_spec_form_id` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`);
UPDATE job_spec qot, form f SET qot.form_id = f.id
WHERE qot.channel_id = f.tmp_channel_id;
ALTER TABLE job_spec MODIFY `form_id` int(11) NOT NULL;

#Assign form to consumer request
ALTER TABLE consumer_request ADD form_id INT(11) DEFAULT NULL AFTER channel_id,
		     ADD CONSTRAINT `fk_consumer_request_form_id` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`);
UPDATE consumer_request qot, form f SET qot.form_id = f.id
WHERE qot.channel_id = f.tmp_channel_id;
ALTER TABLE consumer_request MODIFY `form_id` int(11) NOT NULL;

#Assign the correct form id to question_option_tree
ALTER TABLE question_option_tree ADD `form_id` int(11) DEFAULT NULL;
UPDATE question_option_tree qot, form f SET qot.form_id = f.id 
WHERE qot.channel_id = f.tmp_channel_id;
ALTER TABLE question_option_tree DROP FOREIGN KEY `fk_question_option_tree_channel`, 
                                DROP `channel_id`,
                                ADD CONSTRAINT `fk_question_option_tree_form` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`);
                                
ALTER TABLE question_option_tree MODIFY `form_id` int(11) NOT NULL;

#Copy over channel verticals
INSERT INTO form_vertical(vertical_id, form_id) 
SELECT c.vertical_id, f.id FROM channel_vertical c 
JOIN form f ON f.tmp_channel_id = c.channel_id;

#Purge orphan records
DELETE FROM channel_option WHERE option_id NOT IN (SELECT id FROM `option`);
#Copy over channel options
INSERT INTO form_option(option_id, form_id) 
SELECT c.option_id, f.id FROM channel_option c 
JOIN form f ON f.tmp_channel_id = c.channel_id;

#Purge orphan records
DELETE FROM channel_question WHERE question_id NOT IN (SELECT id FROM question);
#Copy over channel question
INSERT INTO form_question(question_id, form_id) 
SELECT c.question_id, f.id FROM channel_question c 
JOIN form f ON f.tmp_channel_id = c.channel_id;

#Clean up
#DROP TABLE channel_vertical;
#DROP TABLE channel_option;
#DROP TABLE channel_question;
#ALTER TABLE form DROP tmp_channel_id;

UPDATE form_question fq, (
    SELECT cq.channel_id, cq.question_id, f.id as form_id ,cq.page,cq.label,cq.description
    FROM channel_question cq
        JOIN form f ON f.tmp_channel_id = cq.channel_id 
    WHERE cq.channel_id IN (SELECT DISTINCT(channel_id) FROM channel_question WHERE page > 1)) t
SET fq.page = t.page, fq.label = t.label, fq.description = t.description 
WHERE fq.question_id = t.question_id AND fq.form_id = t.form_id;

UPDATE form_question fq, (
    SELECT cq.is_hidden, cq.channel_id, cq.question_id, f.id as form_id ,cq.page,cq.label,cq.description
    FROM channel_question cq
        JOIN form f ON f.tmp_channel_id = cq.channel_id 
    WHERE cq.channel_id IN (SELECT DISTINCT(channel_id) FROM channel_question)) t
SET fq.is_hidden = t.is_hidden, fq.label = t.label, fq.description = t.description 
WHERE fq.question_id = t.question_id AND fq.form_id = t.form_id; 
