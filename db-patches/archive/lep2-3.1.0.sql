INSERT INTO acl_resource VALUES (150,'GetCampaignChangeHistoryRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,150,1,NOW());
INSERT INTO acl_resource VALUES (151,'SaveBucketRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,151,1,NOW());
INSERT INTO acl_resource VALUES (152,'GetBucketRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,152,1,NOW());
INSERT INTO acl_resource VALUES (153,'GetBucketsRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,153,1,NOW());
INSERT INTO acl_resource VALUES (154,'AssignBucketModelRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,154,1,NOW());
INSERT INTO acl_resource VALUES (155,'RemoveBucketModelRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,155,1,NOW());
INSERT INTO acl_resource VALUES (156,'SaveBucketCapRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,156,1,NOW());
INSERT INTO acl_resource VALUES (157,'GetBucketCapsRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,157,1,NOW());

ALTER TABLE consumer_request ADD `affiliate_id` INT(11) NULL,
                     ADD CONSTRAINT `fk_consumer_request_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

INSERT IGNORE INTO match_rule(id,name,description,default_type) VALUES (12,'Lep_Match_BucketCap', 'Filter out providers based on their cap settings for a specific bucket', 'provider');


CREATE TABLE `bucket` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `status` enum('inactive','active') NOT NULL default 'active',
  `inclusive` tinyint(4) NOT NULL default '0',
  `client_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `bucket_client_id` (`client_id`),
  CONSTRAINT `bucket_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Distribution channels between an affiliate and channel forms';

CREATE TABLE `affiliate_bucket` (
  `id` int(11) NOT NULL auto_increment,
  `affiliate_id` int(11) NOT NULL,
  `bucket_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `affiliate_bucket_bucket_id` (`bucket_id`),
  KEY `affiliate_bucket_affiliate_id` (`affiliate_id`),
  CONSTRAINT `affiliate_bucket_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `affiliate_bucket_bucket_id` FOREIGN KEY (`bucket_id`) REFERENCES `bucket` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Maps affiliates to buckets';

CREATE TABLE `bucket_cap` (
  `id` int(11) NOT NULL auto_increment,
  `bucket_id` int(11) default NULL,
  `affiliate_id` int(11) default NULL,
  `provider_id` int(11) default NULL,
  `daily_lead_cap` int(11) NOT NULL default '0',
  `daily_lead_count` int(11) NOT NULL default '0',
  `weekly_lead_cap` int(11) NOT NULL default '0',
  `weekly_lead_count` int(11) NOT NULL default '0',
  `monthly_lead_cap` int(11) NOT NULL default '0',
  `monthly_lead_count` int(11) NOT NULL default '0',
  `total_lead_cap` int(11) NOT NULL default '0',
  `total_lead_count` int(11) NOT NULL default '0',
  `total_spend_cap` decimal(7,2) NOT NULL default '0.00',
  `total_spend` decimal(7,2) NOT NULL default '0.00',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL default NULL,
  `count_modified_date` timestamp NULL default NULL COMMENT 'Last time counts were modified',
  PRIMARY KEY  (`id`),
  KEY `bucket_cap_affiliate_id` (`affiliate_id`),
  KEY `bucket_cap_provider_id` (`provider_id`),
  KEY `bucket_cap_bucket_id` (`bucket_id`),
  CONSTRAINT `bucket_cap_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`),
  CONSTRAINT `bucket_cap_bucket_id` FOREIGN KEY (`bucket_id`) REFERENCES `bucket` (`id`),
  CONSTRAINT `bucket_cap_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Controls bucket caps for affiliates and providers';

CREATE TABLE `channel_bucket` (
  `id` int(11) NOT NULL auto_increment,
  `bucket_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `channel_bucket_bucket_id` (`bucket_id`),
  KEY `channel_bucket_channel_id` (`channel_id`),
  CONSTRAINT `channel_bucket_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `channel_bucket_bucket_id` FOREIGN KEY (`bucket_id`) REFERENCES `bucket` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Maps channels to buckets';

CREATE TABLE `provider_bucket` (
  `id` int(11) NOT NULL auto_increment,
  `bucket_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `provider_bucket_bucket_id` (`bucket_id`),
  KEY `provider_bucket_provider_id` (`provider_id`),
  CONSTRAINT `provider_bucket_bucket_id` FOREIGN KEY (`bucket_id`) REFERENCES `bucket` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `provider_bucket_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB COMMENT='Maps providers to buckets';

CREATE TABLE `consumer_request_bucket` (
  `id` int(11) NOT NULL auto_increment,
  `consumer_request_id` int(11) NOT NULL,
  `bucket_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_crb_consumer_request_id` (`consumer_request_id`),
  KEY `fk_crb_bucket_id` (`bucket_id`),
  CONSTRAINT `fk_crb_consumer_request_id` FOREIGN KEY (`consumer_request_id`) REFERENCES `consumer_request` (`id`),
  CONSTRAINT `fk_crb_bucket_id` FOREIGN KEY (`bucket_id`) REFERENCES `bucket` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Which buckets were applicable for a consumer request';

INSERT IGNORE INTO tag (id,name,description,is_system_tag) 
VALUES (3,'Job Default', 'Identifies questions or options which are preassigned to newly created campaigns',1);
