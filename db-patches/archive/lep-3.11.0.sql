INSERT INTO registration_filter (id, name, description) VALUES (11, 'Lep_RegistrationFilter_ElectronicPhoneVerification', 'Uses an external phone verification to verify if the consumer\'s phone number is a working, traceable telephone number.');
ALTER TABLE consumer_request ADD `requires_verification` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Indicates if the consumer request requires verification (i.e. The EPV filter is enabled for the channel)' AFTER `affiliate_id`; 

INSERT INTO acl_resource (name) VALUES ('VerifyConsumerRequestRQ');
INSERT INTO acl_role_permission(permission, acl_resource_id, acl_role_id)
VALUES ('allow', LAST_INSERT_ID(), 1);

INSERT INTO acl_resource (name) VALUES ('RequestVerificationTokenRQ');
INSERT INTO acl_role_permission(permission, acl_resource_id, acl_role_id)
VALUES ('allow', LAST_INSERT_ID(), 1);

CREATE TABLE `consumer_request_verification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_request_id` int(11) NOT NULL,
  `verification_token` char(4) NOT NULL,
  `transaction_id` varchar(50) NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `cr_consumer_request_id` (`consumer_request_id`),
  CONSTRAINT `cr_consumer_request_id` FOREIGN KEY (`consumer_request_id`) REFERENCES `consumer_request` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COMMENT='Stores consumer verification tokens (i.e. EPV)';

CREATE TABLE `dialer_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `number` varchar(15) NOT NULL,
  `voice_mail_script` varchar(50) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ds_user_id` (`user_id`),
  CONSTRAINT `ds_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Stores the dialer settings for various agents (i.e. users)';

INSERT INTO dialer_settings (user_id, number) 
SELECT id,"+61280053608" FROM user WHERE username = 'joannam';
INSERT INTO dialer_settings (user_id, number) 
SELECT id,"+61280053628" FROM user WHERE username = 'ankits';
INSERT INTO dialer_settings (user_id, number) 
SELECT id,"+61280053616" FROM user WHERE username = 'rachelled';
INSERT INTO dialer_settings (user_id, number) 
SELECT id,"+61280053627" FROM user WHERE username = 'stanleyn';
INSERT INTO dialer_settings (user_id, number) 
SELECT id,"+61280053612" FROM user WHERE username = 'andrewm';
INSERT INTO dialer_settings (user_id, number) 
SELECT id,"+61280053624" FROM user WHERE username = 'karenm';
INSERT INTO dialer_settings (user_id, number) 
SELECT id,"+61421062109" FROM user WHERE username = 'julianb';