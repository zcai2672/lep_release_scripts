ALTER TABLE json_session ADD channel_id int(11) NULL AFTER data;
ALTER TABLE json_session ADD CONSTRAINT `fk_json_session_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE json_session ADD ip_address varchar(16) NULL AFTER data;
ALTER TABLE json_session ADD user_agent varchar(255) NULL AFTER ip_address;
ALTER TABLE json_session CHANGE COLUMN `type` `type`  ENUM('question','auth','referrer','form-view','conversion')  NOT NULL; 

ALTER TABLE `job_spec_detail` ADD CONSTRAINT `fk_job_spec_detail_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
UPDATE job_spec_detail SET option_id=NULL WHERE option_id=0;
DELETE FROM job_spec_detail WHERE option_id IS NOT NULL AND option_id NOT IN (SELECT id FROM `option`);
ALTER TABLE `job_spec_detail` ADD CONSTRAINT `fk_job_spec_detail_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE audit_log ADD is_model_change tinyint(4) DEFAULT 1 COMMENT '1 = change to an actual model. 0 = a high level change.' AFTER audit_message;

INSERT INTO registration_filter(name,description) 
	SELECT 'Lep_RegistrationFilter_All', 'Sets all registrations to pending indiscriminately'
	FROM DUAL WHERE NOT EXISTS (SELECT id FROM registration_filter WHERE name = 'Lep_RegistrationFilter_All');


CREATE INDEX idx_loc_left_id ON location (left_id);
CREATE INDEX idx_loc_level ON location (level);
CREATE INDEX idx_loc_right_id ON location (right_id);
