CREATE UNIQUE INDEX uk_channel_bucket ON channel_bucket (bucket_id, channel_id);
CREATE UNIQUE INDEX uk_provider_bucket ON provider_bucket (bucket_id, provider_id);
CREATE UNIQUE INDEX uk_affiliate_bucket ON affiliate_bucket (bucket_id, affiliate_id);
ALTER TABLE `affiliate` ADD `status` enum('inactive','active') NOT NULL default 'active' AFTER `client_id`,
ADD `description` VARCHAR(500) NULL AFTER `client_id`,
ADD `application_identity_id` int(11) NULL AFTER `client_id`,
ADD `publisher_key` varchar(50) NULL COMMENT 'Publisher key to access JSON services' AFTER `client_id`,
ADD CONSTRAINT `fk_affiliate_application_identity_id` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `provider` CHANGE `affiliate_id` `affiliate_id` int(11) default NULL COMMENT 'Indicates provider was created by an affiliate',
                       CHANGE `type` `type` enum('provider','backfill','prospect','affiliate') NOT NULL default 'provider',
                       ADD `buyer_affiliate_id` int(11) default NULL COMMENT 'Indicates affiliate who is a buyer of leads',
                       DROP `tmp_orig_status`,
                       ADD CONSTRAINT `fk_provider_buyer_affiliate_id` FOREIGN KEY (`buyer_affiliate_id`) REFERENCES `affiliate` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `match_rule` CHANGE `default_type` `default_type` enum('provider','backfill','prospect','affiliate') NOT NULL default 'provider' COMMENT 'Defines the default for which type of provider to filter on';
ALTER TABLE `vertical_match_rule` CHANGE `type` `type` enum('provider','backfill','prospect','affiliate') NOT NULL default 'provider';

CREATE TABLE `affiliate_role` (
  `id` int(11) NOT NULL auto_increment,
  `affiliate_id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL default '',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uk_affiliate_role1` (`affiliate_id`,`role`),
  KEY `fk_affiliate_role_affiliate_id` (`affiliate_id`),
  CONSTRAINT `fk_affiliate_role_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;


INSERT INTO acl_resource VALUES (160,'GetAffiliateRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,160,1,NOW());
INSERT INTO acl_resource VALUES (161,'GetAffiliatesRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,161,1,NOW());
INSERT INTO acl_resource VALUES (162,'SaveAffiliateRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,162,1,NOW());

DROP TABLE IF EXISTS `unmatched_provider`;

ALTER TABLE `metadata` CHANGE `type` `type` enum('force_pending_reason','force_pending','remote_ip','key1','key2','remote_referrer','exclusive_match') NOT NULL;


#migrate JSON publishers to new affiliate models
INSERT INTO affiliate (name, client_id, publisher_key, application_identity_id, status)
SELECT ai.real_name, ai.client_id, jpi.publisher_id, ai.id, 'active'
FROM application_identity ai
JOIN json_publisher_identity jpi ON jpi.application_identity_id = ai.id
LEFT JOIN affiliate a ON a.publisher_key = jpi.publisher_id
WHERE a.id IS NULL AND username != 'quotifyadmin';

INSERT IGNORE INTO affiliate_role (affiliate_id, role)
SELECT a.id, "seller" FROM affiliate a
JOIN json_publisher_identity jpi ON jpi.publisher_id = a.publisher_key;

INSERT INTO bucket (name, description, status ,inclusive, client_id)
SELECT name, CONCAT(name , ' Bucket'), 'active', 1 , 1 FROM affiliate;
#Assign affiliate to buckets
INSERT INTO affiliate_bucket (bucket_id, affiliate_id)
SELECT b.id,c.id FROM affiliate c, bucket b WHERE c.name = b.name;


#Assign all channels to all buckets
INSERT INTO channel_bucket (bucket_id, channel_id)
SELECT b.id,c.id FROM channel c, bucket b WHERE c.status = "active";


INSERT IGNORE INTO match_rule (id,name,description,default_type)
VALUES (13, 'Lep_Match_JobPreference', "Filter out providers who haven't got the same options that are indicated by the consumer and at least one high preference.", 'provider');

INSERT IGNORE INTO tag (id,name,description,is_system_tag)
VALUES (46,'Job Preference','Identifies questions used with the Lep_Match_JobPreference rule',1);

ALTER TABLE `cap` ADD `free_lead_count` int(11) NOT NULL default '0' AFTER `free_lead_cap`,
                  ADD `modified_date` timestamp NULL default NULL AFTER `created_date`,
                  ADD `count_modified_date` timestamp NULL default NULL COMMENT 'Last time counts were modified' AFTER `modified_date`;


#Match Set alteration 5.6 secs
CREATE TABLE `match_set_tmp` engine=innodb SELECT * FROM match_set;
# 3.68 secs
ALTER TABLE `match_set_tmp` ADD `type` enum('provider','prospect','affiliate') NOT NULL default 'provider' AFTER `lead_plan_id`
, ADD `is_free` TINYINT(4) NOT NULL DEFAULT 0 AFTER `lead_delay`,CHANGE `created_date`  `created_date` timestamp NOT NULL DEFAULT  CURRENT_TIMESTAMP;


#20.3 secs
ALTER TABLE `match_set_tmp`
  ADD KEY `fk_match_set_campaign_id` (`campaign_id`),
  ADD KEY `fk_match_set_lead_plan_id` (`lead_plan_id`),
  ADD KEY `fk_match_set_job_spec_id` (`job_spec_id`),
  ADD KEY `fk_match_set_provider_id` (`provider_id`),
  ADD KEY `idx_created_date` (`created_date`),
  ADD KEY `idx_lead_delay` (`lead_delay`),
  ADD KEY `idx_type` (`type`),
  ADD  KEY `idx_is_free` (`is_free`),
  ADD CONSTRAINT `fk2_match_set_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  ADD CONSTRAINT `fk2_match_set_job_spec_id` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`),
  ADD CONSTRAINT `fk2_match_set_lead_plan_id` FOREIGN KEY (`lead_plan_id`) REFERENCES `lead_plan` (`id`),
  ADD CONSTRAINT `fk2_match_set_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  MODIFY `id` int(11) NOT NULL auto_increment,
  ADD PRIMARY KEY  (`id`);

RENAME TABLE `match_set` TO `match_set_old`, `match_set_tmp` TO `match_set`;
DROP TABLE match_set_old;



#Audit log alterations  (2 min 2.22 sec)
CREATE TABLE `audit_log_tmp` engine=innodb SELECT * FROM `audit_log`;

#Query OK, 9829478 rows affected (9 min 17.38 sec)
ALTER TABLE `audit_log_tmp` ADD `parent_model` varchar(100) NULL AFTER `model`,
                        ADD `parent_model_id` int(11) NULL AFTER `model_id`,
                        ADD KEY `idx_parent_model` (`parent_model`),
                        ADD KEY `idx_parent_model_id` (`parent_model_id`),
                        ADD KEY `idx_model` (`model`),
                        ADD KEY `idx_model_id` (`model_id`),
                        ADD KEY `idx_application_identity` (`app_identity`),
                        MODIFY `id` int(11) NOT NULL auto_increment,
                        ADD PRIMARY KEY  (`id`);

RENAME TABLE `audit_log` to `audit_log_old`, `audit_log_tmp` to `audit_log`;
#DROP TABLE `audit_log_old`;



#Unmatched campaign alterations 11 min 1.60 sec
CREATE TABLE `unmatched_campaign_tmp` engine=innodb SELECT * FROM `unmatched_campaign` LIMIT 0;

ALTER TABLE `unmatched_campaign_tmp` ADD `type` enum('provider','prospect','affiliate') NOT NULL default 'provider' AFTER `reason`,
                                     CHANGE `created_date`  `created_date` timestamp NOT NULL DEFAULT  CURRENT_TIMESTAMP;


SET AUTOCOMMIT = 0;SET FOREIGN_KEY_CHECKS=0;

#(13 min 49.18 sec)
INSERT INTO `unmatched_campaign_tmp` (`id`, `provider_id`, `job_spec_id`,`campaign_id`,`reason`
,`created_date`, `is_prospect`, `type`)
SELECT `id`, `provider_id`, `job_spec_id`,`campaign_id`,`reason`
,`created_date`, `is_prospect`, 'provider' FROM `unmatched_campaign`;

SET FOREIGN_KEY_CHECKS = 1;COMMIT;SET AUTOCOMMIT = 1;


ALTER TABLE `unmatched_campaign_tmp`
  ADD KEY `idx_type` (`type`),
  ADD KEY `fk_unmatched_campaign_campaign_id` (`campaign_id`),
  ADD KEY `fk_unmatched_campaign_job_spec_id` (`job_spec_id`),
  ADD KEY `fk_unmatched_campaign_provider_id` (`provider_id`),
  ADD CONSTRAINT `fk2_unmatched_campaign_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  ADD CONSTRAINT `fk2_unmatched_campaign_job_spec_id` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`),
  ADD CONSTRAINT `fk2_unmatched_campaign_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  MODIFY `id` int(11) NOT NULL auto_increment,
  ADD PRIMARY KEY  (`id`);


RENAME TABLE `unmatched_campaign` to `unmatched_campaign_old`, `unmatched_campaign_tmp` to `unmatched_campaign`;
#DROP TABLE `unmatched_campaign_old`;


UPDATE `unmatched_campaign_tmp` ms SET ms.type = "prospect" WHERE ms.is_prospect = 1;
UPDATE `unmatched_campaign`, `job_spec` SET `unmatched_campaign`.created_date = `job_spec`.created_date WHERE `unmatched_campaign`.job_spec_id = `job_spec`.id;


UPDATE match_set_tmp ms,provider_charge pc SET ms.is_free = 1
WHERE ms.campaign_id=pc.campaign_id AND ms.job_spec_id=pc.key_id
    AND pc.key_type='job-spec' AND pc.type='lead' AND pc.amount=0;

UPDATE match_set_tmp ms SET ms.type = "prospect"
WHERE ms.is_prospect = 1;

