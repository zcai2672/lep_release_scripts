
-- applied to sandbox DB Production - ?? :
-- ALTER TABLE `user` ADD `login_status` ENUM( 'active', '1', '2', 'locked' ) NOT NULL DEFAULT 'active';


CREATE TABLE IF NOT EXISTS `user_system_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

INSERT INTO `user_system_roles` (`id`, `role_title`) VALUES
(1, 'csr'),
(2, 'administrator-lem'),
(3, 'provider'),
(4, 'no-capping-flag'),
(5, 'no-creditting-flag'),
(6, 'limited-pricing');


ALTER TABLE external_identifier  ADD KEY `idx_model_name` (`model_name`),
                       ADD KEY `idx_model_id` (`model_id`),
                       ADD KEY `idx_external_system` (`external_system`),
                       ADD KEY `idx_external_id` (`external_id`);

INSERT INTO acl_resource VALUES (163,'ExternalIdentifierSearchRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,163,1,NOW());
INSERT INTO acl_resource VALUES (164,'SaveExternalIdentifiersRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,164,1,NOW());
INSERT INTO acl_resource VALUES (165,'AssignLeadRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,165,1,NOW());

ALTER TABLE match_set ADD is_manual TINYINT(4) DEFAULT 0,
		      ADD KEY idx_is_manual (`is_manual`);
		      
INSERT INTO `user_system_roles` (id, role_title) VALUES (7, 'sensis-customer-care');		      