ALTER TABLE provider CHANGE COLUMN `status` `status`  enum('active','deleted','cancelled','pending','suspended','backfill','prospect') NOT NULL;
ALTER TABLE match_set ADD `is_prospect` tinyint(4) NOT NULL default '0';
ALTER TABLE unmatched_provider ADD `is_prospect` tinyint(4) NOT NULL default '0';
INSERT INTO system_message_event (id,type,name,description,applicable_delivery_methods,token_description)
VALUES (11,'LeadProspectAlert','Lead Prospect Alert','Lead alerts sent to prospect','email,sms,fax','<b>$vertical</b>  (Vertical name)
<b>$provider_name</b>
<b>$provider_key</b> (Identifying key of provider)
<b>$lead_date</b> (Date and time lead was generated)
<b>$lead_delay_date</b> (Date and time lead was delayed (if any))
<b>$email</b> (Email address that the lead alert will be sent to, if applicable for delivery method)
<b>$fax</b> (Fax that the lead alert will be sent to, if applicable for delivery method)
<b>$mobile</b> (Mobile number that the lead alert will be sent to, if applicable for delivery method)
<b>$job_description</b> (Descriptive text for the project. Available in both the message body and message subject.)
<b>$product_description</b> (Main product option of project. Available in both the message body and message subject.)
<b>$consumer_inputs</b> (An array where each item consists of $question_label $option_label and $text_value)
<b>$consumer_name</b> (Is available in both the message body and message subject)
<b>$consumer_address1</b>
<b>$consumer_address2</b>
<b>$consumer_postcode</b>
<b>$consumer_city</b>
<b>$consumer_email</b>
<b>$consumer_phone</b>
<b>$consumer_mobile</b>
<b>$best_contact_time</b>
<b>$message_subject</b>
<b>$lead_count</b>  (Number of leads assigned to the prospect)

<b>$next_provider_name</b>  
Name of another random prospect that could be matched to future projects. Note that
this may not
');

UPDATE system_message_event SET token_description='<b>$vertical</b>  (Vertical name)
<b>$provider_name</b>
<b>$provider_key</b> (Identifying key of provider)
<b>$lead_date</b> (Date and time lead was generated)
<b>$lead_delay_date</b> (Date and time lead was delayed (if any))
<b>$email</b> (Email address that the lead alert will be sent to, if applicable for delivery method)
<b>$fax</b> (Fax that the lead alert will be sent to, if applicable for delivery method)
<b>$mobile</b> (Mobile number that the lead alert will be sent to, if applicable for delivery method)
<b>$job_description</b> (Descriptive text for the project. Available in both the message body and message subject.)
<b>$product_description</b> (Main product option of project. Available in both the message body and message subject.)
<b>$consumer_inputs</b> (An array where each item consists of $question_label $option_label and $text_value)
<b>$consumer_name</b> (Is available in both the message body and message subject)
<b>$consumer_address1</b>
<b>$consumer_address2</b>
<b>$consumer_postcode</b>
<b>$consumer_city</b>
<b>$consumer_email</b>
<b>$consumer_phone</b>
<b>$consumer_mobile</b>
<b>$best_contact_time</b>
<b>$message_subject</b>' WHERE id=1;

UPDATE system_message_event SET token_description='<b>$vertical</b>  (Vertical name)
<b>$provider_name</b>
<b>$provider_key</b> (Identifying key of provider)
<b>$lead_date</b> (Date and time lead was generated)
<b>$lead_delay_date</b> (Date and time lead was delayed (if any))
<b>$email</b> (Email address that the lead alert will be sent to, if applicable for delivery method)
<b>$fax</b> (Fax that the lead alert will be sent to, if applicable for delivery method)
<b>$mobile</b> (Mobile number that the lead alert will be sent to, if applicable for delivery method)
<b>$job_description</b> (Descriptive text for the project. Available in both the message body and message subject.)
<b>$product_description</b> (Main product option of project. Available in both the message body and message subject.)
<b>$consumer_inputs</b> (An array where each item consists of $question_label $option_label and $text_value)
<b>$consumer_name</b> (Is available in both the message body and message subject)
<b>$consumer_address1</b>
<b>$consumer_address2</b>
<b>$consumer_postcode</b>
<b>$consumer_city</b>
<b>$consumer_email</b>
<b>$consumer_phone</b>
<b>$consumer_mobile</b>
<b>$best_contact_time</b>
<b>$message_subject</b>' WHERE id=9;

UPDATE system_message_event SET token_description='<b>$description</b>  (Credit description)
<b>$provider_key</b> (Identifying key of provider)
<b>$provider_contact</b> (Name of provider contact)
<b>$submitted</b> (Date & time the credit was submitted)
<b>$status</b> (Status of the credit)
<b>$comment</b> (Lead credit comment)
<b>$internal_description</b> (Credit\'s internal description)
<b>$email</b> (Provider\'s email address)
<b>$id</b> (Lead Id which is assigned the credit. Available in both the message body and message subject.)
<b>$consumer_name</b> (Name of consumer who submitted the project. Available in both the message body and message subject.)' WHERE id=8;


ALTER TABLE match_rule CHANGE COLUMN `type` `default_type`  enum('provider','backfill','prospect') NOT NULL DEFAULT 'provider' COMMENT 'Defines the default for which type of provider to filter on';
ALTER TABLE vertical_match_rule ADD `type` enum('provider','backfill','prospect') NOT NULL DEFAULT 'provider';
DELETE vmr2 FROM vertical_match_rule vmr1,vertical_match_rule vmr2
WHERE vmr1.match_rule_id=vmr2.match_rule_id AND vmr1.vertical_id=vmr2.vertical_id
AND vmr1.type=vmr2.type AND vmr2.id>vmr1.id;
UPDATE vertical_match_rule,match_rule SET vertical_match_rule.type=match_rule.default_type WHERE vertical_match_rule.match_rule_id=match_rule.id;
ALTER IGNORE TABLE vertical_match_rule DROP INDEX `unique_key`;
CREATE UNIQUE INDEX uk_vertical_match_rule1 ON vertical_match_rule (vertical_id,match_rule_id,type);
ALTER TABLE vertical ADD prospect_slots INT(11) DEFAULT 0 COMMENT 'Number of slots made available for prospects';
ALTER TABLE vertical ADD force_prospects TINYINT(4) DEFAULT 0 COMMENT 'Reserves slots in the final match for prospects regardless of coverage';
UPDATE match_rule SET name='Lep_Match_JobProximity',description='Filter out providers who do not service within 20KM the specified job location. Use this in conjuction with Lep_Rank_JobProximity to rank providers based on proximity.' WHERE id=9;
UPDATE match_rule SET name='Lep_Match_RemovalProximity',description='Filter out providers who do not service within 20KM of the pickup or dropoff locations. Use this in conjuction with Lep_Rank_JobProximity to rank providers based on proximity.' WHERE id=10;
INSERT INTO ranking_rule (id,name,description) VALUES (9,'Lep_Rank_JobProximity','Rank providers on their proximity to a job location. NOTE: This rule must be used in conjunction with either Lep_Match_JobProximity or Lep_Match_RemovalProximity.');
ALTER TABLE match_set ADD lead_delay TIMESTAMP NULL COMMENT 'Indicates until when the lead should be not seen by the provider';
INSERT INTO tag(id,name,description,is_system_tag) VALUES (45,'Lead Delay','Enables assigning lead delays to a question.',1);
ALTER table `option` ADD lead_delay VARCHAR(20) NULL COMMENT 'Used to delay leads. Can be duration in hours, days, time or date time.';
ALTER TABLE consumer ADD comms_opt_out TINYINT(4) DEFAULT 0 COMMENT '1 = Consumer should not be sent any emails' AFTER email;
ALTER TABLE provider ADD comms_opt_out TINYINT(4) DEFAULT 0 COMMENT '1 = The provider will not receive any communications from LEP (including lead alerts)' AFTER phone_string;
INSERT INTO acl_resource VALUES (108,'CommsOptOutRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,108,1,NOW());
INSERT INTO acl_resource VALUES (109,'GetRemarketingOptionsRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,109,1,NOW());
INSERT INTO acl_resource VALUES (110,'DeleteRemarketingOptionRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,110,1,NOW());
INSERT INTO acl_resource VALUES (111,'SaveRemarketingOptionRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,111,1,NOW());
ALTER TABLE contact DROP `password`;

INSERT INTO system_message_event (id,type,name,description,applicable_delivery_methods,token_description)
VALUES (12,'Remarket','Remarketing Email','Remarketing email sent to consumer n days after a registration','email','<b>$consumer_key</b>  (Consumer\'s Key)
<b>$consumer_name</b> (Is available in both the message body and message subject)
<b>$project_date</b> (Date the initial project was submitted)
<b>$vertical_id</b> (Vertical Id of project)
<b>$vertical_name</b> (Name of vertical attached to the project)
<b>$channel_id</b> (Channel Id of project)
<b>$channel_name</b> (Name of channel attached to the project)
<b>$remarket_channel_id</b> (Id of channel that is being marketed to the consumer)
<b>$remarket_channel_name</b> (Name of channel that is being marketed to the consumer)');

CREATE TABLE `remarket` (
  `id` int(11) NOT NULL auto_increment,
  `vertical_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `delay` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_remarket_vertical_id` (`vertical_id`),
  KEY `fk_remarket_channel_id` (`channel_id`),
  CONSTRAINT `fk_remarket_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`),
  CONSTRAINT `fk_remarket_vertical_id` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COMMENT='Defines remarketing options between a vertical and channel';

