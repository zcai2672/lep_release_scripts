INSERT INTO acl_resource VALUES (97,'GetNonceRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,97,1,NOW());

INSERT INTO acl_resource VALUES (98,'GetProfileGroupRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,98,1,NOW());


TRUNCATE api_transaction;
ALTER TABLE api_transaction ADD expiry_date timestamp NOT NULL;

CREATE TABLE `channel_option` (
  `id` int(11) NOT NULL auto_increment,
  `channel_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_label` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_channel_option_channel_id` (`channel_id`),
  KEY `fk_channel_option_option_id` (`option_id`),
  CONSTRAINT `fk_channel_option_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`),
  CONSTRAINT `fk_channel_option_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`)
) ENGINE=InnoDB;