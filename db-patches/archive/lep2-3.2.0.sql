INSERT INTO acl_resource VALUES (158,'CopyServiceAreasRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,158,1,NOW());
INSERT INTO acl_resource VALUES (159,'AssignChannelQuestionsRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,159,1,NOW());

DROP TABLE IF EXISTS consumer_feedback_old;
DROP TABLE IF EXISTS consumer_question_answer;
DROP TABLE IF EXISTS consumer_question_answer_old;
DROP TABLE IF EXISTS provider_service_area;
DROP TABLE IF EXISTS match_set_old;

ALTER TABLE message_template MODIFY `status` ENUM('active','inactive') NOT NULL DEFAULT 'active';
CREATE UNIQUE INDEX uk_message_template ON message_template (system_message_event_id, message_template_group_id, delivery_method);

ALTER TABLE consumer_request ADD `activation_date` TIMESTAMP NULL COMMENT 'Date consumer request went active' AFTER created_date,
                             ADD KEY `idx_activation_date` (`activation_date`);

UPDATE `consumer_request` SET `activation_date` = `modified_date` WHERE `status` = 'active';

                   
ALTER TABLE `provider` ADD `note` text NULL COMMENT 'An internal note against the model' AFTER `created_date`;
ALTER TABLE `channel` ADD `note` text NULL COMMENT 'An internal note against the model' AFTER `created_date`;
ALTER TABLE `lead_credit_request` ADD `note` text NULL COMMENT 'An internal note against the model' AFTER `internal_description`;