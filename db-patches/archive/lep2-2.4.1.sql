ALTER TABLE consumer_request MODIFY `status` enum('pending','inactive','active','unprocessed') NOT NULL default 'unprocessed';
create index `idx_username` on `application_identity` (`username`);
create index `idx_password` on `application_identity` (`password`);
create index `idx_email` on `consumer` (`email`);

ALTER TABLE consumer_request ADD channel_id int(11) NULL AFTER status;
ALTER TABLE consumer_request ADD CONSTRAINT `fk_consumer_request_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;


