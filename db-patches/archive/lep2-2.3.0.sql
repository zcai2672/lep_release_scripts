INSERT INTO acl_resource VALUES (99,'GetMessageTemplateGroupsRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,99,1,NOW());

CREATE TABLE `message_template_group` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(32) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Message template groups defined in the system';

INSERT INTO `message_template_group` (`code`, `name`) VALUES ('Quotify', 'Quotify AU');
INSERT INTO `message_template_group` (`code`, `name`) VALUES ('QuotifyFunerals', 'Quotify AU - Funerals');
INSERT INTO `message_template_group` (`code`, `name`) VALUES ('Rockend', 'Rockend');
INSERT INTO `message_template_group` (`code`, `name`) VALUES ('Detelefoongids', 'Detelefoongids');

UPDATE `config` SET config_value = '24HourFollowUp_Subject' WHERE config_value = '24HFollowUp_Subject';
UPDATE `config` SET config_value = '24HourFollowUp_From' WHERE config_value = '24HFollowUp_From';
UPDATE `config` SET config_value = '24HourFollowUp_FromName' WHERE config_value = '24HFollowUp_FromName';

DELETE FROM `system_message_event` WHERE type = 'ResetPasswordRequest';
DELETE FROM `system_message_event` WHERE type = 'AccountSuspended';

INSERT INTO `config` (config_key, config_value, model_name, model_id) 
	SELECT  'MessageTemplate', channel_group.code, 'channel_group', channel_group.id
	FROM channel_group;

INSERT INTO `config` (config_key, config_value, model_name, model_id)
	SELECT  'MessageTemplate', channel.code, 'channel', channel.id
	FROM channel
	WHERE channel.code = 'QuotifyFunerals';


UPDATE match_rule SET name='Lep_Match_MatchExact',description='Filter out providers who haven\'t got the same options that are indicated by the consumer for the tagged questions.' WHERE id=5;
UPDATE match_rule SET name='Lep_Match_MatchPartial',description='Filter out providers who haven\'t got at least 1 of the options that are indicated by the consumer. Note this filter assumes that if a provider has not selected an option, then all options will apply.' WHERE id=11;
UPDATE tag SET description='Identifies questions used with Lep_Match_MatchExact match rules' WHERE id=4;
UPDATE tag SET name='Match Partial',description='Identifies questions used with Lep_Match_MatchPartial match rules' WHERE id=44;
DELETE FROM match_rule WHERE id IN (4,7);

DELETE FROM tag WHERE name IN ('Option Exact','Match Min 1','Provider Only');

ALTER TABLE user ADD email varchar(250) AFTER real_name;

INSERT INTO acl_resource VALUES (100,'SaveQueueRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,100,1,NOW());
INSERT INTO acl_resource VALUES (101,'GetQueueListRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,101,1,NOW());

CREATE TABLE `queue` (
  `id` int(11) NOT NULL auto_increment,
  `status` enum('processing','failed','processed','unprocessed') NOT NULL default 'unprocessed',
  `notification_email` varchar(250) default NULL,
  `data` longblob NOT NULL,
  `type` enum('KmlImport','ProviderImport') NOT NULL,
  `description` varchar(250) default NULL,
  `failure_reason` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 COMMENT='Generic queue bucket for LEP';

INSERT INTO acl_resource VALUES (102,'ExportLocationsRQ',NOW());
INSERT INTO acl_role_permission VALUES ('','allow',null,102,1,NOW());