# CREATE ARCHIVE DATABASE 
CREATE DATABASE lep3_archive;
CREATE TABLE lep3_archive.json_session_archive engine=ARCHIVE SELECT * FROM lep3.json_session LIMIT 0;
CREATE TABLE lep3_archive.unmatched_campaign_archive engine=ARCHIVE SELECT * FROM lep3.unmatched_campaign LIMIT 0;
CREATE TABLE lep3_archive.api_transaction_archive engine=ARCHIVE SELECT * FROM lep3.api_transaction LIMIT 0;
CREATE TABLE lep3_archive.message_audit_archive engine=ARCHIVE SELECT * FROM lep3.message_audit LIMIT 0;
CREATE TABLE lep3_archive.audit_log_archive engine=ARCHIVE SELECT * FROM lep3.audit_log LIMIT 0;
CREATE TABLE lep3_archive.applied_match_rule_archive engine=ARCHIVE SELECT * FROM lep3.applied_match_rule LIMIT 0;


GRANT USAGE ON *.* TO 'archiver'@'localhost' IDENTIFIED BY PASSWORD '5bde8d845d869293';
GRANT SELECT, DELETE, INSERT ON `lep3`.`json_session` TO 'archiver'@'localhost';
GRANT SELECT, INSERT ON `lep3_archive`.`json_session_archive` TO 'archiver'@'localhost';
GRANT SELECT, DELETE, INSERT ON `lep3`.`api_transaction` TO 'archiver'@'localhost';
GRANT SELECT, INSERT ON `lep3_archive`.`api_transaction_archive` TO 'archiver'@'localhost';
GRANT SELECT, DELETE, INSERT ON `lep3`.`unmatched_campaign` TO 'archiver'@'localhost';
GRANT SELECT, INSERT ON `lep3_archive`.`unmatched_campaign_archive` TO 'archiver'@'localhost';
GRANT SELECT, DELETE, INSERT ON `lep3`.`message_audit` TO 'archiver'@'localhost';
GRANT SELECT, INSERT ON `lep3_archive`.`message_audit_archive` TO 'archiver'@'localhost';
GRANT SELECT, DELETE, INSERT ON `lep3`.`audit_log` TO 'archiver'@'localhost';
GRANT SELECT, INSERT ON `lep3_archive`.`audit_log_archive` TO 'archiver'@'localhost';
GRANT SELECT, DELETE, INSERT ON `lep3`.`applied_match_rule` TO 'archiver'@'localhost';
GRANT SELECT, INSERT ON `lep3_archive`.`applied_match_rule_archive` TO 'archiver'@'localhost';


ALTER TABLE provider ADD is_capped TINYINT(1) DEFAULT 0,
                     ADD cap_reason VARCHAR(100);
			         
ALTER TABLE campaign ADD is_capped TINYINT(1) DEFAULT 0,
                     ADD cap_reason VARCHAR(100);
			    
DROP TABLE IF EXISTS audit_log_old;


DELETE FROM service_area WHERE id IN (
SELECT id FROM (
SELECT DISTINCT(sa.id),sa.campaign_id FROM service_area sa
JOIN ( SELECT sa.campaign_id,c.provider_id FROM service_area sa 
JOIN campaign c on c.id = sa.campaign_id GROUP BY CONCAT_WS(',',campaign_id,question_id,postcode) HAVING COUNT(sa.id) > 1) t ON t.campaign_id = sa.campaign_id WHERE t.campaign_id = sa.campaign_id AND sa.provider_id != t.provider_id
) x );

ALTER TABLE service_area DROP KEY `provider_id_question_id`,
                         DROP KEY `idx_unique_location`,
                         ADD UNIQUE (`campaign_id`, `question_id`, `postcode`);
