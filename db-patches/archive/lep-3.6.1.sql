INSERT INTO system_message_event(id, type, name, description, applicable_delivery_methods, token_description)
VALUES (14, 'Held', 'Held', 'Fires whenever an account or campaign goes on hold.', 'email', '<b>$contact_name</b>  (Provider contact name)
<b>$email</b>  (Provider contact email address)
<b>$provider_name</b> (Provider account name)
<b>$provider_id</b> (Provider Id)
<b>$provider_key</b> (Provider key)
<b>$campaign_id</b> (Campaign Id, if applicable)
<b>$campaign_name</b> (Campaign Name, if applicable)
<b>$has_dates</b> (True if onhold action is the result of dates being entered)
<b>$from_date</b> (On hold from date, if applicable)
<b>$to_date</b> (On hold to date, if applicable)
<b>$description</b> (On hold description, if applicable)');

INSERT INTO system_message_event(id, type, name, description, applicable_delivery_methods, token_description)
VALUES (15, 'UnHeld', 'Un Held', 'Fires whenever an account or campaign comes off held.', 'email', '<b>$contact_name</b>  (Provider contact name)
<b>$email</b>  (Provider contact email address)
<b>$provider_name</b> (Provider account name)
<b>$provider_id</b> (Provider Id)
<b>$provider_key</b> (Provider key)
<b>$campaign_id</b> (Campaign Id, if applicable)
<b>$campaign_name</b> (Campaign Name, if applicable)');


ALTER TABLE message_template  ADD PRIMARY KEY(id);




