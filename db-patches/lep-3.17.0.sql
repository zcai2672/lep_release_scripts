/** REQUEST: Stop BIDDING price *************************************************/
/** STEP 1 - MODIFY TABLE lep.vertical */

	ALTER TABLE `lep3`.`vertical` 
	ADD COLUMN `status_bidding` ENUM('active','inactive') NULL DEFAULT 'active' AFTER `client_id`;


/** STEP 2 - Replace VERTICAL_ID for the Vertical you need to stop from bidding */

	UPDATE `question` as q
		join `option` as o on o.question_id = q.id
		join `campaign_profile` as c on c.option_id=o.id
	SET	c.price = o.default_price
	WHERE q.vertical_id='VERTICAL_ID';



/** REQUEST: Add Review feedback to ACL ******************************************/
/** STEP 1 - ALTER table */

INSERT INTO user_system_roles (role_title) values('no-review-change');


