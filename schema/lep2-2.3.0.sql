
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `acl_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_resource` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `acl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_role` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `acl_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_role_permission` (
  `id` int(11) NOT NULL auto_increment,
  `permission` enum('allow','deny') NOT NULL,
  `privilege` enum('view','delete','insert','update','select') default NULL,
  `acl_resource_id` int(11) default NULL,
  `acl_role_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_acl_role_permission_acl_resource` (`acl_resource_id`),
  KEY `fk_acl_role_permission_acl_role` (`acl_role_id`),
  CONSTRAINT `fk_acl_role_permission_acl_resource` FOREIGN KEY (`acl_resource_id`) REFERENCES `acl_resource` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_acl_role_permission_acl_role` FOREIGN KEY (`acl_role_id`) REFERENCES `acl_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL auto_increment,
  `type` enum('billing','street') NOT NULL,
  `address1` varchar(255) default NULL,
  `address2` varchar(255) default NULL,
  `city` varchar(255) default NULL,
  `region` varchar(255) default NULL,
  `state` varchar(100) default NULL,
  `postcode` varchar(20) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `api_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_transaction` (
  `id` int(11) NOT NULL auto_increment,
  `nonce` varchar(250) NOT NULL,
  `application_identity_id` int(11) NOT NULL,
  `ip` varchar(15) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `expiry_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  KEY `nonce_key` (`nonce`),
  KEY `fk_application_identity` (`application_identity_id`),
  CONSTRAINT `fk_application_identity` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `application_identity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_identity` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `real_name` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_client_id` (`client_id`),
  CONSTRAINT `fk_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `application_identity_acl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_identity_acl_role` (
  `application_identity_id` int(11) default NULL,
  `acl_role_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`application_identity_id`,`acl_role_id`),
  KEY `fk_application_identity_acl_role_application_identity` (`application_identity_id`),
  KEY `fk_application_identity_acl_role_acl_role` (`acl_role_id`),
  CONSTRAINT `fk_application_identity_acl_role_acl_role` FOREIGN KEY (`acl_role_id`) REFERENCES `acl_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_application_identity_acl_role_application_identity` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `application_identity_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_identity_channel` (
  `application_identity_id` int(11) default NULL,
  `channel_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`application_identity_id`,`channel_id`),
  KEY `fk_application_identity_channel_application_identity` (`application_identity_id`),
  KEY `fk_application_identity_channel_channel` (`channel_id`),
  CONSTRAINT `fk_application_identity_channel_application_identity` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_application_identity_channel_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `applied_match_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applied_match_rule` (
  `id` int(11) NOT NULL auto_increment,
  `match_rule_id` int(11) NOT NULL,
  `job_spec_id` int(11) NOT NULL,
  `question_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_match_rule` (`match_rule_id`),
  KEY `fk_job_spec` (`job_spec_id`),
  CONSTRAINT `fk_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_rule` FOREIGN KEY (`match_rule_id`) REFERENCES `match_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=302 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `audit_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_log` (
  `id` int(11) NOT NULL auto_increment,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `field` varchar(100) NOT NULL,
  `old` varchar(255) default NULL,
  `new` varchar(255) default NULL,
  `app_identity` int(11) default NULL,
  `audit_message` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_model` (`model`),
  KEY `idx_model_id` (`model_id`),
  KEY `idx_application_identity` (`app_identity`)
) ENGINE=InnoDB AUTO_INCREMENT=8442 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `backfill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backfill` (
  `id` int(11) NOT NULL auto_increment,
  `job_spec_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `job_spec_id` (`job_spec_id`),
  KEY `provider_id` (`provider_id`),
  CONSTRAINT `backfill_ibfk_1` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`),
  CONSTRAINT `backfill_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_group` (
  `id` int(11) NOT NULL auto_increment,
  `contact_id` int(11) default NULL,
  `address_id` int(11) default NULL,
  `credit_card_id` int(11) default NULL,
  `description` text,
  `type` enum('credit card','invoice') NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL default NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `fk_contact_id` (`contact_id`),
  KEY `fk_address_id` (`address_id`),
  KEY `fk_credit_card_id` (`credit_card_id`),
  CONSTRAINT `fk_address_id` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `fk_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`),
  CONSTRAINT `fk_credit_card_id` FOREIGN KEY (`credit_card_id`) REFERENCES `credit_card` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_group_contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_group_contact_media` (
  `billing_group_id` int(11) NOT NULL,
  `contact_media_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  KEY `fk_billing_group_id` (`billing_group_id`),
  KEY `fk_contact_media_id` (`contact_media_id`),
  CONSTRAINT `fk_biling_group_id` FOREIGN KEY (`billing_group_id`) REFERENCES `billing_group` (`id`),
  CONSTRAINT `fk_contact_media_id` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `code` varchar(50) default NULL,
  `channel_group_id` int(11) default NULL,
  `message_template_group_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `status` enum('inactive','staging','active') default 'staging',
  `parent_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`),
  KEY `fk_channel_group_id` (`channel_group_id`),
  KEY `fk_channel_message_template_group_id` (`message_template_group_id`),
  CONSTRAINT `fk_channel_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_channel_group_id` FOREIGN KEY (`channel_group_id`) REFERENCES `channel_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_filter` (
  `id` int(11) NOT NULL auto_increment,
  `filter_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `params` text NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_channel_id_filter_id` (`filter_id`,`channel_id`),
  KEY `fk_filter_id` (`filter_id`),
  KEY `fk_channel_id` (`channel_id`),
  CONSTRAINT `fk_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_filter_id` FOREIGN KEY (`filter_id`) REFERENCES `registration_filter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=536 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_group` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `message_template_group_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_code` (`code`),
  KEY `fk_channel_group_message_template_group_id` (`message_template_group_id`),
  CONSTRAINT `fk_channel_group_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_option` (
  `id` int(11) NOT NULL auto_increment,
  `channel_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_label` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_channel_option_channel_id` (`channel_id`),
  KEY `fk_channel_option_option_id` (`option_id`),
  CONSTRAINT `fk_channel_option_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`),
  CONSTRAINT `fk_channel_option_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5794 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_question` (
  `id` int(11) NOT NULL auto_increment,
  `channel_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `label` varchar(255) default NULL,
  `description` text,
  `page` int(11) NOT NULL default '1',
  `is_hidden` tinyint(1) NOT NULL default '0',
  `default_value` varchar(50) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_channel_question_channel` (`channel_id`),
  KEY `fk_channel_question_question` (`question_id`),
  CONSTRAINT `fk_channel_question_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_channel_question_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3031 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_vertical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_vertical` (
  `vertical_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`vertical_id`,`channel_id`),
  KEY `fk_channel_vertical_vertical` (`vertical_id`),
  KEY `fk_channel_vertical_channel` (`channel_id`),
  CONSTRAINT `fk_channel_vertical_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_channel_vertical_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `message_template_group_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_client_message_template_group_id` (`message_template_group_id`),
  CONSTRAINT `fk_client_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(11) NOT NULL auto_increment,
  `config_key` varchar(45) NOT NULL,
  `config_value` varchar(255) NOT NULL,
  `model_name` varchar(45) NOT NULL,
  `model_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`config_key`,`model_name`,`model_id`),
  UNIQUE KEY `pk_id` (`id`),
  KEY `idx_config_key` (`config_key`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1 COMMENT='This table stores configuration values for various models';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `consumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(250) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `consumer_key` varchar(32) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_consumer_key` (`consumer_key`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `consumer_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_feedback` (
  `id` int(11) NOT NULL auto_increment,
  `project_id` int(11) NOT NULL,
  `consumer_id` int(11) NOT NULL,
  `provider_id` int(11) default NULL,
  `provider_review_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_cf_project_id` (`project_id`),
  KEY `fk_cf_consumer_id` (`consumer_id`),
  KEY `fk_cf_provider_id` (`provider_id`),
  KEY `fk_cf_provider_review_id` (`provider_review_id`),
  CONSTRAINT `fk_cf_consumer_id` FOREIGN KEY (`consumer_id`) REFERENCES `consumer` (`id`),
  CONSTRAINT `fk_cf_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `fk_cf_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_cf_provider_review_id` FOREIGN KEY (`provider_review_id`) REFERENCES `provider_review` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `consumer_question_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_question_answer` (
  `id` int(11) NOT NULL auto_increment,
  `consumer_id` int(11) NOT NULL,
  `project_id` int(11) default NULL,
  `question_id` int(11) default NULL,
  `option_code` varchar(10) default NULL,
  `text_value` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_consumer_id` (`consumer_id`),
  KEY `idx_project_id` (`project_id`),
  KEY `idx_question_id` (`question_id`),
  CONSTRAINT `fk_consumer_id` FOREIGN KEY (`consumer_id`) REFERENCES `consumer` (`id`),
  CONSTRAINT `fk_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `fk_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `consumer_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_request` (
  `id` int(11) NOT NULL auto_increment,
  `request_key` varchar(32) NOT NULL,
  `status` enum('pending','inactive','active') NOT NULL default 'active',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_request_key` (`request_key`),
  KEY `idx_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `consumer_request_filter_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_request_filter_history` (
  `id` int(11) NOT NULL auto_increment,
  `consumer_request_id` int(11) NOT NULL,
  `question_id` int(11) default NULL,
  `registration_filter_id` int(11) default NULL,
  `description` text NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_consumer_request_id` (`consumer_request_id`),
  KEY `fk_registration_filter_id` (`registration_filter_id`),
  CONSTRAINT `fk_consumer_request_id` FOREIGN KEY (`consumer_request_id`) REFERENCES `consumer_request` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registration_filter_id` FOREIGN KEY (`registration_filter_id`) REFERENCES `registration_filter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL auto_increment,
  `first_name` varchar(100) default NULL,
  `last_name` varchar(100) default NULL,
  `email` varchar(250) default NULL,
  `type` enum('primary','billing','lead') NOT NULL,
  `password` varchar(50) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_first_name` (`first_name`),
  KEY `idx_last_name` (`last_name`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_media` (
  `id` int(11) NOT NULL auto_increment,
  `number` varchar(50) NOT NULL,
  `is_displayable` tinyint(4) NOT NULL default '1',
  `type` enum('mobile','fax','phone') NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `purpose` enum('billing','primary','display','lead') NOT NULL default 'primary',
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=298 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `credit_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_card` (
  `id` int(11) NOT NULL auto_increment,
  `type` enum('visa','mastercard','amex','diners') NOT NULL,
  `number` varchar(300) NOT NULL,
  `name` text NOT NULL,
  `expiry_month` smallint(2) NOT NULL,
  `expiry_year` smallint(4) NOT NULL,
  `ccv` varchar(5) NOT NULL,
  `created_date` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_identifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_identifier` (
  `id` int(11) NOT NULL auto_increment,
  `model_name` varchar(45) NOT NULL,
  `model_id` int(11) NOT NULL,
  `external_id` varchar(100) NOT NULL,
  `external_system` varchar(20) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_review` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `authority` decimal(5,2) NOT NULL default '0.00',
  `source` varchar(20) NOT NULL,
  `teaser` varchar(255) default NULL,
  `author` varchar(50) default NULL,
  `provider_review_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_provider_id` USING BTREE (`provider_id`),
  KEY `fk_pr_provider_preview_id` (`provider_review_id`),
  CONSTRAINT `fk_er_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_pr_provider_preview_id` FOREIGN KEY (`provider_review_id`) REFERENCES `provider_review` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Review collated about providers from external sources';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `feedback_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_answer` (
  `id` int(11) NOT NULL auto_increment,
  `question_id` int(11) default NULL,
  `option_code` varchar(10) default NULL,
  `option_id` int(11) default NULL,
  `consumer_feedback_id` int(11) default NULL,
  `text_value` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_question_id` (`question_id`),
  KEY `fk_fa_option_id` (`option_id`),
  KEY `fk_fa_consumer_feedback_id` (`consumer_feedback_id`),
  CONSTRAINT `fk_fa_consumer_feedback_id` FOREIGN KEY (`consumer_feedback_id`) REFERENCES `consumer_feedback` (`id`),
  CONSTRAINT `fk_fa_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`),
  CONSTRAINT `fk_fa_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `job_spec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_spec` (
  `id` int(11) NOT NULL auto_increment,
  `session_id` varchar(50) default NULL,
  `vertical_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `is_preview` tinyint(4) NOT NULL default '0' COMMENT 'Flags whether the job spec was created in a preview function.',
  `is_exclusive_match` tinyint(4) NOT NULL default '0' COMMENT 'Flags whether this is an exclusive match job spec',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `consumer_request_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_job_spec_vertical` (`vertical_id`),
  KEY `fk_consumer_request_id` (`consumer_request_id`),
  KEY `pk_id` (`id`),
  CONSTRAINT `fk_consumer_request` FOREIGN KEY (`consumer_request_id`) REFERENCES `consumer_request` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_spec_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `job_spec_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_spec_detail` (
  `id` int(11) NOT NULL auto_increment,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) default NULL,
  `job_spec_id` int(11) NOT NULL,
  `text_value` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_job_spec_detail_job_spec` (`job_spec_id`),
  CONSTRAINT `fk_job_spec_detail_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2525 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `json_publisher_identity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `json_publisher_identity` (
  `id` int(11) NOT NULL auto_increment,
  `application_identity_id` int(11) NOT NULL,
  `publisher_id` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `publisher_id` (`publisher_id`),
  KEY `fk_application_identity_id` (`application_identity_id`),
  CONSTRAINT `fk_application_identity_id` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `json_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `json_session` (
  `id` int(11) NOT NULL auto_increment,
  `session_id` varchar(100) default NULL,
  `type` enum('question','auth') NOT NULL,
  `data_key` varchar(50) default NULL,
  `data` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_sessionid_key` (`session_id`,`data_key`)
) ENGINE=InnoDB AUTO_INCREMENT=7600 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `lead_credit_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead_credit_request` (
  `match_set_id` int(11) NOT NULL default '0',
  `status` enum('Declined','Approved','Pending') default 'Pending',
  `comment` varchar(255) default NULL,
  `description` text,
  `internal_description` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL default NULL,
  PRIMARY KEY  (`match_set_id`),
  KEY `fk_match_set_id` (`match_set_id`),
  KEY `idx_status` (`status`),
  CONSTRAINT `fk_match_set_id` FOREIGN KEY (`match_set_id`) REFERENCES `match_set` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL auto_increment,
  `label` varchar(100) default '',
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) default NULL,
  `type` enum('postcode','area') NOT NULL default 'area',
  `left_id` int(11) default NULL,
  `right_id` int(11) default NULL,
  `latitude` varchar(10) default NULL,
  `longitude` varchar(10) default NULL,
  `points_encoded` blob,
  `levels_encoded` blob,
  `alt_label` varchar(100) default NULL COMMENT 'An alternative name for the location',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  `postcode` int(4) default NULL,
  `level` enum('suburb','region','metro','regional','state','country') default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `idx_name` (`name`),
  KEY `idx_postocde` (`postcode`)
) ENGINE=InnoDB AUTO_INCREMENT=130670 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `match_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text,
  `status` enum('inactive','active') NOT NULL default 'active',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `match_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_rule` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) default NULL,
  `type` enum('backfill','provider') NOT NULL default 'provider',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `match_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_set` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `job_spec_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `is_matched` tinyint(4) NOT NULL default '0',
  `ranking_final_score` decimal(5,2) default '0.00',
  PRIMARY KEY  (`id`),
  KEY `fk_match_set_job_spec` (`job_spec_id`),
  KEY `fk_provider_id` (`provider_id`),
  CONSTRAINT `fk_match_set_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=588 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `message_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_audit` (
  `id` int(11) NOT NULL auto_increment,
  `message_event_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `delivery_method` enum('api','sms','fax','email') default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_message_audit_message_event` (`message_event_id`),
  CONSTRAINT `fk_message_audit_message_event` FOREIGN KEY (`message_event_id`) REFERENCES `message_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=710 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `message_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_event` (
  `id` int(11) NOT NULL auto_increment,
  `system_message_event_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `parent_id` int(11) default NULL,
  `key_id` int(11) default NULL,
  `status` enum('failed','processed','unprocessed') default 'unprocessed',
  `failure_reason` varchar(200) default NULL,
  `trigger_date` timestamp NULL default NULL COMMENT 'Future date for when message should be sent',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_message_event_system_message_event` (`system_message_event_id`),
  KEY `fk_parent_id` (`parent_id`),
  KEY `fk_key_id` (`key_id`),
  CONSTRAINT `fk_message_event_system_message_event` FOREIGN KEY (`system_message_event_id`) REFERENCES `system_message_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `message_event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=923 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `message_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_template` (
  `id` int(11) NOT NULL auto_increment,
  `message_template_group_id` int(11) NOT NULL,
  `system_message_event_id` int(11) NOT NULL,
  `delivery_method` enum('email','fax','sms') NOT NULL default 'email',
  `template` text,
  `from_email` varchar(255) default NULL,
  `from_name` varchar(100) default NULL,
  `subject` varchar(255) default NULL,
  `status` enum('active','inactive') default 'active',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_system_message_event_id` (`system_message_event_id`),
  KEY `fk_message_template_group_id` (`message_template_group_id`),
  CONSTRAINT `fk_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`),
  CONSTRAINT `fk_system_message_event_id` FOREIGN KEY (`system_message_event_id`) REFERENCES `system_message_event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `message_template_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_template_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `status` enum('active','inactive') default 'active',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Message template groups defined in the system';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `option` (
  `id` int(11) NOT NULL auto_increment,
  `label` varchar(255) default NULL,
  `real_value` int(11) default NULL,
  `question_id` int(11) default NULL,
  `code` varchar(20) NOT NULL,
  `sequence` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `parent_id` int(11) default NULL,
  `provider_description` text,
  `hint` varchar(100) default NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `default_price` decimal(10,2) NOT NULL default '0.00',
  `default_price_type` enum('$','%') NOT NULL default '$',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_code` (`code`),
  KEY `fk_options_question` (`question_id`),
  KEY `fk_parent_id_option_id` (`parent_id`),
  CONSTRAINT `fk_options_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_id_option_id` FOREIGN KEY (`parent_id`) REFERENCES `option` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `option_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `option_tag` (
  `tag_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`tag_id`,`option_id`),
  KEY `fk_option_tag_tag` (`tag_id`),
  KEY `fk_option_tag_option` (`option_id`),
  CONSTRAINT `fk_option_tag_option` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_option_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `profile_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_filter` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `profile_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `new_job` tinyint(4) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `vertical_id` int(11) default NULL,
  `description` text NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  PRIMARY KEY  (`id`),
  KEY `fk_vertical_id` (`vertical_id`),
  CONSTRAINT `fk_vertical_id` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `profile_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_rule` (
  `id` int(11) NOT NULL auto_increment,
  `target_question_id` int(11) NOT NULL,
  `target_option_id` int(11) default NULL,
  `min_value` int(11) default NULL,
  `max_value` int(11) default NULL,
  `dependency` int(11) default NULL,
  `type` enum('equal','sum') NOT NULL default 'equal',
  `profile_group_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_profile_rule_question` (`target_question_id`),
  KEY `fk_profile_rule_option` (`target_option_id`),
  CONSTRAINT `fk_profile_rule_option` FOREIGN KEY (`target_option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `profile_rule_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_rule_attribute` (
  `id` int(11) NOT NULL auto_increment,
  `profile_rule_id` int(11) default NULL,
  `source_question_id` int(11) default NULL,
  `source_option_id` int(11) default NULL,
  `filter_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_profile_rule_attribute_profile_rule` (`profile_rule_id`),
  KEY `fk_profile_rule_attribute_profile_question` (`source_question_id`),
  KEY `fk_profile_rule_attribute_profile_option` (`source_option_id`),
  KEY `fk_profile_rule_attribute_profile_filter` (`filter_id`),
  CONSTRAINT `fk_profile_rule_attribute_profile_filter` FOREIGN KEY (`filter_id`) REFERENCES `profile_filter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_rule_attribute_profile_option` FOREIGN KEY (`source_option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_rule_attribute_profile_question` FOREIGN KEY (`source_question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `profile_rule_attribute_ibfk_1` FOREIGN KEY (`profile_rule_id`) REFERENCES `profile_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL auto_increment,
  `job_spec_id` int(11) NOT NULL,
  `project_key` varchar(32) NOT NULL,
  `status` enum('Matched','Sub-matched','Pending','Unmatched') default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `address_id` int(11) default NULL,
  `first_name` varchar(100) default NULL,
  `last_name` varchar(100) default NULL,
  `consumer_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_project_job_spec` (`job_spec_id`),
  KEY `fk_project_address` (`address_id`),
  KEY `pk_id` (`id`),
  KEY `idx_project_key` (`project_key`),
  KEY `idx_consumer_name_composite` (`first_name`,`last_name`),
  KEY `idx_status` (`status`),
  KEY `fk_consumer_id` (`consumer_id`),
  CONSTRAINT `consumer_id` FOREIGN KEY (`consumer_id`) REFERENCES `consumer` (`id`),
  CONSTRAINT `fk_project_address` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `project_contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_contact_media` (
  `project_id` int(11) NOT NULL,
  `contact_media_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`project_id`,`contact_media_id`),
  KEY `fk_project_contact_media_project` (`project_id`),
  KEY `fk_project_contact_media_contact_media` (`contact_media_id`),
  CONSTRAINT `project_contact_media_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `project_contact_media_ibfk_2` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `is_held` tinyint(4) NOT NULL default '0',
  `status` enum('active','deleted','cancelled','pending','suspended','backfill') NOT NULL,
  `ranking_adjustment` decimal(5,2) NOT NULL default '1.00',
  `name_string` varchar(100) default NULL,
  `address_string` varchar(100) default NULL,
  `phone_string` varchar(50) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL default NULL,
  `match_group_id` int(11) default NULL,
  `billing_group_id` int(11) default NULL,
  `provider_key` varchar(32) NOT NULL,
  `rating_score` decimal(5,2) default '0.00',
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`),
  KEY `fk_billing_group_id` (`billing_group_id`),
  KEY `fk_match_group` (`match_group_id`),
  CONSTRAINT `fk_billing_group_id` FOREIGN KEY (`billing_group_id`) REFERENCES `billing_group` (`id`),
  CONSTRAINT `fk_match_group` FOREIGN KEY (`match_group_id`) REFERENCES `match_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_address` (
  `provider_id` int(11) default NULL,
  `address_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`provider_id`,`address_id`),
  KEY `fk_provider_address_provider` (`provider_id`),
  KEY `fk_provider_address_address` (`address_id`),
  CONSTRAINT `provider_address_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `provider_address_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_cap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_cap` (
  `id` int(11) NOT NULL auto_increment,
  `daily_lead_cap` int(11) NOT NULL default '0',
  `provider_id` int(11) NOT NULL,
  `weekly_lead_cap` int(11) NOT NULL default '0',
  `monthly_lead_cap` int(11) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `total_lead_cap` int(11) NOT NULL default '0',
  `total_spend_cap` decimal(7,2) default NULL,
  `free_lead_cap` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `provider_id` (`provider_id`),
  KEY `fk_provider_cap_provider` (`provider_id`),
  CONSTRAINT `provider_cap_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_charge` (
  `id` int(11) NOT NULL auto_increment,
  `billing_group_id` int(11) default NULL,
  `provider_id` int(11) NOT NULL,
  `type` enum('lead','lead-credit','misc','misc-credit','subscription','subscription-credit') NOT NULL,
  `amount` double(10,2) default NULL,
  `description` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `key_id` int(11) default NULL,
  `key_type` enum('job-spec','subscription') default NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_billing_group_id` (`billing_group_id`),
  KEY `idx_provider_id` (`provider_id`),
  KEY `idx_key_id` (`key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=341 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_contact` (
  `provider_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`provider_id`,`contact_id`),
  KEY `fk_providers_contacts_providers` (`provider_id`),
  KEY `fk_providers_contacts_contacts` (`contact_id`),
  CONSTRAINT `provider_contact_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `provider_contact_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_contact_media` (
  `contact_media_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`provider_id`,`contact_media_id`),
  KEY `fk_provider_contact_contact_media` (`contact_media_id`),
  KEY `fk_provider_contact_media_provider` (`provider_id`),
  CONSTRAINT `provider_contact_media_ibfk_1` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `provider_contact_media_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_lead_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_lead_price` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `type` enum('$','%') NOT NULL default '$',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_provider_lead_price_provider` (`provider_id`),
  KEY `fk_provider_lead_price_option` (`option_id`),
  KEY `fk_provider_lead_price_question` (`question_id`),
  CONSTRAINT `fk_provider_lead_price_option` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provider_lead_price_provider` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provider_lead_price_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_message_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_message_delivery` (
  `id` int(11) NOT NULL auto_increment,
  `system_message_event_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `delivery_method` enum('fax','sms','email','api') NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_provider_message_delivery_system_message_event` (`system_message_event_id`),
  KEY `fk_provider_message_delivery_provider` (`provider_id`),
  CONSTRAINT `fk_provider_message_delivery_provider` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provider_message_delivery_system_message_event` FOREIGN KEY (`system_message_event_id`) REFERENCES `system_message_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_message_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_message_params` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) default NULL,
  `params` text,
  `created_date` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_provider_id` (`provider_id`),
  CONSTRAINT `fkprovider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_onhold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_onhold` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `description` text,
  `active` tinyint(1) default '1',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_provider_id` (`provider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_question` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `preference` decimal(5,2) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_provider_question_provider` (`provider_id`),
  KEY `fk_provider_question_question` (`question_id`),
  KEY `fk_provider_question_option` (`option_id`),
  CONSTRAINT `fk_provider_question_option` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provider_question_provider` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provider_question_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_review` (
  `id` int(11) NOT NULL auto_increment,
  `type` enum('external','internal') NOT NULL,
  `provider_id` int(11) NOT NULL,
  `project_id` int(11) default NULL,
  `score` decimal(5,2) NOT NULL default '0.00',
  `status` enum('active','inactive') NOT NULL default 'active',
  `comment` text NOT NULL,
  `response` text NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_pr_provider_id` (`provider_id`),
  KEY `fk_pr_project_id` (`project_id`),
  CONSTRAINT `fk_pr_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `fk_pr_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='An aggregate table of internal and external provider reviews';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_service_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_service_area` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `question_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `idx_unique_location` (`provider_id`,`question_id`,`postcode`),
  KEY `fk_provider_service_area_provider` (`provider_id`),
  KEY `fk_provider_service_area_question` (`question_id`),
  KEY `idx_postcode` (`postcode`),
  KEY `provider_id_question_id` (`provider_id`,`question_id`),
  CONSTRAINT `fk_provider_service_area_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `provider_service_area_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_subscription` (
  `id` int(11) NOT NULL auto_increment,
  `service_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `subscription_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `amount` decimal(10,2) NOT NULL default '0.00',
  `auto_renew` tinyint(1) NOT NULL default '1',
  `cancellation_date` timestamp NULL default NULL,
  `billing_period` enum('Annually','Monthly') NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_service_id` (`service_id`),
  KEY `fk_providerid` (`provider_id`),
  CONSTRAINT `fk_providerid` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_service_id` FOREIGN KEY (`service_id`) REFERENCES `subscription_service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_subscription_charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_subscription_charge` (
  `id` int(11) NOT NULL auto_increment,
  `provider_subscription_id` int(11) NOT NULL,
  `charge_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_provider_subscription_id` (`provider_subscription_id`),
  KEY `fk_charge_id` (`charge_id`),
  CONSTRAINT `fk_chargeid` FOREIGN KEY (`charge_id`) REFERENCES `provider_charge` (`id`),
  CONSTRAINT `fk_charge_id` FOREIGN KEY (`charge_id`) REFERENCES `provider_charge` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_vertical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_vertical` (
  `provider_id` int(11) NOT NULL,
  `vertical_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`vertical_id`,`provider_id`),
  KEY `fk_providers_verticals_providers` (`provider_id`),
  KEY `fk_providers_verticals_verticals` (`vertical_id`),
  CONSTRAINT `fk_providers_verticals_verticals` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `provider_vertical_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL auto_increment,
  `label` varchar(255) default NULL,
  `description` text,
  `type` enum('text','select','location','radio','checkbox','rating','textarea','display','location-specific','slider','date','password') NOT NULL,
  `vertical_id` int(11) default NULL,
  `is_mandatory` tinyint(4) NOT NULL default '0',
  `status` enum('active','inactive') NOT NULL default 'active',
  `code` varchar(20) NOT NULL,
  `internal_description` text,
  `provider_description` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `min_value` int(11) default NULL,
  `min_label` varchar(100) default NULL,
  `max_value` int(11) default NULL,
  `max_label` varchar(100) default NULL,
  `help` text,
  `help_url` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_code` (`code`),
  KEY `fk_question_vertical` (`vertical_id`),
  KEY `pk_id` (`id`),
  CONSTRAINT `fk_question_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `question_option_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_option_tree` (
  `id` int(11) NOT NULL auto_increment,
  `parent_option_id` int(11) default NULL,
  `child_question_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `is_mandatory` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `fk_question_option_tree_option` (`parent_option_id`),
  KEY `fk_question_option_tree_question` (`child_question_id`),
  KEY `fk_question_option_tree_channel` (`channel_id`),
  CONSTRAINT `fk_question_option_tree_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_option_tree_option` FOREIGN KEY (`parent_option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_option_tree_question` FOREIGN KEY (`child_question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3276 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `question_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_tag` (
  `question_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`tag_id`,`question_id`),
  KEY `fk_question_tag_question` (`question_id`),
  KEY `fk_question_tag_tag` (`tag_id`),
  CONSTRAINT `fk_question_tag_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` int(11) NOT NULL auto_increment,
  `status` enum('processing','failed','processed','unprocessed') NOT NULL default 'unprocessed',
  `notification_email` varchar(250) default NULL,
  `data` longblob NOT NULL,
  `type` enum('KmlImport','ProviderImport') NOT NULL,
  `description` varchar(250) default NULL,
  `failure_reason` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 COMMENT='Generic queue bucket for LEP';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ranking_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ranking_rule` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `parent_match_rule_id` int(11) default NULL,
  `description` varchar(255) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `registration_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_filter` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `subscription_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_service` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `code` varchar(20) NOT NULL,
  `attributes` text,
  `modified_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `system_message_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_message_event` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(100) NOT NULL,
  `name` varchar(100) default NULL,
  `description` varchar(255) default NULL,
  `applicable_delivery_methods` varchar(20) default NULL COMMENT 'Comma delimited list of delivery methods applicable for this system message event',
  `token_description` text COMMENT 'Describes tokens which are available to message templates derived from this system message event',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` text,
  `is_system_tag` tinyint(1) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL auto_increment,
  `question_id` int(11) default NULL,
  `option_id` int(11) default NULL,
  `provider_id` int(11) NOT NULL,
  `job_spec_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `percentage` decimal(10,2) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_transaction_question` (`question_id`),
  KEY `fk_transaction_option` (`option_id`),
  KEY `fk_transaction_provider` (`provider_id`),
  KEY `fk_transaction_job_spec` (`job_spec_id`),
  CONSTRAINT `fk_transaction_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_option` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_provider` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `unmatched_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unmatched_provider` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `job_spec_id` int(11) NOT NULL,
  `reason` varchar(200) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_unmatched_provider_job_spec` (`job_spec_id`),
  CONSTRAINT `fk_unmatched_provider_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(255) NOT NULL default '',
  `password` varchar(255) NOT NULL default '',
  `real_name` varchar(255) default '',
  `email` varchar(250) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL default '',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vertical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertical` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `feedback_days` int(11) NOT NULL default '0' COMMENT 'Number of days after a project submission when a feedback request email will be sent out',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vertical_match_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertical_match_rule` (
  `id` int(11) NOT NULL auto_increment,
  `match_rule_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL default '0',
  `vertical_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_key` (`match_rule_id`,`vertical_id`),
  KEY `fk_channel_match_rule_match_rule` (`match_rule_id`),
  KEY `fk_channel_match_rule_vertical` (`vertical_id`),
  CONSTRAINT `fk_channel_match_rule_match_rule` FOREIGN KEY (`match_rule_id`) REFERENCES `match_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_channel_match_rule_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vertical_question_option_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertical_question_option_tree` (
  `id` int(11) NOT NULL auto_increment,
  `parent_option_id` int(11) NOT NULL,
  `child_question_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_vertical_question_option_tree_option` (`parent_option_id`),
  KEY `fk_vertical_question_option_tree_question` (`child_question_id`),
  CONSTRAINT `fk_vertical_question_option_tree_option` FOREIGN KEY (`parent_option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vertical_question_option_tree_question` FOREIGN KEY (`child_question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vertical_ranking_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertical_ranking_rule` (
  `id` int(11) NOT NULL auto_increment,
  `ranking_rule_id` int(11) NOT NULL,
  `vertical_id` int(11) NOT NULL,
  `weight` int(11) NOT NULL COMMENT 'A weight from 1 to 100',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_vertical_ranking_attribute_ranking_attribute` (`ranking_rule_id`),
  KEY `fk_vertical_ranking_attribute_vertical` (`vertical_id`),
  CONSTRAINT `fk_vertical_ranking_attribute_ranking_rule` FOREIGN KEY (`ranking_rule_id`) REFERENCES `ranking_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vertical_ranking_attribute_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DELIMITER ;;
DELIMITER ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `acl_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_resource` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `acl_resource` WRITE;
/*!40000 ALTER TABLE `acl_resource` DISABLE KEYS */;
INSERT INTO `acl_resource` VALUES (1,'GetQuestionsRQ','0000-00-00 00:00:00'),(2,'RegisterConsumerRequestRQ','0000-00-00 00:00:00'),(3,'GetConsumerRQ','0000-00-00 00:00:00'),(4,'GetConsumerRequestRQ','0000-00-00 00:00:00'),(5,'GetProviderRQ','2008-12-22 07:15:42'),(6,'SaveProviderRQ','2008-12-23 03:42:06'),(7,'GetProvidersRQ','2008-12-23 05:03:51'),(8,'UpdateProviderStatusRQ','2008-12-30 08:58:51'),(9,'GetSystemQuestionsRQ','2009-01-05 06:55:14'),(10,'GetChannelRQ','2009-01-06 04:18:08'),(11,'SaveChannelRQ','2009-01-06 10:39:28'),(12,'PublishChannelChangesRQ','2009-01-08 06:59:48'),(13,'UpdateChannelStatusRQ','2009-01-08 08:31:36'),(14,'GetChannelsRQ','2009-01-08 11:24:43'),(15,'GetProviderOnHoldDatesRQ','2009-01-13 07:14:45'),(16,'DuplicateQuestionRQ','2009-01-13 11:21:01'),(17,'DuplicateChannelRQ','2009-01-13 11:21:45'),(18,'PreviewConsumerRequestRQ','2009-01-14 10:04:33'),(19,'GetVerticalRQ','2009-01-15 12:13:37'),(20,'GetVerticalsRQ','2009-01-15 12:13:42'),(21,'SaveVerticalRQ','2009-01-16 04:17:40'),(22,'UpdateVerticalStatusRQ','2009-01-16 12:05:04'),(23,'GetSystemRankingRulesRQ','2009-01-19 06:36:00'),(24,'GetSystemMatchRulesRQ','2009-01-19 06:36:10'),(25,'GetApplicationIdentitiesRQ','2009-01-19 06:36:20'),(26,'GetSystemQuestionTagsRQ','2009-02-26 06:33:36'),(27,'GetLeadsRQ','2009-02-26 06:33:37'),(28,'GetSubscriptionsRQ','2009-02-26 06:33:37'),(29,'GetFeedbackListRQ','2009-02-26 06:33:37'),(30,'GetQuestionOptionRQ','2009-04-14 06:25:33'),(31,'GetVolumeForecastRQ','2009-04-14 06:25:33'),(32,'SaveCreditRequestRQ','2009-03-05 04:02:18'),(33,'GetLocationListRQ','2009-04-14 06:25:34'),(34,'SaveConsumerFeedbackRQ','2009-04-14 06:25:33'),(35,'GetConsumerFeedbackRQ','2009-04-14 06:25:33'),(36,'GetConsumerProjectsRQ','2009-04-14 06:25:33'),(37,'GetConsumerByKeyRQ','2009-04-02 05:28:59'),(38,'GenerateMessageEventRQ','2009-04-02 05:29:10'),(39,'GetSystemQuestionTypesRQ','2009-04-03 08:58:08'),(40,'GetSystemTagsRQ','2009-04-03 09:49:33'),(41,'SaveQuestionOptionRQ','2009-04-06 07:01:51'),(42,'SaveQuestionRQ','2009-04-06 07:01:51'),(43,'DeleteQuestionOptionRQ','2009-04-06 08:17:26'),(44,'AssignTagsRQ','2009-04-06 08:17:34'),(45,'UnAssignTagsRQ','2009-04-06 10:35:48'),(46,'UpdateQuestionStatusRQ','2009-04-07 06:43:15'),(47,'GetProfileGroupsRQ','2009-04-08 06:10:27'),(48,'GetProviderSystemMessagesRQ','2009-04-14 06:25:40'),(49,'SaveProfileRuleRQ','2009-04-08 12:32:13'),(50,'DeleteProfileRuleRQ','2009-04-08 13:01:53'),(51,'DeleteProfileRuleSourceRQ','2009-04-09 08:28:43'),(52,'SaveProfileRuleSourceRQ','2009-04-09 08:40:59'),(53,'SaveProfileGroupRQ','2009-04-09 10:40:49'),(54,'ValidatePostcodeRQ','2009-04-21 06:35:26'),(55,'GetFeedbackQuestionsRQ','2009-05-06 07:26:44'),(56,'GetProviderChargesRQ','2009-05-06 07:26:45'),(57,'GetProjectsRQ','2009-05-06 07:26:45'),(58,'GetProjectRQ','2009-05-06 07:26:45'),(59,'AddProviderChargeRQ','2009-05-06 07:26:45'),(60,'GetConsumerRequestsRQ','2009-05-06 07:26:45'),(61,'UpdateConsumerRequestStatusRQ','2009-05-06 07:26:45'),(62,'GetCreditRequestsRQ','2009-05-06 07:26:45'),(63,'GetRegistrationFiltersRQ','2009-05-06 07:26:45'),(64,'AssignChannelFiltersRQ','2009-05-06 07:26:45'),(65,'GetMatchGroupsRQ','2009-05-22 04:54:23'),(66,'SaveMatchGroupRQ','2009-05-22 04:54:23'),(67,'GetMatchGroupRQ','2009-05-22 04:54:23'),(68,'VerifyProviderLeadStatusRQ','2009-05-22 04:54:23'),(69,'GetBillingGroupsRQ','2009-05-22 04:54:23'),(70,'GetBillingGroupChargesRQ','2009-05-22 04:54:23'),(71,'GetBillingGroupRQ','2009-05-22 04:54:23'),(72,'SaveBillingGroupRQ','2009-05-22 04:54:23'),(73,'SaveDefaultPriceRQ','2009-05-22 04:54:23'),(74,'GetProviderChangeHistoryRQ','2009-06-18 13:05:50'),(75,'GetDirectoryLocationsRQ','2009-07-08 13:37:53'),(76,'GetChannelGroupsRQ','2009-07-08 13:37:53'),(77,'UpdateExternalIdentifierRQ','2009-07-16 09:25:19'),(78,'SaveMessageEventRQ','2009-07-31 07:43:04'),(79,'GetProviderServiceAreasRQ','2009-08-28 09:04:32'),(80,'UpdateConsumerInputRQ','2009-10-06 10:18:03'),(81,'GetRegistrationChannelRQ','2009-10-06 10:18:03'),(82,'AddHoldDatesRQ','2009-10-19 12:03:55'),(83,'RemoveHoldDatesRQ','2009-10-19 12:03:55'),(84,'GetMessageEventsRQ','2010-02-18 02:39:32'),(85,'GetSystemMessageEventsRQ','2010-02-18 02:39:32'),(86,'SaveConfigRQ','2010-03-10 02:55:56'),(87,'GetConfigRQ','2010-03-10 02:55:56'),(88,'GetProviderReviewsRQ','2010-03-10 02:55:56'),(89,'UpdateProviderReviewRQ','2010-03-10 02:55:57'),(90,'SaveExternalReviewRQ','2010-03-10 02:55:57'),(91,'GetExternalReviewRQ','2010-03-10 02:55:57'),(92,'GetFeedbackRQ','2010-03-10 02:55:58'),(93,'GetExternalReviewsRQ','2010-03-10 02:55:58'),(94,'SaveClientConfigRQ','2010-03-10 02:55:58'),(95,'GetClientConfigRQ','2010-03-10 02:55:58'),(97,'GetNonceRQ','2010-04-01 11:05:53'),(98,'GetProfileGroupRQ','2010-04-01 11:05:53'),(99,'GetMessageTemplateGroupsRQ','2010-05-27 21:04:10'),(100,'SaveQueueRQ','2010-05-27 21:04:17'),(101,'GetQueueListRQ','2010-05-27 21:04:18'),(102,'ExportLocationsRQ','2010-05-27 21:04:19'),(103,'GetMessageTemplateRQ','2010-06-16 05:05:28'),(104,'GetMessageTemplatesRQ','2010-06-16 05:05:29'),(105,'SaveMessageTemplateRQ','2010-06-16 05:05:29'),(106,'SaveMessageTemplateGroupRQ','2010-06-16 05:05:29'),(107,'SaveAttributesRQ','2010-06-16 05:05:29');
/*!40000 ALTER TABLE `acl_resource` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `match_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_rule` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) default NULL,
  `type` enum('backfill','provider') NOT NULL default 'provider',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `match_rule` WRITE;
/*!40000 ALTER TABLE `match_rule` DISABLE KEYS */;
INSERT INTO `match_rule` VALUES (1,'Lep_Match_Cap','Filter out providers based on their daily, weekly and monthly lead caps','provider','0000-00-00 00:00:00'),(3,'Lep_Match_RemovalLocations','Filter out providers who do not service the specified pick up and drop off locations','provider','0000-00-00 00:00:00'),(5,'Lep_Match_MatchExact','Filter out providers who haven\'t got the same options that are indicated by the consumer for the tagged questions.','provider','0000-00-00 00:00:00'),(6,'Lep_Match_JobLocation','Filter out providers who don\'t service the specified job location. To be used for verticals which can have only one job location','provider','0000-00-00 00:00:00'),(8,'Lep_Match_MultiLocations','Filter out providers who do not service at least one of the specified location. Note that this rule is used in conjuction with the location-specific and location-multiple questions (e.g.Finance locations).','provider','2009-03-30 07:49:51'),(9,'Lep_Match_BackfillDefault','Default Backfill filter rule. Filters only on location.','backfill','2009-06-18 13:05:51'),(10,'Lep_Match_BackfillRemovals','Removals Backfill filter rule. Filters on PickUp Location Or Drop-off location.','backfill','2009-07-31 07:43:06'),(11,'Lep_Match_MatchPartial','Filter out providers who haven\'t got at least 1 of the options that are indicated by the consumer. Note this filter assumes that if a provider has not selected an option, then all options will apply.','provider','2009-09-18 13:56:00');
/*!40000 ALTER TABLE `match_rule` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `ranking_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ranking_rule` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `parent_match_rule_id` int(11) default NULL,
  `description` varchar(255) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `ranking_rule` WRITE;
/*!40000 ALTER TABLE `ranking_rule` DISABLE KEYS */;
INSERT INTO `ranking_rule` VALUES (1,'Lep_Rank_DaysLastMatch',NULL,'Days since last match attribute','0000-00-00 00:00:00'),(2,'Lep_Rank_EffectiveLeadPrice',NULL,'Effective Lead Price:Average price per lead (minus credits) for the match criteria of the project being ranked','0000-00-00 00:00:00'),(3,'Lep_Rank_ProviderPreference',NULL,'Ranked based on a provider\'s preference to accept certail jobs','2008-12-18 08:07:23'),(4,'Lep_Rank_ProviderSuitability',NULL,'Rank providers based on their suitability score. i.e. Average Rating for same job types','2008-12-18 09:36:29'),(5,'Lep_Rank_RatingQuality',NULL,'Rank providers based on their average Quality Rating score','2008-12-22 08:59:35'),(6,'Lep_Rank_CurrentLeadPrice',NULL,'Rank providers based on their current BASE lead price','2009-01-28 08:20:13'),(7,'Lep_Rank_ConsumerLocationPreference',8,'Rank providers based on whether to service the consumer\'s preferred location or supplementary locations','2009-03-30 08:49:44'),(8,'Lep_Rank_ProviderAverageRating',NULL,'Rank providers based on their average rating. Average rating is a product of both consumer feedback ratings and external review scores','2010-03-10 02:56:43');
/*!40000 ALTER TABLE `ranking_rule` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `registration_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_filter` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `registration_filter` WRITE;
/*!40000 ALTER TABLE `registration_filter` DISABLE KEYS */;
INSERT INTO `registration_filter` VALUES (1,'Lep_RegistrationFilter_Email','Checks for a valid email address.','2009-05-01 06:38:03'),(2,'Lep_RegistrationFilter_Name','Checks for valid first and last names.','2009-05-01 06:38:26'),(3,'Lep_RegistrationFilter_Phone','Checks for valid phone numbers.','2009-05-01 06:59:01'),(4,'Lep_RegistrationFilter_BlackList','Checks a registration event for black listed words.','2009-05-01 07:51:50'),(5,'Lep_RegistrationFilter_Duplicate','Checks for duplicate registrations.','2009-09-25 09:14:55');
/*!40000 ALTER TABLE `registration_filter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `system_message_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_message_event` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(100) NOT NULL,
  `name` varchar(100) default NULL,
  `description` varchar(255) default NULL,
  `applicable_delivery_methods` varchar(20) default NULL COMMENT 'Comma delimited list of delivery methods applicable for this system message event',
  `token_description` text COMMENT 'Describes tokens which are available to message templates derived from this system message event',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `system_message_event` WRITE;
/*!40000 ALTER TABLE `system_message_event` DISABLE KEYS */;
INSERT INTO `system_message_event` VALUES (1,'LeadAlert','Lead Alert',NULL,'email,sms,fax','<b>$vertical</b>  (Vertical name)\r\n<b>$provider_name</b>\r\n<b>$lead_time</b> (Date and time lead was generated)\r\n<b>$email</b> (Email address that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$fax</b> (Fax that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$mobile</b> (Mobile number that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$job_description</b> (Descriptive text for the project)\r\n<b>$product_description</b> (Main product option of project)\r\n<b>$consumer_inputs</b> (An array where each item consists of $question_label $option_label and $text_value)\r\n<b>$consumer_name</b> (Is available in both the message body and message subject)\r\n<b>$consumer_address1</b>\r\n<b>$consumer_address2</b>\r\n<b>$consumer_postcode</b>\r\n<b>$consumer_city</b>\r\n<b>$consumer_email</b>\r\n<b>$consumer_phone</b>\r\n<b>$consumer_mobile</b>\r\n<b>$best_contact_time</b>\r\n<b>$message_subject</b>','2009-01-22 12:41:47'),(2,'MatchResults','Match Results','Generates an email to the consumer in response registration matching','email','<b>$email</b>  (Consumer\'s email address)\r\n<b>$consumer_name</b>  (Consumer\'s name)\r\n<b>$consumer_key</b>  (Consumer\'s unique key)\r\n<b>$project_data</b>  (Array of projects. See below for description of indexes.)\r\n\r\nTokens available to a project\r\n<b>$vertical</b> (Vertical assigned to project)\r\n<b>$status</b> (Status of project)\r\n<b>$providers</b> (Array or providers assigned to project)\r\n<b>$backfill</b> (Array or backfill providers assigned to project)\r\n\r\nTokens available to a provider\r\n<b>$name</b> (Provider\'s display name)\r\n<b>$address</b> (Provider\'s display address)\r\n<b>$rating</b> (Provider\'s rating score)\r\n<b>$id</b> (Provider Id)\r\n<b>$contact_name</b>\r\n<b>$email</b>\r\n<b>$phone</b>\r\n<b>$fax</b>\r\n\r\nTokens available to a backfill provider\r\n<b>$name</b>\r\n<b>$address</b>\r\n<b>$phone</b>','2009-01-22 12:41:54'),(4,'24HourFollowUp','24 Hour Follow Up','Sends a \"Have you been contacted?\" email to the consumer 24 hours after their project was submitted.','email','<b>$email</b>  (Consumer\'s email address)\r\n<b>$consumer_key</b> (Consumer identifying key)\r\n<b>$consumer_name</b> (Consumer\'s name)','2009-07-08 13:37:52'),(5,'RatingRequest','Ratings Request','Sends a Ratings Request email to the consumer.','email','<b>$email</b>  (Consumer\'s email address)\r\n<b>$consumer_key</b> (Consumer identifying key)\r\n<b>$consumer_name</b> (Consumer\'s name)','2009-07-08 13:37:52'),(6,'RequestARating','Request A Rating','A provider initiated Ratings Request email to the consumer.','email','<b>$email</b>  (Consumer\'s email address)\r\n<b>$consumer_key</b> (Consumer identifying key)\r\n<b>$consumer_name</b> (Consumer\'s name)\r\n<b>$provider_name</b> (Name of provider who requested the rating)\r\n<b>$description</b> (Project description)','2009-07-08 13:37:52'),(8,'LeadCredit','Lead Credited','Email sent to a provider when a lead is credited.','email','<b>$description</b>  (Credit description)\r\n<b>$provider_contact</b> (Name of provider contact)\r\n<b>$submitted</b> (Date & time the credit was submitted)\r\n<b>$status</b> (Status of the credit)\r\n<b>$comment</b> (Lead credit comment)\r\n<b>$internal_description</b> (Credit\'s internal description)\r\n<b>$email</b> (Provider\'s email address)\r\n<b>$id</b> (Lead Id which is assigned the credit. Available in both the message body and message subject.)\r\n<b>$consumer_name</b> (Name of consumer who submitted the project. Available in both the message body and message subject.)','2009-07-08 13:37:52'),(9,'LeadAlertReminder','Lead Alert Reminder','A reminder sent to a provider reminding them of a lead.','email,sms,fax','<b>$vertical</b>  (Vertical name)\r\n<b>$provider_name</b>\r\n<b>$lead_time</b> (Date and time lead was generated)\r\n<b>$email</b> (Email address that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$fax</b> (Fax that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$mobile</b> (Mobile number that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$job_description</b> (Descriptive text for the project)\r\n<b>$product_description</b> (Main product option of project)\r\n<b>$consumer_inputs</b> (An array where each item consists of $question_label $option_label and $text_value)\r\n<b>$consumer_name</b> (Is available in both the message body and message subject)\r\n<b>$consumer_address1</b>\r\n<b>$consumer_address2</b>\r\n<b>$consumer_postcode</b>\r\n<b>$consumer_city</b>\r\n<b>$consumer_email</b>\r\n<b>$consumer_phone</b>\r\n<b>$consumer_mobile</b>\r\n<b>$best_contact_time</b>\r\n<b>$message_subject</b>','2009-07-08 13:37:52'),(10,'NewAccount','New Account','An email sent to the provider when an account is created outside of Drupal','email','<b>$contact_name</b>  (Provider contact)\r\n<b>$provider_name</b>\r\n<b>$provider_key</b> (Provider\'s unique key)\r\n<b>$provider_id</b> \r\n<b>$hash</b> (Unique hash for email)','2009-07-08 13:37:52');
/*!40000 ALTER TABLE `system_message_event` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` text,
  `is_system_tag` tinyint(1) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (1,'Pick Up Location','Identifies removals pick up location questions',1,'0000-00-00 00:00:00'),(2,'Drop Off Location','Identifies removals drop-off location questions',1,'0000-00-00 00:00:00'),(4,'Match Exact','Identifies questions used with Lep_Match_MatchExact match rules',1,'0000-00-00 00:00:00'),(5,'Email','Identifies consumer email questions',1,'0000-00-00 00:00:00'),(6,'First Name','Identifies consumer first name questions',1,'0000-00-00 00:00:00'),(7,'Last Name','Identifies consumer last name questions',1,'0000-00-00 00:00:00'),(8,'Address Line 1','Identifies consumer address line 1 questions',1,'0000-00-00 00:00:00'),(10,'Provider Profile','Identifies questions used when profiling a provider',1,'0000-00-00 00:00:00'),(12,'Provider Preference','Identifies questions used when ranking a provider based on preference',1,'2008-12-18 08:32:08'),(16,'Address Line 2','Identifies consumer address line 2 questions',1,'2009-01-02 06:50:52'),(17,'Address City','Identifies consumer city questions',1,'2009-01-02 06:50:59'),(18,'Address Postcode','Identifies consumer postcode questions',1,'2009-01-02 06:51:04'),(19,'Address Region','Identifies consumer region questions',1,'2009-01-02 06:51:10'),(21,'Job Location','Identifies job location and/or areas serviced',1,'2009-01-12 06:32:33'),(22,'Supplementary Charge','Identifies question which can have a supplementary charge component assigned to them',1,'2009-01-23 10:33:55'),(23,'Base Charge','Identifies question which can have a base charge component assigned to them',1,'2009-01-23 10:34:16'),(24,'Calendar','Indicates that a UI element contains a calendar component',1,'2009-03-23 05:18:25'),(25,'Other','Indicates that a UI element contains a text field component where the user can enter in free text classed as \"Other\"',1,'2009-03-23 08:58:50'),(26,'Registration End Point','Indicates which options when selected should terminate the registration process',1,'2009-03-24 05:54:31'),(27,'Consumer Primary Location','Identifies the consumers primary job location preference. This question does not apply to a provider and should not be used in Channels where only one job location is collected. Use the \"Job location\" tag instead.',0,'2009-03-30 08:09:09'),(28,'Consumer Supplementary Location','Identifies the consumers supplementary job location preferences. This question does not apply to a provider and should not be used in Channels where only one job location is collected. Use the \"Job location\" tag instead.',0,'2009-03-30 08:09:09'),(29,'Feedback','Identifies questions used for the feedback of a project',1,'2009-03-11 06:35:58'),(30,'Provider List','Identifies questions which require a dynamic provider list to be provided as options',1,'2009-03-19 03:42:01'),(31,'Feedback End Point','Identifies questions that are end nodes',1,'2009-03-19 10:48:09'),(32,'Feedback Return Point','Identifies the question that is a return point for the feedback questions',1,'2009-03-19 10:53:51'),(33,'Rated Provider','Identifies the question that signifies a rated provider',1,'2009-03-26 09:24:16'),(34,'Rating','Identifies the question that is a feedback rating',1,'2009-03-26 09:29:29'),(35,'Provider Chosen','Identifies the questions that illustrate a provider has been chosen from the feedback questions',1,'2009-03-26 10:05:35'),(38,'Quality','Identifies \"Quality\" rating questions',1,'2008-12-22 09:00:52'),(39,'Phone','Identifies a phone number submitted with a Consumer Request',1,'2009-04-02 09:03:49'),(40,'Feedback Comment','Identifies a feedback comments question',1,'2008-12-22 09:00:52'),(41,'Mobile','Identifies a mobile number submitted with a Consumer Request',1,'2009-06-18 13:05:51'),(42,'Preferred Contact Time','Identifies consumer\'s preferred contact time',1,'2009-07-08 13:37:52'),(43,'Feedback Lead Reminder','When chosen by a consumer, questions with this tag will generate a Lead Reminder Alert',1,'2009-07-08 13:37:52'),(44,'Match Partial','Identifies questions used with Lep_Match_MatchPartial match rules',1,'2009-09-18 13:49:26');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;
DELIMITER ;;
DELIMITER ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

#Default Data for a "clean" install
INSERT INTO `client` (id,name) VALUES (1,'Default Client');

INSERT INTO `acl_role` (id,name) VALUES (1,'Full Access');
INSERT INTO `acl_role_permission` (permission,acl_resource_id,acl_role_id)
SELECT 'allow',id,1 FROM `acl_resource`;

INSERT into `user` (id,username,password,real_name) VALUES (1,'administrator','aa5af1a498a1227904e3e49dbbd0f4e60fa366c9ed9c9a0fb3036898eed506cd','Administrator');
INSERT INTO `user_role` (user_id,role) VALUES (1,'administrator');
INSERT INTO `user_role` (user_id,role) VALUES (1,'channel-manager');

INSERT INTO `application_identity` (id,username,password,real_name,client_id) VALUES (1,'lep2-default','hUI3x92is92','Default User',1);
INSERT INTO `application_identity_acl_role` (application_identity_id,acl_role_id) VALUES (1,1);
