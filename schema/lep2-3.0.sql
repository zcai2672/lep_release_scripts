-- MySQL dump 10.11
--
-- Host: localhost    Database: lep-schema
-- ------------------------------------------------------
-- Server version	5.0.84-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acl_resource`
--

DROP TABLE IF EXISTS `acl_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_resource` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_resource`
--

LOCK TABLES `acl_resource` WRITE;
/*!40000 ALTER TABLE `acl_resource` DISABLE KEYS */;
INSERT INTO `acl_resource` VALUES (1,'GetQuestionsRQ','0000-00-00 00:00:00'),(2,'RegisterConsumerRequestRQ','0000-00-00 00:00:00'),(3,'GetConsumerRQ','0000-00-00 00:00:00'),(4,'GetConsumerRequestRQ','0000-00-00 00:00:00'),(5,'GetProviderRQ','2008-12-22 01:16:05'),(6,'SaveProviderRQ','2008-12-22 21:42:29'),(7,'GetProvidersRQ','2008-12-22 23:04:14'),(8,'UpdateProviderStatusRQ','2008-12-30 02:59:14'),(9,'GetSystemQuestionsRQ','2009-01-05 00:55:38'),(10,'GetChannelRQ','2009-01-05 22:18:32'),(11,'SaveChannelRQ','2009-01-06 04:39:52'),(12,'PublishChannelChangesRQ','2009-01-08 01:00:12'),(13,'UpdateChannelStatusRQ','2009-01-08 02:32:00'),(14,'GetChannelsRQ','2009-01-08 05:25:07'),(15,'GetProviderOnHoldDatesRQ','2009-01-13 01:15:09'),(16,'DuplicateQuestionRQ','2009-01-13 05:21:25'),(17,'DuplicateChannelRQ','2009-01-13 05:22:09'),(18,'PreviewConsumerRequestRQ','2009-01-14 04:04:57'),(19,'GetVerticalRQ','2009-01-15 06:14:01'),(20,'GetVerticalsRQ','2009-01-15 06:14:06'),(21,'SaveVerticalRQ','2009-01-15 22:18:04'),(22,'UpdateVerticalStatusRQ','2009-01-16 06:05:28'),(23,'GetSystemRankingRulesRQ','2009-01-19 00:36:24'),(24,'GetSystemMatchRulesRQ','2009-01-19 00:36:34'),(25,'GetApplicationIdentitiesRQ','2009-01-19 00:36:44'),(26,'GetSystemQuestionTagsRQ','2009-02-26 00:34:00'),(27,'GetLeadsRQ','2009-02-26 00:34:01'),(28,'GetSubscriptionsRQ','2009-02-26 00:34:01'),(29,'GetFeedbackListRQ','2009-02-26 00:34:01'),(30,'GetQuestionOptionRQ','2009-04-14 00:25:57'),(31,'GetVolumeForecastRQ','2009-04-14 00:25:57'),(32,'SaveCreditRequestRQ','2009-03-04 22:02:42'),(33,'GetLocationListRQ','2009-04-14 00:25:58'),(34,'SaveConsumerFeedbackRQ','2009-04-14 00:25:57'),(35,'GetConsumerFeedbackRQ','2009-04-14 00:25:57'),(36,'GetConsumerProjectsRQ','2009-04-14 00:25:57'),(37,'GetConsumerByKeyRQ','2009-04-01 23:29:23'),(38,'GenerateMessageEventRQ','2009-04-01 23:29:34'),(39,'GetSystemQuestionTypesRQ','2009-04-03 02:58:32'),(40,'GetSystemTagsRQ','2009-04-03 03:49:57'),(41,'SaveQuestionOptionRQ','2009-04-06 01:02:15'),(42,'SaveQuestionRQ','2009-04-06 01:02:15'),(43,'DeleteQuestionOptionRQ','2009-04-06 02:17:50'),(44,'AssignTagsRQ','2009-04-06 02:17:58'),(45,'UnAssignTagsRQ','2009-04-06 04:36:12'),(46,'UpdateQuestionStatusRQ','2009-04-07 00:43:39'),(47,'GetProfileGroupsRQ','2009-04-08 00:10:51'),(48,'GetProviderSystemMessagesRQ','2009-04-14 00:26:04'),(49,'SaveProfileRuleRQ','2009-04-08 06:32:37'),(50,'DeleteProfileRuleRQ','2009-04-08 07:02:17'),(51,'DeleteProfileRuleSourceRQ','2009-04-09 02:29:07'),(52,'SaveProfileRuleSourceRQ','2009-04-09 02:41:23'),(53,'SaveProfileGroupRQ','2009-04-09 04:41:13'),(54,'ValidatePostcodeRQ','2009-04-21 00:35:50'),(55,'GetFeedbackQuestionsRQ','2009-05-06 01:27:08'),(56,'GetProviderChargesRQ','2009-05-06 01:27:09'),(57,'GetProjectsRQ','2009-05-06 01:27:09'),(58,'GetProjectRQ','2009-05-06 01:27:09'),(59,'AddProviderChargeRQ','2009-05-06 01:27:09'),(60,'GetConsumerRequestsRQ','2009-05-06 01:27:09'),(61,'UpdateConsumerRequestStatusRQ','2009-05-06 01:27:09'),(62,'GetCreditRequestsRQ','2009-05-06 01:27:09'),(63,'GetRegistrationFiltersRQ','2009-05-06 01:27:09'),(64,'AssignChannelFiltersRQ','2009-05-06 01:27:09'),(65,'GetMatchGroupsRQ','2009-05-21 22:54:47'),(66,'SaveMatchGroupRQ','2009-05-21 22:54:47'),(67,'GetMatchGroupRQ','2009-05-21 22:54:47'),(68,'VerifyProviderLeadStatusRQ','2009-05-21 22:54:47'),(69,'GetBillingGroupsRQ','2009-05-21 22:54:47'),(70,'GetBillingGroupChargesRQ','2009-05-21 22:54:47'),(71,'GetBillingGroupRQ','2009-05-21 22:54:47'),(72,'SaveBillingGroupRQ','2009-05-21 22:54:47'),(73,'SaveDefaultPriceRQ','2009-05-21 22:54:47'),(74,'GetProviderChangeHistoryRQ','2009-06-18 07:06:14'),(75,'GetDirectoryLocationsRQ','2009-07-08 07:38:17'),(76,'GetChannelGroupsRQ','2009-07-08 07:38:17'),(77,'UpdateExternalIdentifierRQ','2009-07-16 03:25:43'),(78,'SaveMessageEventRQ','2009-07-31 01:43:28'),(79,'GetProviderServiceAreasRQ','2009-08-28 03:04:56'),(80,'UpdateConsumerInputRQ','2009-10-06 04:18:27'),(81,'GetRegistrationChannelRQ','2009-10-06 04:18:27'),(82,'SaveHoldDatesRQ','2009-10-19 06:04:19'),(83,'DeleteHoldDatesRQ','2009-10-19 06:04:19'),(84,'GetMessageEventsRQ','2010-02-17 20:39:56'),(85,'GetSystemMessageEventsRQ','2010-02-17 20:39:56'),(86,'SaveConfigRQ','2010-03-09 20:56:20'),(87,'GetConfigRQ','2010-03-09 20:56:20'),(88,'GetProviderReviewsRQ','2010-03-09 20:56:20'),(89,'UpdateProviderReviewRQ','2010-03-09 20:56:21'),(90,'SaveExternalReviewRQ','2010-03-09 20:56:21'),(91,'GetExternalReviewRQ','2010-03-09 20:56:21'),(92,'GetFeedbackRQ','2010-03-09 20:56:22'),(93,'GetExternalReviewsRQ','2010-03-09 20:56:22'),(94,'SaveClientConfigRQ','2010-03-09 20:56:22'),(95,'GetClientConfigRQ','2010-03-09 20:56:22'),(97,'GetNonceRQ','2010-03-30 20:29:13'),(98,'GetProfileGroupRQ','2010-03-30 20:29:13'),(99,'GetMessageTemplateGroupsRQ','2010-05-27 22:00:38'),(100,'SaveQueueRQ','2010-05-27 22:00:38'),(101,'GetQueueListRQ','2010-05-27 22:00:38'),(102,'ExportLocationsRQ','2010-05-27 22:00:38'),(103,'GetMessageTemplateRQ','2010-06-22 21:31:59'),(104,'GetMessageTemplatesRQ','2010-06-22 21:31:59'),(105,'SaveMessageTemplateRQ','2010-06-22 21:31:59'),(106,'SaveMessageTemplateGroupRQ','2010-06-22 21:31:59'),(107,'SaveAttributesRQ','2010-06-22 21:31:59'),(108,'CommsOptOutRQ','2010-09-22 00:43:18'),(109,'GetRemarketingOptionsRQ','2010-09-22 00:43:18'),(110,'DeleteRemarketingOptionRQ','2010-09-22 00:43:18'),(111,'SaveRemarketingOptionRQ','2010-09-22 00:43:18'),(112,'DeleteCapRQ','2010-09-23 03:31:37'),(113,'SaveCapRQ','2010-09-23 03:31:37'),(114,'GetCapsRQ','2010-09-23 03:31:37'),(115,'GetHoldDatesRQ','2010-09-23 05:32:28'),(116,'SaveServiceAreaRQ','2010-10-01 01:48:32'),(117,'GetServiceAreasRQ','2010-10-01 01:48:32'),(118,'GetCampaignProfilesRQ','2010-09-28 00:36:08'),(119,'SaveCampaignProfileRQ','2010-09-28 00:36:08'),(120,'DeleteCampaignProfileRQ','2010-09-28 01:59:03'),(121,'GetCampaignRQ','2010-10-01 07:51:49'),(122,'SaveCampaignRQ','2010-10-01 07:51:49'),(123,'GetCampaignsRQ','2010-10-01 07:51:49'),(124,'SaveCampaignContactMediaRQ','2010-10-18 00:41:34'),(125,'SaveMessageDeliveryOptionsRQ','2010-10-18 00:41:34'),(126,'SaveContactRQ','2010-10-18 04:48:59'),(127,'GetContactRQ','2010-10-18 04:48:59'),(128,'GetContactsRQ','2010-10-18 04:48:59'),(129,'SaveCampaignProfilesRQ','2010-11-03 04:12:43'),(130,'DuplicateBillingGroupRQ','2010-11-05 08:39:06'),(131,'VerifyCampaignLeadStatusRQ','2010-11-09 01:57:45'),(132,'DuplicateCampaignRQ','2010-11-26 07:59:01'),(133,'SaveProviderAttributeRQ','2010-12-02 23:02:20'),(134,'SaveChannelWidgetRQ','2010-12-07 07:08:52'),(135,'GetChannelWidgetRQ','2010-12-07 07:08:52'),(136,'SaveLeadPlanRQ','2011-01-07 04:42:55'),(137,'GetPackagesRQ','2011-01-07 06:40:15'),(138,'GetLeadPlansRQ','2011-01-09 22:39:07'),(139,'CancelLeadPlanRQ','2011-01-12 05:12:34'),(140,'ActivateLeadPlanRQ','2011-01-13 22:57:25');
/*!40000 ALTER TABLE `acl_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_role`
--

DROP TABLE IF EXISTS `acl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_role` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_role`
--

LOCK TABLES `acl_role` WRITE;
/*!40000 ALTER TABLE `acl_role` DISABLE KEYS */;
INSERT INTO `acl_role` VALUES (1,'Full Access','2011-01-31 06:04:02');
/*!40000 ALTER TABLE `acl_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_role_permission`
--

DROP TABLE IF EXISTS `acl_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_role_permission` (
  `id` int(11) NOT NULL auto_increment,
  `permission` enum('allow','deny') NOT NULL,
  `privilege` enum('view','delete','insert','update','select') default NULL,
  `acl_resource_id` int(11) default NULL,
  `acl_role_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_acl_role_permission_acl_resource` (`acl_resource_id`),
  KEY `fk_acl_role_permission_acl_role` (`acl_role_id`),
  CONSTRAINT `fk_acl_role_permission_acl_resource` FOREIGN KEY (`acl_resource_id`) REFERENCES `acl_resource` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_acl_role_permission_acl_role` FOREIGN KEY (`acl_role_id`) REFERENCES `acl_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=377 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_role_permission`
--

LOCK TABLES `acl_role_permission` WRITE;
/*!40000 ALTER TABLE `acl_role_permission` DISABLE KEYS */;
INSERT INTO `acl_role_permission` VALUES (238,'allow',NULL,1,1,'2011-01-31 06:04:02'),(239,'allow',NULL,2,1,'2011-01-31 06:04:02'),(240,'allow',NULL,3,1,'2011-01-31 06:04:02'),(241,'allow',NULL,4,1,'2011-01-31 06:04:02'),(242,'allow',NULL,5,1,'2011-01-31 06:04:02'),(243,'allow',NULL,6,1,'2011-01-31 06:04:02'),(244,'allow',NULL,7,1,'2011-01-31 06:04:02'),(245,'allow',NULL,8,1,'2011-01-31 06:04:02'),(246,'allow',NULL,9,1,'2011-01-31 06:04:02'),(247,'allow',NULL,10,1,'2011-01-31 06:04:02'),(248,'allow',NULL,11,1,'2011-01-31 06:04:02'),(249,'allow',NULL,12,1,'2011-01-31 06:04:02'),(250,'allow',NULL,13,1,'2011-01-31 06:04:02'),(251,'allow',NULL,14,1,'2011-01-31 06:04:02'),(252,'allow',NULL,15,1,'2011-01-31 06:04:02'),(253,'allow',NULL,16,1,'2011-01-31 06:04:02'),(254,'allow',NULL,17,1,'2011-01-31 06:04:02'),(255,'allow',NULL,18,1,'2011-01-31 06:04:02'),(256,'allow',NULL,19,1,'2011-01-31 06:04:02'),(257,'allow',NULL,20,1,'2011-01-31 06:04:02'),(258,'allow',NULL,21,1,'2011-01-31 06:04:02'),(259,'allow',NULL,22,1,'2011-01-31 06:04:02'),(260,'allow',NULL,23,1,'2011-01-31 06:04:02'),(261,'allow',NULL,24,1,'2011-01-31 06:04:02'),(262,'allow',NULL,25,1,'2011-01-31 06:04:02'),(263,'allow',NULL,26,1,'2011-01-31 06:04:02'),(264,'allow',NULL,27,1,'2011-01-31 06:04:02'),(265,'allow',NULL,28,1,'2011-01-31 06:04:02'),(266,'allow',NULL,29,1,'2011-01-31 06:04:02'),(267,'allow',NULL,30,1,'2011-01-31 06:04:02'),(268,'allow',NULL,31,1,'2011-01-31 06:04:02'),(269,'allow',NULL,32,1,'2011-01-31 06:04:02'),(270,'allow',NULL,33,1,'2011-01-31 06:04:02'),(271,'allow',NULL,34,1,'2011-01-31 06:04:02'),(272,'allow',NULL,35,1,'2011-01-31 06:04:02'),(273,'allow',NULL,36,1,'2011-01-31 06:04:02'),(274,'allow',NULL,37,1,'2011-01-31 06:04:02'),(275,'allow',NULL,38,1,'2011-01-31 06:04:02'),(276,'allow',NULL,39,1,'2011-01-31 06:04:02'),(277,'allow',NULL,40,1,'2011-01-31 06:04:02'),(278,'allow',NULL,41,1,'2011-01-31 06:04:02'),(279,'allow',NULL,42,1,'2011-01-31 06:04:02'),(280,'allow',NULL,43,1,'2011-01-31 06:04:02'),(281,'allow',NULL,44,1,'2011-01-31 06:04:02'),(282,'allow',NULL,45,1,'2011-01-31 06:04:02'),(283,'allow',NULL,46,1,'2011-01-31 06:04:02'),(284,'allow',NULL,47,1,'2011-01-31 06:04:02'),(285,'allow',NULL,48,1,'2011-01-31 06:04:02'),(286,'allow',NULL,49,1,'2011-01-31 06:04:02'),(287,'allow',NULL,50,1,'2011-01-31 06:04:02'),(288,'allow',NULL,51,1,'2011-01-31 06:04:02'),(289,'allow',NULL,52,1,'2011-01-31 06:04:02'),(290,'allow',NULL,53,1,'2011-01-31 06:04:02'),(291,'allow',NULL,54,1,'2011-01-31 06:04:02'),(292,'allow',NULL,55,1,'2011-01-31 06:04:02'),(293,'allow',NULL,56,1,'2011-01-31 06:04:02'),(294,'allow',NULL,57,1,'2011-01-31 06:04:02'),(295,'allow',NULL,58,1,'2011-01-31 06:04:02'),(296,'allow',NULL,59,1,'2011-01-31 06:04:02'),(297,'allow',NULL,60,1,'2011-01-31 06:04:02'),(298,'allow',NULL,61,1,'2011-01-31 06:04:02'),(299,'allow',NULL,62,1,'2011-01-31 06:04:02'),(300,'allow',NULL,63,1,'2011-01-31 06:04:02'),(301,'allow',NULL,64,1,'2011-01-31 06:04:02'),(302,'allow',NULL,65,1,'2011-01-31 06:04:02'),(303,'allow',NULL,66,1,'2011-01-31 06:04:02'),(304,'allow',NULL,67,1,'2011-01-31 06:04:02'),(305,'allow',NULL,68,1,'2011-01-31 06:04:02'),(306,'allow',NULL,69,1,'2011-01-31 06:04:02'),(307,'allow',NULL,70,1,'2011-01-31 06:04:02'),(308,'allow',NULL,71,1,'2011-01-31 06:04:02'),(309,'allow',NULL,72,1,'2011-01-31 06:04:02'),(310,'allow',NULL,73,1,'2011-01-31 06:04:02'),(311,'allow',NULL,74,1,'2011-01-31 06:04:02'),(312,'allow',NULL,75,1,'2011-01-31 06:04:02'),(313,'allow',NULL,76,1,'2011-01-31 06:04:02'),(314,'allow',NULL,77,1,'2011-01-31 06:04:02'),(315,'allow',NULL,78,1,'2011-01-31 06:04:02'),(316,'allow',NULL,79,1,'2011-01-31 06:04:02'),(317,'allow',NULL,80,1,'2011-01-31 06:04:02'),(318,'allow',NULL,81,1,'2011-01-31 06:04:02'),(319,'allow',NULL,82,1,'2011-01-31 06:04:02'),(320,'allow',NULL,83,1,'2011-01-31 06:04:02'),(321,'allow',NULL,84,1,'2011-01-31 06:04:02'),(322,'allow',NULL,85,1,'2011-01-31 06:04:02'),(323,'allow',NULL,86,1,'2011-01-31 06:04:02'),(324,'allow',NULL,87,1,'2011-01-31 06:04:02'),(325,'allow',NULL,88,1,'2011-01-31 06:04:02'),(326,'allow',NULL,89,1,'2011-01-31 06:04:02'),(327,'allow',NULL,90,1,'2011-01-31 06:04:02'),(328,'allow',NULL,91,1,'2011-01-31 06:04:02'),(329,'allow',NULL,92,1,'2011-01-31 06:04:02'),(330,'allow',NULL,93,1,'2011-01-31 06:04:02'),(331,'allow',NULL,94,1,'2011-01-31 06:04:02'),(332,'allow',NULL,95,1,'2011-01-31 06:04:02'),(333,'allow',NULL,97,1,'2011-01-31 06:04:02'),(334,'allow',NULL,98,1,'2011-01-31 06:04:02'),(335,'allow',NULL,99,1,'2011-01-31 06:04:02'),(336,'allow',NULL,100,1,'2011-01-31 06:04:02'),(337,'allow',NULL,101,1,'2011-01-31 06:04:02'),(338,'allow',NULL,102,1,'2011-01-31 06:04:02'),(339,'allow',NULL,103,1,'2011-01-31 06:04:02'),(340,'allow',NULL,104,1,'2011-01-31 06:04:02'),(341,'allow',NULL,105,1,'2011-01-31 06:04:02'),(342,'allow',NULL,106,1,'2011-01-31 06:04:02'),(343,'allow',NULL,107,1,'2011-01-31 06:04:02'),(344,'allow',NULL,108,1,'2011-01-31 06:04:02'),(345,'allow',NULL,109,1,'2011-01-31 06:04:02'),(346,'allow',NULL,110,1,'2011-01-31 06:04:02'),(347,'allow',NULL,111,1,'2011-01-31 06:04:02'),(348,'allow',NULL,112,1,'2011-01-31 06:04:02'),(349,'allow',NULL,113,1,'2011-01-31 06:04:02'),(350,'allow',NULL,114,1,'2011-01-31 06:04:02'),(351,'allow',NULL,115,1,'2011-01-31 06:04:02'),(352,'allow',NULL,116,1,'2011-01-31 06:04:02'),(353,'allow',NULL,117,1,'2011-01-31 06:04:02'),(354,'allow',NULL,118,1,'2011-01-31 06:04:02'),(355,'allow',NULL,119,1,'2011-01-31 06:04:02'),(356,'allow',NULL,120,1,'2011-01-31 06:04:02'),(357,'allow',NULL,121,1,'2011-01-31 06:04:02'),(358,'allow',NULL,122,1,'2011-01-31 06:04:02'),(359,'allow',NULL,123,1,'2011-01-31 06:04:02'),(360,'allow',NULL,124,1,'2011-01-31 06:04:02'),(361,'allow',NULL,125,1,'2011-01-31 06:04:02'),(362,'allow',NULL,126,1,'2011-01-31 06:04:02'),(363,'allow',NULL,127,1,'2011-01-31 06:04:02'),(364,'allow',NULL,128,1,'2011-01-31 06:04:02'),(365,'allow',NULL,129,1,'2011-01-31 06:04:02'),(366,'allow',NULL,130,1,'2011-01-31 06:04:02'),(367,'allow',NULL,131,1,'2011-01-31 06:04:02'),(368,'allow',NULL,132,1,'2011-01-31 06:04:02'),(369,'allow',NULL,133,1,'2011-01-31 06:04:02'),(370,'allow',NULL,134,1,'2011-01-31 06:04:02'),(371,'allow',NULL,135,1,'2011-01-31 06:04:02'),(372,'allow',NULL,136,1,'2011-01-31 06:04:02'),(373,'allow',NULL,137,1,'2011-01-31 06:04:02'),(374,'allow',NULL,138,1,'2011-01-31 06:04:02'),(375,'allow',NULL,139,1,'2011-01-31 06:04:02'),(376,'allow',NULL,140,1,'2011-01-31 06:04:02');
/*!40000 ALTER TABLE `acl_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL auto_increment,
  `type` enum('billing','street') NOT NULL,
  `address1` varchar(255) default NULL,
  `address2` varchar(255) default NULL,
  `city` varchar(255) default NULL,
  `region` varchar(255) default NULL,
  `state` varchar(100) default NULL,
  `postcode` varchar(20) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=345933 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user_provider`
--

DROP TABLE IF EXISTS `admin_user_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user_provider` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user_provider`
--

LOCK TABLES `admin_user_provider` WRITE;
/*!40000 ALTER TABLE `admin_user_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_user_provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliate`
--

DROP TABLE IF EXISTS `affiliate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliate` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_affiliate_client_id` (`client_id`),
  CONSTRAINT `fk_affiliate_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliate`
--

LOCK TABLES `affiliate` WRITE;
/*!40000 ALTER TABLE `affiliate` DISABLE KEYS */;
/*!40000 ALTER TABLE `affiliate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_transaction`
--

DROP TABLE IF EXISTS `api_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_transaction` (
  `id` int(11) NOT NULL auto_increment,
  `nonce` varchar(250) NOT NULL,
  `application_identity_id` int(11) NOT NULL,
  `ip` varchar(15) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `expiry_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  KEY `nonce_key` (`nonce`),
  KEY `fk_application_identity` (`application_identity_id`),
  CONSTRAINT `fk_application_identity` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_transaction`
--

LOCK TABLES `api_transaction` WRITE;
/*!40000 ALTER TABLE `api_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `application_identity`
--

DROP TABLE IF EXISTS `application_identity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_identity` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `real_name` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_client_id` USING BTREE (`client_id`),
  KEY `idx_username` USING BTREE (`username`),
  KEY `idx_password` USING BTREE (`password`),
  CONSTRAINT `application_identity_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='InnoDB free: 3818496 kB; (`client_id`) REFER `lep2/client`(`';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application_identity`
--

LOCK TABLES `application_identity` WRITE;
/*!40000 ALTER TABLE `application_identity` DISABLE KEYS */;
INSERT INTO `application_identity` VALUES (1,'quotifydrupal','70f9c3704877367ef85e37623636d2d6','Quotify Drupal Account',1,'2009-04-14 01:48:42'),(2,'quotifyadmin','725d12af027996640e84ce3a472e80df','Quotify Admin Account',1,'2009-07-16 03:18:41'),(4,'overmonitor','dd00a2eb3988886ce2c503d4099433fc','Over Monitor',1,'2009-07-16 03:19:12');
/*!40000 ALTER TABLE `application_identity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `application_identity_acl_role`
--

DROP TABLE IF EXISTS `application_identity_acl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_identity_acl_role` (
  `application_identity_id` int(11) default NULL,
  `acl_role_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` USING BTREE (`application_identity_id`,`acl_role_id`),
  KEY `fk_application_identity_acl_role_application_identity` USING BTREE (`application_identity_id`),
  KEY `fk_application_identity_acl_role_acl_role` USING BTREE (`acl_role_id`),
  CONSTRAINT `application_identity_acl_role_ibfk_1` FOREIGN KEY (`acl_role_id`) REFERENCES `acl_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `application_identity_acl_role_ibfk_2` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='InnoDB free: 3818496 kB; (`acl_role_id`) REFER `lep2/acl_rol';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application_identity_acl_role`
--

LOCK TABLES `application_identity_acl_role` WRITE;
/*!40000 ALTER TABLE `application_identity_acl_role` DISABLE KEYS */;
INSERT INTO `application_identity_acl_role` VALUES (1,1,'2009-04-14 01:48:42'),(2,1,'2009-07-16 03:19:38'),(4,1,'2009-07-16 03:19:41');
/*!40000 ALTER TABLE `application_identity_acl_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `application_identity_channel`
--

DROP TABLE IF EXISTS `application_identity_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_identity_channel` (
  `application_identity_id` int(11) default NULL,
  `channel_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`application_identity_id`,`channel_id`),
  KEY `fk_application_identity_channel_application_identity` (`application_identity_id`),
  KEY `fk_application_identity_channel_channel` (`channel_id`),
  CONSTRAINT `fk_application_identity_channel_application_identity` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_application_identity_channel_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application_identity_channel`
--

LOCK TABLES `application_identity_channel` WRITE;
/*!40000 ALTER TABLE `application_identity_channel` DISABLE KEYS */;
/*!40000 ALTER TABLE `application_identity_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applied_match_rule`
--

DROP TABLE IF EXISTS `applied_match_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applied_match_rule` (
  `id` int(11) NOT NULL auto_increment,
  `match_rule_id` int(11) NOT NULL,
  `job_spec_id` int(11) NOT NULL,
  `question_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_match_rule` (`match_rule_id`),
  KEY `fk_job_spec` (`job_spec_id`),
  CONSTRAINT `fk_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_rule` FOREIGN KEY (`match_rule_id`) REFERENCES `match_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2482958 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applied_match_rule`
--

LOCK TABLES `applied_match_rule` WRITE;
/*!40000 ALTER TABLE `applied_match_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `applied_match_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_log`
--

DROP TABLE IF EXISTS `audit_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_log` (
  `id` int(11) NOT NULL auto_increment,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `field` varchar(100) NOT NULL,
  `old` varchar(255) default NULL,
  `new` varchar(255) default NULL,
  `app_identity` int(11) default NULL,
  `audit_message` text,
  `is_model_change` tinyint(4) default '1' COMMENT '1 = change to an actual model. 0 = a high level change.',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_model` (`model`),
  KEY `idx_model_id` (`model_id`),
  KEY `idx_application_identity` (`app_identity`)
) ENGINE=InnoDB AUTO_INCREMENT=3857174 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_log`
--

LOCK TABLES `audit_log` WRITE;
/*!40000 ALTER TABLE `audit_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `backfill`
--

DROP TABLE IF EXISTS `backfill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backfill` (
  `id` int(11) NOT NULL auto_increment,
  `job_spec_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `campaign_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `job_spec_id` (`job_spec_id`),
  KEY `provider_id` (`provider_id`),
  KEY `fk_backfill_campaign_id` (`campaign_id`),
  CONSTRAINT `backfill_ibfk_1` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`),
  CONSTRAINT `backfill_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_backfill_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147078 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backfill`
--

LOCK TABLES `backfill` WRITE;
/*!40000 ALTER TABLE `backfill` DISABLE KEYS */;
/*!40000 ALTER TABLE `backfill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `billing_group`
--

DROP TABLE IF EXISTS `billing_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_group` (
  `id` int(11) NOT NULL auto_increment,
  `contact_id` int(11) default NULL,
  `address_id` int(11) default NULL,
  `credit_card_id` int(11) default NULL,
  `description` text,
  `type` enum('credit card','invoice') NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL default NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `fk_contact_id` (`contact_id`),
  KEY `fk_address_id` (`address_id`),
  KEY `fk_credit_card_id` (`credit_card_id`),
  KEY `fk_billing_group_client_id` (`client_id`),
  CONSTRAINT `fk_address_id` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `fk_billing_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`),
  CONSTRAINT `fk_credit_card_id` FOREIGN KEY (`credit_card_id`) REFERENCES `credit_card` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93563 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billing_group`
--

LOCK TABLES `billing_group` WRITE;
/*!40000 ALTER TABLE `billing_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `billing_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `billing_group_contact_media`
--

DROP TABLE IF EXISTS `billing_group_contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_group_contact_media` (
  `billing_group_id` int(11) NOT NULL,
  `contact_media_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  KEY `fk_billing_group_id` (`billing_group_id`),
  KEY `fk_contact_media_id` (`contact_media_id`),
  CONSTRAINT `fk_biling_group_id` FOREIGN KEY (`billing_group_id`) REFERENCES `billing_group` (`id`),
  CONSTRAINT `fk_contact_media_id` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billing_group_contact_media`
--

LOCK TABLES `billing_group_contact_media` WRITE;
/*!40000 ALTER TABLE `billing_group_contact_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `billing_group_contact_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `vertical_id` int(11) NOT NULL,
  `status` enum('inactive','pending','deleted','active') NOT NULL default 'pending',
  `is_held` tinyint(4) NOT NULL default '0',
  `provider_id` int(11) NOT NULL,
  `contact_id` int(11) default NULL,
  `lead_plan_id` int(11) default NULL COMMENT 'Indicates campaign''s current lead plan. No lead plan means a la carte.',
  `created_date` timestamp NULL default CURRENT_TIMESTAMP,
  `campaign_key` varchar(32) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_campaign_provider_id` (`provider_id`),
  KEY `fk_campaign_vertical_id` (`vertical_id`),
  KEY `fk_campaign_contact_id` (`contact_id`),
  KEY `fk_campaign_lead_plan_id` (`lead_plan_id`),
  CONSTRAINT `fk_campaign_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`),
  CONSTRAINT `fk_campaign_lead_plan_id` FOREIGN KEY (`lead_plan_id`) REFERENCES `lead_plan` (`id`),
  CONSTRAINT `fk_campaign_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_campaign_vertical_id` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85772 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign`
--

LOCK TABLES `campaign` WRITE;
/*!40000 ALTER TABLE `campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_contact`
--

DROP TABLE IF EXISTS `campaign_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_contact` (
  `id` int(11) NOT NULL auto_increment,
  `campaign_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `campaign_contact_campaign_id_fk` (`campaign_id`),
  KEY `campaign_contact_contact_id_fk` (`contact_id`),
  CONSTRAINT `campaign_contact_campaign_id_fk` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `campaign_contact_contact_id_fk` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_contact`
--

LOCK TABLES `campaign_contact` WRITE;
/*!40000 ALTER TABLE `campaign_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_contact_media`
--

DROP TABLE IF EXISTS `campaign_contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_contact_media` (
  `id` int(11) NOT NULL auto_increment,
  `contact_media_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `contact_media_contact_media_id_fk` (`contact_media_id`),
  KEY `campaign_campaign_id_fk` (`campaign_id`),
  CONSTRAINT `campaign_campaign_id_fk` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `contact_media_contact_media_id_fk` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_contact_media`
--

LOCK TABLES `campaign_contact_media` WRITE;
/*!40000 ALTER TABLE `campaign_contact_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_contact_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_profile`
--

DROP TABLE IF EXISTS `campaign_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_profile` (
  `id` int(11) NOT NULL auto_increment,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `price` decimal(10,2) default NULL,
  `price_type` enum('%','$') default NULL,
  `preference` decimal(10,2) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `campaign_profile_question_id` (`question_id`),
  KEY `campaign_profile_option_id` (`option_id`),
  KEY `campaign_profile_campaign_id` (`campaign_id`),
  CONSTRAINT `campaign_profile_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  CONSTRAINT `campaign_profile_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`),
  CONSTRAINT `campaign_profile_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22586 DEFAULT CHARSET=latin1 COMMENT='Stores campaign lead prices & profile information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_profile`
--

LOCK TABLES `campaign_profile` WRITE;
/*!40000 ALTER TABLE `campaign_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cap`
--

DROP TABLE IF EXISTS `cap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cap` (
  `id` int(11) NOT NULL auto_increment,
  `daily_lead_cap` int(11) NOT NULL default '0',
  `provider_id` int(11) NOT NULL,
  `weekly_lead_cap` int(11) NOT NULL default '0',
  `monthly_lead_cap` int(11) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `total_lead_cap` int(11) NOT NULL default '0',
  `total_spend_cap` decimal(7,2) default NULL,
  `free_lead_cap` int(11) NOT NULL default '0',
  `campaign_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uk_cap1` (`provider_id`,`campaign_id`),
  KEY `fk_provider_cap_provider` (`provider_id`),
  KEY `fk_cap_campaign_id` (`campaign_id`),
  CONSTRAINT `cap_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cap_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cap`
--

LOCK TABLES `cap` WRITE;
/*!40000 ALTER TABLE `cap` DISABLE KEYS */;
/*!40000 ALTER TABLE `cap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `code` varchar(50) default NULL,
  `channel_group_id` int(11) default NULL,
  `message_template_group_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `status` enum('active','inactive','template','staging') NOT NULL default 'staging',
  `parent_id` int(11) default NULL,
  `client_id` int(11) NOT NULL,
  `template_channel_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`),
  KEY `fk_channel_group_id` (`channel_group_id`),
  KEY `fk_channel_message_template_group_id` (`message_template_group_id`),
  KEY `fk_channel_client_id` (`client_id`),
  KEY `idx_created_date` (`created_date`),
  KEY `idx_template_channel_id` (`template_channel_id`),
  CONSTRAINT `fk_channel_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_channel_group_id` FOREIGN KEY (`channel_group_id`) REFERENCES `channel_group` (`id`),
  CONSTRAINT `fk_channel_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1388 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel`
--

LOCK TABLES `channel` WRITE;
/*!40000 ALTER TABLE `channel` DISABLE KEYS */;
/*!40000 ALTER TABLE `channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_filter`
--

DROP TABLE IF EXISTS `channel_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_filter` (
  `id` int(11) NOT NULL auto_increment,
  `filter_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `params` text NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_channel_id_filter_id` (`filter_id`,`channel_id`),
  KEY `fk_filter_id` (`filter_id`),
  KEY `fk_channel_id` (`channel_id`),
  CONSTRAINT `fk_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_filter_id` FOREIGN KEY (`filter_id`) REFERENCES `registration_filter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8211 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_filter`
--

LOCK TABLES `channel_filter` WRITE;
/*!40000 ALTER TABLE `channel_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `channel_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_group`
--

DROP TABLE IF EXISTS `channel_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_group` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `message_template_group_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_code` (`code`),
  KEY `fk_channel_group_message_template_group_id` (`message_template_group_id`),
  KEY `fk_channel_group_client_id` (`client_id`),
  CONSTRAINT `fk_channel_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_channel_group_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_group`
--

LOCK TABLES `channel_group` WRITE;
/*!40000 ALTER TABLE `channel_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `channel_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_option`
--

DROP TABLE IF EXISTS `channel_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_option` (
  `id` int(11) NOT NULL auto_increment,
  `channel_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_label` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_channel_option_channel_id` (`channel_id`),
  KEY `fk_channel_option_option_id` (`option_id`),
  CONSTRAINT `fk_channel_option_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`),
  CONSTRAINT `fk_channel_option_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20091 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_option`
--

LOCK TABLES `channel_option` WRITE;
/*!40000 ALTER TABLE `channel_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `channel_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_question`
--

DROP TABLE IF EXISTS `channel_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_question` (
  `id` int(11) NOT NULL auto_increment,
  `channel_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `label` varchar(255) default NULL,
  `description` text,
  `page` int(11) NOT NULL default '1',
  `is_hidden` tinyint(1) NOT NULL default '0',
  `default_value` varchar(50) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_channel_question_channel` (`channel_id`),
  KEY `fk_channel_question_question` (`question_id`),
  CONSTRAINT `fk_channel_question_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_channel_question_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=63536 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_question`
--

LOCK TABLES `channel_question` WRITE;
/*!40000 ALTER TABLE `channel_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `channel_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_vertical`
--

DROP TABLE IF EXISTS `channel_vertical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_vertical` (
  `vertical_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`vertical_id`,`channel_id`),
  KEY `fk_channel_vertical_vertical` (`vertical_id`),
  KEY `fk_channel_vertical_channel` (`channel_id`),
  CONSTRAINT `fk_channel_vertical_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_channel_vertical_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_vertical`
--

LOCK TABLES `channel_vertical` WRITE;
/*!40000 ALTER TABLE `channel_vertical` DISABLE KEYS */;
/*!40000 ALTER TABLE `channel_vertical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_widget`
--

DROP TABLE IF EXISTS `channel_widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_widget` (
  `id` int(11) NOT NULL auto_increment,
  `channel_id` int(11) NOT NULL,
  `layout` varchar(50) NOT NULL,
  `default_question_id` int(11) default NULL,
  `type` enum('lightbox','inline') NOT NULL default 'inline' COMMENT 'Widget invocation type',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `channel_widget_channel_id` (`channel_id`),
  KEY `channel_widget_default_question_id` (`default_question_id`),
  CONSTRAINT `channel_widget_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `channel_widget_default_question_id` FOREIGN KEY (`default_question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_widget`
--

LOCK TABLES `channel_widget` WRITE;
/*!40000 ALTER TABLE `channel_widget` DISABLE KEYS */;
/*!40000 ALTER TABLE `channel_widget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `message_template_group_id` int(11) default NULL,
  `country` varchar(100) NOT NULL COMMENT 'Country of client and its children',
  `currency_code` char(3) NOT NULL COMMENT 'Currency used by the client and its children',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_client_message_template_group_id` (`message_template_group_id`),
  CONSTRAINT `fk_client_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2359 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'Default Client',NULL,'','','2011-01-31 06:04:02');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(11) NOT NULL auto_increment,
  `config_key` varchar(45) NOT NULL,
  `config_value` varchar(255) NOT NULL,
  `model_name` varchar(45) NOT NULL,
  `model_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `pk_id` (`id`),
  KEY `idx_config_key` (`config_key`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1 COMMENT='This table stores configuration values for various models';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumer`
--

DROP TABLE IF EXISTS `consumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(250) default NULL,
  `comms_opt_out` tinyint(4) default '0' COMMENT '1 = Consumer should not be sent any emails',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `consumer_key` varchar(32) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_consumer_key` (`consumer_key`),
  KEY `idx_email` (`email`),
  KEY `fk_consumer_client_id` (`client_id`),
  CONSTRAINT `fk_consumer_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=185019 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumer`
--

LOCK TABLES `consumer` WRITE;
/*!40000 ALTER TABLE `consumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumer_feedback`
--

DROP TABLE IF EXISTS `consumer_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_feedback` (
  `id` int(11) NOT NULL auto_increment,
  `project_id` int(11) NOT NULL,
  `consumer_id` int(11) NOT NULL,
  `provider_id` int(11) default NULL,
  `provider_review_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_cf_project_id` (`project_id`),
  KEY `fk_cf_consumer_id` (`consumer_id`),
  KEY `fk_cf_provider_id` (`provider_id`),
  KEY `fk_cf_provider_review_id` (`provider_review_id`),
  CONSTRAINT `fk_cf_consumer_id` FOREIGN KEY (`consumer_id`) REFERENCES `consumer` (`id`),
  CONSTRAINT `fk_cf_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `fk_cf_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_cf_provider_review_id` FOREIGN KEY (`provider_review_id`) REFERENCES `provider_review` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17882 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumer_feedback`
--

LOCK TABLES `consumer_feedback` WRITE;
/*!40000 ALTER TABLE `consumer_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumer_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumer_feedback_old`
--

DROP TABLE IF EXISTS `consumer_feedback_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_feedback_old` (
  `id` int(11) NOT NULL default '0',
  `project_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `question_id` int(11) default NULL,
  `option_id` int(11) default NULL,
  `text_value` text,
  `created_date` timestamp NOT NULL default '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumer_feedback_old`
--

LOCK TABLES `consumer_feedback_old` WRITE;
/*!40000 ALTER TABLE `consumer_feedback_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumer_feedback_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumer_question_answer`
--

DROP TABLE IF EXISTS `consumer_question_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_question_answer` (
  `id` int(11) NOT NULL auto_increment,
  `consumer_id` int(11) NOT NULL,
  `project_id` int(11) default NULL,
  `question_id` int(11) default NULL,
  `option_code` varchar(10) default NULL,
  `text_value` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_consumer_id` (`consumer_id`),
  KEY `idx_project_id` (`project_id`),
  KEY `idx_question_id` (`question_id`),
  CONSTRAINT `fk_consumer_id` FOREIGN KEY (`consumer_id`) REFERENCES `consumer` (`id`),
  CONSTRAINT `fk_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `fk_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87305 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumer_question_answer`
--

LOCK TABLES `consumer_question_answer` WRITE;
/*!40000 ALTER TABLE `consumer_question_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumer_question_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumer_request`
--

DROP TABLE IF EXISTS `consumer_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_request` (
  `id` int(11) NOT NULL auto_increment,
  `request_key` varchar(32) NOT NULL,
  `status` enum('pending','inactive','active','unprocessed') NOT NULL default 'unprocessed',
  `channel_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL default NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_request_key` (`request_key`),
  KEY `idx_status` (`status`),
  KEY `fk_consumer_request_channel_id` (`channel_id`),
  KEY `fk_consumer_request_client_id` (`client_id`),
  KEY `idx_created_date` (`created_date`),
  KEY `idx_modified_date` (`modified_date`),
  CONSTRAINT `fk_consumer_request_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`),
  CONSTRAINT `fk_consumer_request_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=195126 DEFAULT CHARSET=latin1 COMMENT='Once a consumer request reaches a final state of either acti';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumer_request`
--

LOCK TABLES `consumer_request` WRITE;
/*!40000 ALTER TABLE `consumer_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumer_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumer_request_filter_history`
--

DROP TABLE IF EXISTS `consumer_request_filter_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_request_filter_history` (
  `id` int(11) NOT NULL auto_increment,
  `consumer_request_id` int(11) NOT NULL,
  `question_id` int(11) default NULL,
  `registration_filter_id` int(11) default NULL,
  `description` text NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_consumer_request_id` (`consumer_request_id`),
  KEY `fk_registration_filter_id` (`registration_filter_id`),
  CONSTRAINT `fk_consumer_request_id` FOREIGN KEY (`consumer_request_id`) REFERENCES `consumer_request` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registration_filter_id` FOREIGN KEY (`registration_filter_id`) REFERENCES `registration_filter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14482 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumer_request_filter_history`
--

LOCK TABLES `consumer_request_filter_history` WRITE;
/*!40000 ALTER TABLE `consumer_request_filter_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumer_request_filter_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL auto_increment,
  `first_name` varchar(100) default NULL,
  `last_name` varchar(100) default NULL,
  `email` varchar(250) default NULL,
  `type` enum('primary','billing','lead') NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_first_name` (`first_name`),
  KEY `idx_last_name` (`last_name`)
) ENGINE=InnoDB AUTO_INCREMENT=36207 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_media`
--

DROP TABLE IF EXISTS `contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_media` (
  `id` int(11) NOT NULL auto_increment,
  `number` varchar(50) NOT NULL,
  `is_displayable` tinyint(4) NOT NULL default '1',
  `type` enum('mobile','fax','phone') NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `purpose` enum('billing','primary','display','lead') NOT NULL default 'primary',
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=508570 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_media`
--

LOCK TABLES `contact_media` WRITE;
/*!40000 ALTER TABLE `contact_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_card`
--

DROP TABLE IF EXISTS `credit_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_card` (
  `id` int(11) NOT NULL auto_increment,
  `type` enum('visa','mastercard','amex','diners') NOT NULL,
  `number` varchar(300) NOT NULL,
  `name` text NOT NULL,
  `expiry_month` smallint(2) NOT NULL,
  `expiry_year` smallint(4) NOT NULL,
  `ccv` varchar(5) NOT NULL,
  `created_date` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1750 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_card`
--

LOCK TABLES `credit_card` WRITE;
/*!40000 ALTER TABLE `credit_card` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `external_identifier`
--

DROP TABLE IF EXISTS `external_identifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_identifier` (
  `id` int(11) NOT NULL auto_increment,
  `model_name` varchar(45) NOT NULL,
  `model_id` int(11) NOT NULL,
  `external_id` varchar(100) NOT NULL,
  `external_system` varchar(20) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36914 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `external_identifier`
--

LOCK TABLES `external_identifier` WRITE;
/*!40000 ALTER TABLE `external_identifier` DISABLE KEYS */;
/*!40000 ALTER TABLE `external_identifier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `external_review`
--

DROP TABLE IF EXISTS `external_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_review` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `authority` decimal(5,2) NOT NULL default '0.00',
  `source` varchar(20) NOT NULL,
  `teaser` varchar(255) default NULL,
  `author` varchar(50) default NULL,
  `provider_review_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_provider_id` USING BTREE (`provider_id`),
  KEY `fk_pr_provider_preview_id` (`provider_review_id`),
  CONSTRAINT `fk_er_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_pr_provider_preview_id` FOREIGN KEY (`provider_review_id`) REFERENCES `provider_review` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Review collated about providers from external sources';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `external_review`
--

LOCK TABLES `external_review` WRITE;
/*!40000 ALTER TABLE `external_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `external_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_answer`
--

DROP TABLE IF EXISTS `feedback_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_answer` (
  `id` int(11) NOT NULL auto_increment,
  `question_id` int(11) default NULL,
  `option_code` varchar(10) default NULL,
  `option_id` int(11) default NULL,
  `consumer_feedback_id` int(11) default NULL,
  `text_value` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_question_id` (`question_id`),
  KEY `fk_fa_option_id` (`option_id`),
  KEY `fk_fa_consumer_feedback_id` (`consumer_feedback_id`),
  CONSTRAINT `fk_fa_consumer_feedback_id` FOREIGN KEY (`consumer_feedback_id`) REFERENCES `consumer_feedback` (`id`),
  CONSTRAINT `fk_fa_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`),
  CONSTRAINT `fk_fa_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=119938 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_answer`
--

LOCK TABLES `feedback_answer` WRITE;
/*!40000 ALTER TABLE `feedback_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_spec`
--

DROP TABLE IF EXISTS `job_spec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_spec` (
  `id` int(11) NOT NULL auto_increment,
  `session_id` varchar(50) default NULL,
  `vertical_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `is_preview` tinyint(4) NOT NULL default '0' COMMENT 'Flags whether the job spec was created in a preview function.',
  `is_exclusive_match` tinyint(4) NOT NULL default '0' COMMENT 'Flags whether this is an exclusive match job spec',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `consumer_request_id` int(11) default NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_job_spec_vertical` (`vertical_id`),
  KEY `fk_consumer_request_id` (`consumer_request_id`),
  KEY `pk_id` (`id`),
  KEY `fk_job_spec_client_id` (`client_id`),
  CONSTRAINT `fk_consumer_request` FOREIGN KEY (`consumer_request_id`) REFERENCES `consumer_request` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_spec_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_job_spec_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=196396 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_spec`
--

LOCK TABLES `job_spec` WRITE;
/*!40000 ALTER TABLE `job_spec` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_spec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_spec_detail`
--

DROP TABLE IF EXISTS `job_spec_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_spec_detail` (
  `id` int(11) NOT NULL auto_increment,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) default NULL,
  `job_spec_id` int(11) NOT NULL,
  `text_value` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_job_spec_detail_job_spec` (`job_spec_id`),
  KEY `idx_question_id` (`question_id`),
  KEY `idx_option_id` (`option_id`),
  CONSTRAINT `fk_job_spec_detail_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_spec_detail_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_spec_detail_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3844461 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_spec_detail`
--

LOCK TABLES `job_spec_detail` WRITE;
/*!40000 ALTER TABLE `job_spec_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_spec_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `json_publisher_identity`
--

DROP TABLE IF EXISTS `json_publisher_identity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `json_publisher_identity` (
  `id` int(11) NOT NULL auto_increment,
  `application_identity_id` int(11) NOT NULL,
  `publisher_id` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `publisher_id` USING BTREE (`publisher_id`),
  KEY `fk_application_identity_id` (`application_identity_id`),
  CONSTRAINT `fk_application_identity_id` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `json_publisher_identity`
--

LOCK TABLES `json_publisher_identity` WRITE;
/*!40000 ALTER TABLE `json_publisher_identity` DISABLE KEYS */;
/*!40000 ALTER TABLE `json_publisher_identity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `json_session`
--

DROP TABLE IF EXISTS `json_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `json_session` (
  `id` int(11) NOT NULL auto_increment,
  `session_id` varchar(100) default NULL,
  `type` enum('question','auth','referrer','form-view','conversion') NOT NULL,
  `data_key` varchar(50) default NULL,
  `data` text,
  `ip_address` varchar(16) default NULL,
  `user_agent` varchar(255) default NULL,
  `channel_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_sessionid_key` (`session_id`,`data_key`),
  KEY `fk_json_session_channel_id` (`channel_id`),
  CONSTRAINT `fk_json_session_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13929 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `json_session`
--

LOCK TABLES `json_session` WRITE;
/*!40000 ALTER TABLE `json_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `json_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lead_credit_request`
--

DROP TABLE IF EXISTS `lead_credit_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead_credit_request` (
  `match_set_id` int(11) NOT NULL default '0',
  `status` enum('Declined','Approved','Pending') default 'Pending',
  `comment` varchar(255) default NULL,
  `description` text,
  `internal_description` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL default NULL COMMENT 'Assumed to be date final state was reached',
  PRIMARY KEY  (`match_set_id`),
  KEY `fk_match_set_id` (`match_set_id`),
  KEY `idx_status` (`status`),
  CONSTRAINT `fk_match_set_id` FOREIGN KEY (`match_set_id`) REFERENCES `match_set` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Once a credit request reaches a final state of either Approv';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lead_credit_request`
--

LOCK TABLES `lead_credit_request` WRITE;
/*!40000 ALTER TABLE `lead_credit_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `lead_credit_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lead_plan`
--

DROP TABLE IF EXISTS `lead_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead_plan` (
  `id` int(11) NOT NULL auto_increment,
  `package_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `activation_date` timestamp NULL default NULL,
  `expiry_date` timestamp NULL default NULL,
  `status` enum('pending','expired','cancelled','active') NOT NULL default 'pending',
  `auto_renew` tinyint(1) NOT NULL default '1',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=430 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lead_plan`
--

LOCK TABLES `lead_plan` WRITE;
/*!40000 ALTER TABLE `lead_plan` DISABLE KEYS */;
/*!40000 ALTER TABLE `lead_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL auto_increment,
  `label` varchar(100) default '',
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) default NULL,
  `type` enum('postcode','area') NOT NULL default 'area',
  `left_id` int(11) default NULL,
  `right_id` int(11) default NULL,
  `latitude` varchar(10) default NULL,
  `longitude` varchar(10) default NULL,
  `points_encoded` blob,
  `levels_encoded` blob,
  `alt_label` varchar(100) default NULL COMMENT 'An alternative name for the location',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  `postcode` int(4) default NULL,
  `level` enum('suburb','region','metro','regional','state','country') default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `idx_name` (`name`),
  KEY `idx_postocde` (`postcode`),
  KEY `idx_loc_left_id` (`left_id`),
  KEY `idx_loc_level` (`level`),
  KEY `idx_loc_right_id` (`right_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14314 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match_group`
--

DROP TABLE IF EXISTS `match_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text,
  `status` enum('inactive','active') NOT NULL default 'active',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match_group`
--

LOCK TABLES `match_group` WRITE;
/*!40000 ALTER TABLE `match_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `match_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match_rule`
--

DROP TABLE IF EXISTS `match_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_rule` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) default NULL,
  `default_type` enum('provider','backfill','prospect') NOT NULL default 'provider' COMMENT 'Defines the default for which type of provider to filter on',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match_rule`
--

LOCK TABLES `match_rule` WRITE;
/*!40000 ALTER TABLE `match_rule` DISABLE KEYS */;
INSERT INTO `match_rule` VALUES (1,'Lep_Match_Cap','Filter out providers based on their daily, weekly and monthly lead caps','provider','0000-00-00 00:00:00'),(3,'Lep_Match_RemovalLocations','Filter out providers who do not service the specified pick up and drop off locations','provider','0000-00-00 00:00:00'),(5,'Lep_Match_MatchExact','Filter out providers who haven\'t got the same options that are indicated by the consumer for the tagged questions.','provider','0000-00-00 00:00:00'),(6,'Lep_Match_JobLocation','Filter out providers who don\'t service the specified job location. To be used for verticals which can have only one job location','provider','0000-00-00 00:00:00'),(8,'Lep_Match_MultiLocations','Filter out providers who do not service at least one of the specified location. Note that this rule is used in conjuction with the location-specific and location-multiple questions (e.g.Finance locations).','provider','2009-03-30 01:50:15'),(9,'Lep_Match_JobProximity','Filter out providers who do not service within 20KM the specified job location. Use this in conjuction with Lep_Rank_JobProximity to rank providers based on proximity.','backfill','2009-06-18 07:06:15'),(10,'Lep_Match_RemovalProximity','Filter out providers who do not service within 20KM of the pickup or dropoff locations. Use this in conjuction with Lep_Rank_JobProximity to rank providers based on proximity.','backfill','2009-07-31 01:43:30'),(11,'Lep_Match_MatchPartial','Filter out providers who haven\'t got at least 1 of the options that are indicated by the consumer. Note this filter assumes that if a provider has not selected an option, then all options will apply.','provider','2009-09-18 07:56:24');
/*!40000 ALTER TABLE `match_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match_set`
--

DROP TABLE IF EXISTS `match_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_set` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `job_spec_id` int(11) NOT NULL,
  `campaign_id` int(11) default NULL,
  `lead_plan_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `is_matched` tinyint(4) NOT NULL default '0',
  `ranking_final_score` decimal(5,2) default '0.00',
  `is_prospect` tinyint(4) NOT NULL default '0',
  `lead_delay` timestamp NULL default NULL COMMENT 'Indicates until when the lead should be not seen by the provider',
  PRIMARY KEY  (`id`),
  KEY `fk_match_set_job_spec` (`job_spec_id`),
  KEY `fk_provider_id` (`provider_id`),
  KEY `fk_match_set_campaign_id` (`campaign_id`),
  KEY `idx_created_date` (`created_date`),
  KEY `idx_lead_delay` (`lead_delay`),
  KEY `fk_match_set_lead_plan_id` (`lead_plan_id`),
  CONSTRAINT `fk_match_set_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  CONSTRAINT `fk_match_set_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_set_lead_plan_id` FOREIGN KEY (`lead_plan_id`) REFERENCES `lead_plan` (`id`),
  CONSTRAINT `fk_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=333035 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match_set`
--

LOCK TABLES `match_set` WRITE;
/*!40000 ALTER TABLE `match_set` DISABLE KEYS */;
/*!40000 ALTER TABLE `match_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_audit`
--

DROP TABLE IF EXISTS `message_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_audit` (
  `id` int(11) NOT NULL auto_increment,
  `message_event_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `delivery_method` enum('api','sms','fax','email') default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_message_audit_message_event` (`message_event_id`),
  CONSTRAINT `fk_message_audit_message_event` FOREIGN KEY (`message_event_id`) REFERENCES `message_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_audit`
--

LOCK TABLES `message_audit` WRITE;
/*!40000 ALTER TABLE `message_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `message_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_delivery_option`
--

DROP TABLE IF EXISTS `message_delivery_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_delivery_option` (
  `id` int(11) NOT NULL auto_increment,
  `system_message_event_id` int(11) NOT NULL,
  `delivery_method` enum('fax','sms','email','api') NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `campaign_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_provider_message_delivery_system_message_event` (`system_message_event_id`),
  KEY `fk_message_delivery_option_campaign_id` (`campaign_id`),
  CONSTRAINT `fk_message_delivery_option_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  CONSTRAINT `fk_provider_message_delivery_system_message_event` FOREIGN KEY (`system_message_event_id`) REFERENCES `system_message_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=348 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_delivery_option`
--

LOCK TABLES `message_delivery_option` WRITE;
/*!40000 ALTER TABLE `message_delivery_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `message_delivery_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_event`
--

DROP TABLE IF EXISTS `message_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_event` (
  `id` int(11) NOT NULL auto_increment,
  `system_message_event_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `parent_id` int(11) default NULL,
  `key_id` int(11) default NULL,
  `status` enum('failed','processed','unprocessed') default 'unprocessed',
  `failure_reason` varchar(200) default NULL,
  `trigger_date` timestamp NULL default NULL COMMENT 'Future date for when message should be sent',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_message_event_system_message_event` (`system_message_event_id`),
  KEY `fk_parent_id` (`parent_id`),
  KEY `fk_key_id` (`key_id`),
  KEY `idx_status` (`status`),
  KEY `idx_created_date` (`created_date`),
  KEY `idx_trigger_date` (`trigger_date`),
  KEY `fk_message_event_client_id` (`client_id`),
  CONSTRAINT `fk_message_event_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_message_event_system_message_event` FOREIGN KEY (`system_message_event_id`) REFERENCES `system_message_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `message_event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1727 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_event`
--

LOCK TABLES `message_event` WRITE;
/*!40000 ALTER TABLE `message_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `message_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_template`
--

DROP TABLE IF EXISTS `message_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_template` (
  `id` int(11) NOT NULL auto_increment,
  `message_template_group_id` int(11) NOT NULL,
  `system_message_event_id` int(11) NOT NULL,
  `delivery_method` enum('email','fax','sms') NOT NULL default 'email',
  `template` text,
  `from_email` varchar(255) default NULL,
  `from_name` varchar(100) default NULL,
  `subject` varchar(255) default NULL,
  `status` enum('active','inactive') default 'active',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_system_message_event_id` (`system_message_event_id`),
  KEY `fk_message_template_group_id` (`message_template_group_id`),
  KEY `fk_message_template_client_id` (`client_id`),
  KEY `idx_created_date` (`created_date`),
  CONSTRAINT `fk_message_template_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`),
  CONSTRAINT `fk_system_message_event_id` FOREIGN KEY (`system_message_event_id`) REFERENCES `system_message_event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_template`
--

LOCK TABLES `message_template` WRITE;
/*!40000 ALTER TABLE `message_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `message_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_template_group`
--

DROP TABLE IF EXISTS `message_template_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_template_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `status` enum('active','inactive') default 'active',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_message_template_group_client_id` (`client_id`),
  CONSTRAINT `fk_message_template_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Message template groups defined in the system';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_template_group`
--

LOCK TABLES `message_template_group` WRITE;
/*!40000 ALTER TABLE `message_template_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `message_template_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metadata`
--

DROP TABLE IF EXISTS `metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metadata` (
  `id` int(11) NOT NULL auto_increment,
  `model_id` int(11) NOT NULL,
  `model` varchar(100) NOT NULL,
  `type` enum('force_pending_reason','force_pending','remote_ip','key1','key2','remote_referrer') NOT NULL,
  `data` text NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `metadata_client_id` (`client_id`),
  KEY `metadata_model_id` (`model_id`),
  KEY `metadata_model` (`model`),
  KEY `metadata_type` (`type`),
  CONSTRAINT `metadata_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10382 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metadata`
--

LOCK TABLES `metadata` WRITE;
/*!40000 ALTER TABLE `metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onhold`
--

DROP TABLE IF EXISTS `onhold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `onhold` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `description` text,
  `active` tinyint(1) default '1',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `campaign_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_provider_id` (`provider_id`),
  KEY `fk_onhold_campaign_id` (`campaign_id`),
  CONSTRAINT `fk_onhold_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2624 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onhold`
--

LOCK TABLES `onhold` WRITE;
/*!40000 ALTER TABLE `onhold` DISABLE KEYS */;
/*!40000 ALTER TABLE `onhold` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `option`
--

DROP TABLE IF EXISTS `option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `option` (
  `id` int(11) NOT NULL auto_increment,
  `label` varchar(255) default NULL,
  `real_value` int(11) default NULL,
  `question_id` int(11) default NULL,
  `code` varchar(20) NOT NULL,
  `sequence` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `parent_id` int(11) default NULL,
  `provider_description` text,
  `hint` varchar(100) default NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `default_price` decimal(10,2) NOT NULL default '0.00',
  `default_price_type` enum('$','%') NOT NULL default '$',
  `lead_delay` varchar(20) default NULL COMMENT 'Used to delay leads. Can be duration in hours, days, time or date time.',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_code` (`code`),
  KEY `fk_options_question` (`question_id`),
  KEY `fk_parent_id_option_id` (`parent_id`),
  CONSTRAINT `fk_options_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_id_option_id` FOREIGN KEY (`parent_id`) REFERENCES `option` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6206 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `option`
--

LOCK TABLES `option` WRITE;
/*!40000 ALTER TABLE `option` DISABLE KEYS */;
/*!40000 ALTER TABLE `option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `option_tag`
--

DROP TABLE IF EXISTS `option_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `option_tag` (
  `tag_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`tag_id`,`option_id`),
  KEY `fk_option_tag_tag` (`tag_id`),
  KEY `fk_option_tag_option` (`option_id`),
  CONSTRAINT `fk_option_tag_option` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_option_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `option_tag`
--

LOCK TABLES `option_tag` WRITE;
/*!40000 ALTER TABLE `option_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `option_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package`
--

DROP TABLE IF EXISTS `package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `lead_count` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `client_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `vertical_id` int(11) default NULL COMMENT 'No vertical defined then package applies to all verticals',
  PRIMARY KEY  (`id`),
  KEY `fk_package_client_id` (`client_id`),
  CONSTRAINT `fk_package_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package`
--

LOCK TABLES `package` WRITE;
/*!40000 ALTER TABLE `package` DISABLE KEYS */;
/*!40000 ALTER TABLE `package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_filter`
--

DROP TABLE IF EXISTS `profile_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_filter` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_filter`
--

LOCK TABLES `profile_filter` WRITE;
/*!40000 ALTER TABLE `profile_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_group`
--

DROP TABLE IF EXISTS `profile_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `new_job` tinyint(4) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `vertical_id` int(11) default NULL,
  `description` text NOT NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_vertical_id` (`vertical_id`),
  KEY `fk_profile_group_client_id` (`client_id`),
  KEY `idx_status` (`status`),
  KEY `idx_name` (`name`),
  KEY `idx_created_date` (`created_date`),
  CONSTRAINT `fk_profile_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_vertical_id` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=496 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_group`
--

LOCK TABLES `profile_group` WRITE;
/*!40000 ALTER TABLE `profile_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_rule`
--

DROP TABLE IF EXISTS `profile_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_rule` (
  `id` int(11) NOT NULL auto_increment,
  `target_question_id` int(11) NOT NULL,
  `target_option_id` int(11) default NULL,
  `min_value` int(11) default NULL,
  `max_value` int(11) default NULL,
  `dependency` int(11) default NULL,
  `type` enum('equal','sum') NOT NULL default 'equal',
  `profile_group_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_profile_rule_question` (`target_question_id`),
  KEY `fk_profile_rule_option` (`target_option_id`),
  CONSTRAINT `fk_profile_rule_option` FOREIGN KEY (`target_option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2389 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_rule`
--

LOCK TABLES `profile_rule` WRITE;
/*!40000 ALTER TABLE `profile_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_rule_attribute`
--

DROP TABLE IF EXISTS `profile_rule_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_rule_attribute` (
  `id` int(11) NOT NULL auto_increment,
  `profile_rule_id` int(11) default NULL,
  `source_question_id` int(11) default NULL,
  `source_option_id` int(11) default NULL,
  `filter_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_profile_rule_attribute_profile_rule` (`profile_rule_id`),
  KEY `fk_profile_rule_attribute_profile_question` (`source_question_id`),
  KEY `fk_profile_rule_attribute_profile_option` (`source_option_id`),
  KEY `fk_profile_rule_attribute_profile_filter` (`filter_id`),
  CONSTRAINT `fk_profile_rule_attribute_profile_filter` FOREIGN KEY (`filter_id`) REFERENCES `profile_filter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_rule_attribute_profile_option` FOREIGN KEY (`source_option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_rule_attribute_profile_question` FOREIGN KEY (`source_question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `profile_rule_attribute_ibfk_1` FOREIGN KEY (`profile_rule_id`) REFERENCES `profile_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5605 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_rule_attribute`
--

LOCK TABLES `profile_rule_attribute` WRITE;
/*!40000 ALTER TABLE `profile_rule_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile_rule_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL auto_increment,
  `job_spec_id` int(11) NOT NULL,
  `project_key` varchar(32) NOT NULL,
  `status` enum('Matched','Sub-matched','Pending','Unmatched') default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `address_id` int(11) default NULL,
  `first_name` varchar(100) default NULL,
  `last_name` varchar(100) default NULL,
  `consumer_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_project_job_spec` (`job_spec_id`),
  KEY `fk_project_address` (`address_id`),
  KEY `pk_id` (`id`),
  KEY `idx_project_key` (`project_key`),
  KEY `idx_consumer_name_composite` (`first_name`,`last_name`),
  KEY `idx_status` (`status`),
  KEY `fk_consumer_id` (`consumer_id`),
  KEY `fk_project_client_id` (`client_id`),
  KEY `idx_created_date` (`created_date`),
  CONSTRAINT `consumer_id` FOREIGN KEY (`consumer_id`) REFERENCES `consumer` (`id`),
  CONSTRAINT `fk_project_address` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=195005 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_contact_media`
--

DROP TABLE IF EXISTS `project_contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_contact_media` (
  `project_id` int(11) NOT NULL,
  `contact_media_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`project_id`,`contact_media_id`),
  KEY `fk_project_contact_media_project` (`project_id`),
  KEY `fk_project_contact_media_contact_media` (`contact_media_id`),
  CONSTRAINT `project_contact_media_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `project_contact_media_ibfk_2` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_contact_media`
--

LOCK TABLES `project_contact_media` WRITE;
/*!40000 ALTER TABLE `project_contact_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_contact_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider`
--

DROP TABLE IF EXISTS `provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `is_held` tinyint(4) NOT NULL default '0',
  `status` enum('active','deleted','cancelled','pending','suspended','backfill','prospect') NOT NULL,
  `type` enum('provider','backfill','prospect') NOT NULL default 'provider',
  `ranking_adjustment` decimal(5,2) NOT NULL default '1.00',
  `name_string` varchar(100) default NULL,
  `address_string` varchar(100) default NULL,
  `phone_string` varchar(50) default NULL,
  `comms_opt_out` tinyint(4) default '0' COMMENT '1 = The provider will not receive any communications from LEP (including lead alerts)',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL default NULL,
  `match_group_id` int(11) default NULL,
  `billing_group_id` int(11) default NULL,
  `provider_key` varchar(32) NOT NULL,
  `rating_score` decimal(5,2) default '0.00',
  `client_id` int(11) default NULL,
  `tmp_orig_status` varchar(20) NOT NULL,
  `affiliate_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`),
  KEY `fk_billing_group_id` (`billing_group_id`),
  KEY `fk_match_group` (`match_group_id`),
  KEY `fk_provider_client_id` (`client_id`),
  KEY `idx_status` (`status`),
  KEY `idx_is_held` (`is_held`),
  KEY `idx_created_date` (`created_date`),
  KEY `idx_modified_date` (`modified_date`),
  KEY `fk_provider_affiliate_id` (`affiliate_id`),
  CONSTRAINT `fk_provider_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`),
  CONSTRAINT `fk_billing_group_id` FOREIGN KEY (`billing_group_id`) REFERENCES `billing_group` (`id`),
  CONSTRAINT `fk_match_group` FOREIGN KEY (`match_group_id`) REFERENCES `match_group` (`id`),
  CONSTRAINT `fk_provider_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102597 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider`
--

LOCK TABLES `provider` WRITE;
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_address`
--

DROP TABLE IF EXISTS `provider_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_address` (
  `provider_id` int(11) default NULL,
  `address_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`provider_id`,`address_id`),
  KEY `fk_provider_address_provider` (`provider_id`),
  KEY `fk_provider_address_address` (`address_id`),
  CONSTRAINT `provider_address_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `provider_address_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_address`
--

LOCK TABLES `provider_address` WRITE;
/*!40000 ALTER TABLE `provider_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_attribute`
--

DROP TABLE IF EXISTS `provider_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_attribute` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `provider_id` (`provider_id`),
  CONSTRAINT `provider_attribute_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_attribute`
--

LOCK TABLES `provider_attribute` WRITE;
/*!40000 ALTER TABLE `provider_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_charge`
--

DROP TABLE IF EXISTS `provider_charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_charge` (
  `id` int(11) NOT NULL auto_increment,
  `billing_group_id` int(11) default NULL,
  `provider_id` int(11) NOT NULL,
  `campaign_id` int(11) default NULL,
  `type` enum('lead','lead-credit','lead-plan','misc','misc-credit','subscription','subscription-credit','lead-estimate','lead-credit-estimate') NOT NULL,
  `amount` double(10,2) default NULL,
  `description` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `key_id` int(11) default NULL,
  `key_type` enum('job-spec','lead-plan') default NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_billing_group_id` (`billing_group_id`),
  KEY `idx_provider_id` (`provider_id`),
  KEY `idx_key_id` (`key_id`),
  KEY `fk_provider_charge_campaign_id` (`campaign_id`),
  CONSTRAINT `fk_provider_charge_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=293530 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_charge`
--

LOCK TABLES `provider_charge` WRITE;
/*!40000 ALTER TABLE `provider_charge` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_charge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_contact`
--

DROP TABLE IF EXISTS `provider_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_contact` (
  `provider_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`provider_id`,`contact_id`),
  KEY `fk_providers_contacts_providers` (`provider_id`),
  KEY `fk_providers_contacts_contacts` (`contact_id`),
  CONSTRAINT `provider_contact_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `provider_contact_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_contact`
--

LOCK TABLES `provider_contact` WRITE;
/*!40000 ALTER TABLE `provider_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_contact_media`
--

DROP TABLE IF EXISTS `provider_contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_contact_media` (
  `contact_media_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`provider_id`,`contact_media_id`),
  KEY `fk_provider_contact_contact_media` (`contact_media_id`),
  KEY `fk_provider_contact_media_provider` (`provider_id`),
  CONSTRAINT `provider_contact_media_ibfk_1` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `provider_contact_media_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_contact_media`
--

LOCK TABLES `provider_contact_media` WRITE;
/*!40000 ALTER TABLE `provider_contact_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_contact_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_message_params`
--

DROP TABLE IF EXISTS `provider_message_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_message_params` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) default NULL,
  `params` text,
  `created_date` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_provider_id` (`provider_id`),
  CONSTRAINT `fkprovider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_message_params`
--

LOCK TABLES `provider_message_params` WRITE;
/*!40000 ALTER TABLE `provider_message_params` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_message_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_review`
--

DROP TABLE IF EXISTS `provider_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_review` (
  `id` int(11) NOT NULL auto_increment,
  `type` enum('external','internal') NOT NULL,
  `provider_id` int(11) NOT NULL,
  `project_id` int(11) default NULL,
  `score` decimal(5,2) NOT NULL default '0.00',
  `status` enum('active','inactive') NOT NULL default 'active',
  `comment` text NOT NULL,
  `response` text NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_pr_provider_id` (`provider_id`),
  KEY `fk_pr_project_id` (`project_id`),
  CONSTRAINT `fk_pr_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `fk_pr_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3097 DEFAULT CHARSET=latin1 COMMENT='An aggregate table of internal and external provider reviews';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_review`
--

LOCK TABLES `provider_review` WRITE;
/*!40000 ALTER TABLE `provider_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_subscription`
--

DROP TABLE IF EXISTS `provider_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_subscription` (
  `id` int(11) NOT NULL auto_increment,
  `service_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `subscription_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `amount` decimal(10,2) NOT NULL default '0.00',
  `auto_renew` tinyint(1) NOT NULL default '1',
  `cancellation_date` timestamp NULL default NULL,
  `billing_period` enum('Annually','Monthly') NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_service_id` (`service_id`),
  KEY `fk_providerid` (`provider_id`),
  CONSTRAINT `fk_providerid` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_service_id` FOREIGN KEY (`service_id`) REFERENCES `subscription_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_subscription`
--

LOCK TABLES `provider_subscription` WRITE;
/*!40000 ALTER TABLE `provider_subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_subscription_charge`
--

DROP TABLE IF EXISTS `provider_subscription_charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_subscription_charge` (
  `id` int(11) NOT NULL auto_increment,
  `provider_subscription_id` int(11) NOT NULL,
  `charge_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_provider_subscription_id` (`provider_subscription_id`),
  KEY `fk_charge_id` (`charge_id`),
  CONSTRAINT `fk_chargeid` FOREIGN KEY (`charge_id`) REFERENCES `provider_charge` (`id`),
  CONSTRAINT `fk_charge_id` FOREIGN KEY (`charge_id`) REFERENCES `provider_charge` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_subscription_charge`
--

LOCK TABLES `provider_subscription_charge` WRITE;
/*!40000 ALTER TABLE `provider_subscription_charge` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_subscription_charge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_vertical`
--

DROP TABLE IF EXISTS `provider_vertical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_vertical` (
  `provider_id` int(11) NOT NULL,
  `vertical_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`vertical_id`,`provider_id`),
  KEY `fk_providers_verticals_providers` (`provider_id`),
  KEY `fk_providers_verticals_verticals` (`vertical_id`),
  CONSTRAINT `fk_providers_verticals_verticals` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `provider_vertical_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_vertical`
--

LOCK TABLES `provider_vertical` WRITE;
/*!40000 ALTER TABLE `provider_vertical` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_vertical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL auto_increment,
  `label` varchar(255) default NULL,
  `description` text,
  `type` enum('checkbox','date','display','location','location-specific','password','radio','rating','select','slider','text','textarea') NOT NULL,
  `vertical_id` int(11) default NULL,
  `is_mandatory` tinyint(4) NOT NULL default '0',
  `status` enum('active','inactive') NOT NULL default 'active',
  `code` varchar(20) NOT NULL,
  `internal_description` text,
  `provider_description` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `min_value` int(11) default NULL,
  `min_label` varchar(100) default NULL,
  `max_value` int(11) default NULL,
  `max_label` varchar(100) default NULL,
  `help` text,
  `help_url` varchar(255) default NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_code` (`code`),
  KEY `fk_question_vertical` (`vertical_id`),
  KEY `pk_id` (`id`),
  KEY `fk_question_client_id` (`client_id`),
  KEY `idx_label` (`label`),
  KEY `idx_status` (`status`),
  KEY `idx_type` (`type`),
  KEY `idx_created_date` (`created_date`),
  CONSTRAINT `fk_question_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_question_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4388 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_option_tree`
--

DROP TABLE IF EXISTS `question_option_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_option_tree` (
  `id` int(11) NOT NULL auto_increment,
  `parent_option_id` int(11) default NULL,
  `child_question_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `is_mandatory` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `fk_question_option_tree_option` (`parent_option_id`),
  KEY `fk_question_option_tree_question` (`child_question_id`),
  KEY `fk_question_option_tree_channel` (`channel_id`),
  CONSTRAINT `fk_question_option_tree_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_option_tree_option` FOREIGN KEY (`parent_option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_option_tree_question` FOREIGN KEY (`child_question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=87455 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_option_tree`
--

LOCK TABLES `question_option_tree` WRITE;
/*!40000 ALTER TABLE `question_option_tree` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_option_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_tag`
--

DROP TABLE IF EXISTS `question_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_tag` (
  `question_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`tag_id`,`question_id`),
  KEY `fk_question_tag_question` (`question_id`),
  KEY `fk_question_tag_tag` (`tag_id`),
  CONSTRAINT `fk_question_tag_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_tag`
--

LOCK TABLES `question_tag` WRITE;
/*!40000 ALTER TABLE `question_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` int(11) NOT NULL auto_increment,
  `status` enum('processing','failed','processed','unprocessed') NOT NULL default 'unprocessed',
  `notification_email` varchar(250) default NULL,
  `data` longblob NOT NULL,
  `type` enum('KmlImport','ProviderImport') NOT NULL,
  `description` varchar(250) default NULL,
  `failure_reason` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  `client_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_queue_client_id` (`client_id`),
  CONSTRAINT `fk_queue_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Generic queue bucket for LEP';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue`
--

LOCK TABLES `queue` WRITE;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ranking_rule`
--

DROP TABLE IF EXISTS `ranking_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ranking_rule` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `parent_match_rule_id` int(11) default NULL,
  `description` varchar(255) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ranking_rule`
--

LOCK TABLES `ranking_rule` WRITE;
/*!40000 ALTER TABLE `ranking_rule` DISABLE KEYS */;
INSERT INTO `ranking_rule` VALUES (1,'Lep_Rank_TimeLastMatch',NULL,'Rank providers based on last time they were matched','0000-00-00 00:00:00'),(2,'Lep_Rank_EffectiveLeadPrice',NULL,'Effective Lead Price:Average price per lead (minus credits) for the match criteria of the project being ranked','0000-00-00 00:00:00'),(3,'Lep_Rank_CampaignPreference',NULL,'Ranked based on a provider\'s preference to accept certail jobs','2008-12-18 02:07:46'),(4,'Lep_Rank_ProviderSuitability',NULL,'Rank providers based on their suitability score. i.e. Average Rating for same job types','2008-12-18 03:36:52'),(5,'Lep_Rank_RatingQuality',NULL,'Rank providers based on their average Quality Rating score','2008-12-22 02:59:58'),(6,'Lep_Rank_CurrentLeadPrice',NULL,'Rank providers based on their current BASE lead price','2009-01-28 02:20:37'),(7,'Lep_Rank_ConsumerLocationPreference',8,'Rank providers based on whether to service the consumer\'s preferred location or supplementary locations','2009-03-30 02:50:08'),(8,'Lep_Rank_ProviderAverageRating',NULL,'Rank providers based on their average rating. Average rating is a product of both consumer feedback ratings and external review scores','2010-03-09 20:57:07'),(9,'Lep_Rank_JobProximity',NULL,'Rank providers on their proximity to a job location. NOTE: This rule must be used in conjunction with either Lep_Match_JobProximity or Lep_Match_RemovalProximity.','2010-09-22 00:41:20'),(10,'Lep_Rank_CapLevel',NULL,'Rank providers based on their current caps and lead counts.','2010-11-22 00:19:50');
/*!40000 ALTER TABLE `ranking_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registration_filter`
--

DROP TABLE IF EXISTS `registration_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_filter` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration_filter`
--

LOCK TABLES `registration_filter` WRITE;
/*!40000 ALTER TABLE `registration_filter` DISABLE KEYS */;
INSERT INTO `registration_filter` VALUES (1,'Lep_RegistrationFilter_Email','Checks for a valid email address.','2009-05-01 00:38:27'),(2,'Lep_RegistrationFilter_Name','Checks for valid first and last names.','2009-05-01 00:38:50'),(3,'Lep_RegistrationFilter_Phone','Checks for valid phone numbers.','2009-05-01 00:59:25'),(4,'Lep_RegistrationFilter_BlackList','Checks a registration event for black listed words.','2009-05-01 01:52:14'),(5,'Lep_RegistrationFilter_Duplicate','Checks for duplicate registrations.','2009-09-25 03:15:19'),(6,'Lep_RegistrationFilter_All','Filter all registrations indiscriminately.','2010-08-05 01:32:47'),(7,'Lep_RegistrationFilter_ConsumerIpAddress','Checks for duplicate registrations using the same IP address','2011-01-14 08:27:41'),(8,'Lep_RegistrationFilter_ForceToPending','Forces a registration to pending depending on the ForcePending flag','2011-01-14 08:27:41');
/*!40000 ALTER TABLE `registration_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `remarket`
--

DROP TABLE IF EXISTS `remarket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remarket` (
  `id` int(11) NOT NULL auto_increment,
  `vertical_id` int(11) default NULL,
  `channel_id` int(11) default NULL,
  `delay` int(11) default NULL,
  `created_date` timestamp NULL default CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_remarket_client_id` (`client_id`),
  CONSTRAINT `fk_remarket_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remarket`
--

LOCK TABLES `remarket` WRITE;
/*!40000 ALTER TABLE `remarket` DISABLE KEYS */;
/*!40000 ALTER TABLE `remarket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_area`
--

DROP TABLE IF EXISTS `service_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_area` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `question_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `campaign_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `idx_unique_location` (`provider_id`,`campaign_id`,`question_id`,`postcode`),
  KEY `fk_provider_service_area_provider` (`provider_id`),
  KEY `fk_provider_service_area_question` (`question_id`),
  KEY `idx_postcode` (`postcode`),
  KEY `provider_id_question_id` (`provider_id`,`question_id`),
  CONSTRAINT `fk_provider_service_area_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `service_area_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26862 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_area`
--

LOCK TABLES `service_area` WRITE;
/*!40000 ALTER TABLE `service_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription_service`
--

DROP TABLE IF EXISTS `subscription_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_service` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `code` varchar(20) NOT NULL,
  `attributes` text,
  `modified_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription_service`
--

LOCK TABLES `subscription_service` WRITE;
/*!40000 ALTER TABLE `subscription_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_message_event`
--

DROP TABLE IF EXISTS `system_message_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_message_event` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(100) NOT NULL,
  `name` varchar(100) default NULL,
  `description` varchar(255) default NULL,
  `applicable_delivery_methods` varchar(20) default NULL COMMENT 'Comma delimited list of delivery methods applicable for this system message event',
  `token_description` text COMMENT 'Describes tokens which are available to message templates derived from this system message event',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_message_event`
--

LOCK TABLES `system_message_event` WRITE;
/*!40000 ALTER TABLE `system_message_event` DISABLE KEYS */;
INSERT INTO `system_message_event` VALUES (1,'LeadAlert','Lead Alert',NULL,'email,sms,fax','<b>$vertical</b>  (Vertical name)\r\n<b>$provider_name</b>\r\n<b>$provider_key</b> (Identifying key of provider)\r\n<b>$lead_date</b> (Date and time lead was generated)\r\n<b>$lead_delay_date</b> (Date and time lead was delayed (if any))\r\n<b>$email</b> (Email address that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$fax</b> (Fax that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$mobile</b> (Mobile number that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$job_description</b> (Descriptive text for the project. Available in both the message body and message subject.)\r\n<b>$product_description</b> (Main product option of project. Available in both the message body and message subject.)\r\n<b>$consumer_inputs</b> (An array where each item consists of $question_label $option_label and $text_value)\r\n<b>$consumer_name</b> (Is available in both the message body and message subject)\r\n<b>$consumer_address1</b>\r\n<b>$consumer_address2</b>\r\n<b>$consumer_postcode</b>\r\n<b>$consumer_city</b>\r\n<b>$consumer_email</b>\r\n<b>$consumer_phone</b>\r\n<b>$consumer_mobile</b>\r\n<b>$best_contact_time</b>\r\n<b>$message_subject</b>','2009-01-22 06:42:11'),(2,'MatchResults','Match Results','Generates an email to the consumer in response registration matching','email','<b>$email</b>  (Consumer\'s email address)\r\n<b>$consumer_name</b>  (Consumer\'s name)\r\n<b>$consumer_key</b>  (Consumer\'s unique key)\r\n<b>$project_data</b>  (Array of projects. See below for description of indexes.)\r\n\r\nTokens available to a project\r\n<b>$vertical</b> (Vertical assigned to project)\r\n<b>$status</b> (Status of project)\r\n<b>$providers</b> (Array or providers assigned to project)\r\n<b>$backfill</b> (Array or backfill providers assigned to project)\r\n\r\nTokens available to a provider\r\n<b>$name</b> (Provider\'s display name)\r\n<b>$address</b> (Provider\'s display address)\r\n<b>$rating</b> (Provider\'s rating score)\r\n<b>$id</b> (Provider Id)\r\n<b>$contact_name</b>\r\n<b>$email</b>\r\n<b>$phone</b>\r\n<b>$fax</b>\r\n\r\nTokens available to a backfill provider\r\n<b>$name</b>\r\n<b>$address</b>\r\n<b>$phone</b>','2009-01-22 06:42:18'),(4,'24HourFollowUp','24 Hour Follow Up','Sends a \"Have you been contacted?\" email to the consumer 24 hours after their project was submitted.','email','<b>$email</b>  (Consumer\'s email address)\r\n<b>$consumer_key</b> (Consumer identifying key)\r\n<b>$consumer_name</b> (Consumer\'s name)','2009-07-08 07:38:16'),(5,'RatingRequest','Ratings Request','Sends a Ratings Request email to the consumer.','email','<b>$consumer_email_key</b>  (Consumer\'s email address hash)\r\n<b>$consumer_key</b> (Consumer identifying key)\r\n<b>$consumer_name</b> (Consumer\'s name)','2009-07-08 07:38:16'),(6,'RequestARating','Request A Rating','A provider initiated Ratings Request email to the consumer.','email','<b>$email</b>  (Consumer\'s email address)\r\n<b>$consumer_key</b> (Consumer identifying key)\r\n<b>$consumer_name</b> (Consumer\'s name)\r\n<b>$provider_name</b> (Name of provider who requested the rating)\r\n<b>$description</b> (Project description)','2009-07-08 07:38:16'),(8,'LeadCredit','Lead Credited','Email sent to a provider when a lead is credited.','email','<b>$description</b>  (Credit description)\r\n<b>$provider_key</b> (Identifying key of provider)\r\n<b>$provider_contact</b> (Name of provider contact)\r\n<b>$submitted</b> (Date & time the credit was submitted)\r\n<b>$status</b> (Status of the credit)\r\n<b>$comment</b> (Lead credit comment)\r\n<b>$internal_description</b> (Credit\'s internal description)\r\n<b>$email</b> (Provider\'s email address)\r\n<b>$id</b> (Lead Id which is assigned the credit. Available in both the message body and message subject.)\r\n<b>$consumer_name</b> (Name of consumer who submitted the project. Available in both the message body and message subject.)','2009-07-08 07:38:16'),(9,'LeadAlertReminder','Lead Alert Reminder','A reminder sent to a provider reminding them of a lead.','email,sms,fax','<b>$vertical</b>  (Vertical name)\r\n<b>$provider_name</b>\r\n<b>$provider_key</b> (Identifying key of provider)\r\n<b>$lead_date</b> (Date and time lead was generated)\r\n<b>$lead_delay_date</b> (Date and time lead was delayed (if any))\r\n<b>$email</b> (Email address that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$fax</b> (Fax that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$mobile</b> (Mobile number that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$job_description</b> (Descriptive text for the project. Available in both the message body and message subject.)\r\n<b>$product_description</b> (Main product option of project. Available in both the message body and message subject.)\r\n<b>$consumer_inputs</b> (An array where each item consists of $question_label $option_label and $text_value)\r\n<b>$consumer_name</b> (Is available in both the message body and message subject)\r\n<b>$consumer_address1</b>\r\n<b>$consumer_address2</b>\r\n<b>$consumer_postcode</b>\r\n<b>$consumer_city</b>\r\n<b>$consumer_email</b>\r\n<b>$consumer_phone</b>\r\n<b>$consumer_mobile</b>\r\n<b>$best_contact_time</b>\r\n<b>$message_subject</b>','2009-07-08 07:38:16'),(10,'NewAccount','New Account','An email sent to the provider when an account is created outside of Drupal','email','<b>$contact_name</b>  (Provider contact)\r\n<b>$provider_name</b>\r\n<b>$provider_key</b> (Provider\'s unique key)\r\n<b>$provider_id</b> \r\n<b>$hash</b> (Unique hash for email)','2009-07-08 07:38:16'),(11,'LeadProspectAlert','Lead Prospect Alert','Lead alerts sent to prospect','email,sms,fax','<b>$vertical</b>  (Vertical name)\r\n<b>$provider_name</b>\r\n<b>$provider_key</b> (Identifying key of provider)\r\n<b>$lead_date</b> (Date and time lead was generated)\r\n<b>$lead_delay_date</b> (Date and time lead was delayed (if any))\r\n<b>$email</b> (Email address that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$fax</b> (Fax that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$mobile</b> (Mobile number that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$job_description</b> (Descriptive text for the project. Available in both the message body and message subject.)\r\n<b>$product_description</b> (Main product option of project. Available in both the message body and message subject.)\r\n<b>$consumer_inputs</b> (An array where each item consists of $question_label $option_label and $text_value)\r\n<b>$consumer_name</b> (Is available in both the message body and message subject)\r\n<b>$consumer_address1</b>\r\n<b>$consumer_address2</b>\r\n<b>$consumer_postcode</b>\r\n<b>$consumer_city</b>\r\n<b>$consumer_email</b>\r\n<b>$consumer_phone</b>\r\n<b>$consumer_mobile</b>\r\n<b>$best_contact_time</b>\r\n<b>$message_subject</b>\r\n<b>$lead_count</b>  (Number of leads assigned to the prospect)\r\n\r\n<b>$next_provider_name</b>  \r\nName of another random prospect that could be matched to future projects. Note that\r\nthis may not\r\n','2010-09-22 00:41:18'),(12,'Remarket','Remarketing Email','Remarketing email sent to consumer n days after a registration','email','<b>$consumer_key</b>  (Consumer\'s Key)\r\n<b>$consumer_name</b> (Is available in both the message body and message subject)\r\n<b>$project_date</b> (Date the initial project was submitted)\r\n<b>$vertical_id</b> (Vertical Id of project)\r\n<b>$vertical_name</b> (Name of vertical attached to the project)\r\n<b>$channel_id</b> (Channel Id of project)\r\n<b>$channel_name</b> (Name of channel attached to the project)\r\n<b>$remarket_channel_id</b> (Id of channel that is being marketed to the consumer)\r\n<b>$remarket_channel_name</b> (Name of channel that is being marketed to the consumer)','2010-09-22 00:43:21');
/*!40000 ALTER TABLE `system_message_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` text,
  `is_system_tag` tinyint(1) NOT NULL default '0',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `client_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `fk_tag_client_id` (`client_id`),
  CONSTRAINT `fk_tag_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (1,'Pick Up Location','Identifies removals pick up location questions',1,'0000-00-00 00:00:00',NULL),(2,'Drop Off Location','Identifies removals drop-off location questions',1,'0000-00-00 00:00:00',NULL),(4,'Match Exact','Identifies questions used with Lep_Match_MatchExact match rules',1,'0000-00-00 00:00:00',NULL),(5,'Email','Identifies consumer email questions',1,'0000-00-00 00:00:00',NULL),(6,'First Name','Identifies consumer first name questions',1,'0000-00-00 00:00:00',NULL),(7,'Last Name','Identifies consumer last name questions',1,'0000-00-00 00:00:00',NULL),(8,'Address Line 1','Identifies consumer address line 1 questions',1,'0000-00-00 00:00:00',NULL),(10,'Provider Profile','Identifies questions used when profiling a provider',1,'0000-00-00 00:00:00',NULL),(12,'Provider Preference','Identifies questions used when ranking a provider based on preference',1,'2008-12-18 02:32:31',NULL),(16,'Address Line 2','Identifies consumer address line 2 questions',1,'2009-01-02 00:51:16',NULL),(17,'Address City','Identifies consumer city questions',1,'2009-01-02 00:51:23',NULL),(18,'Address Postcode','Identifies consumer postcode questions',1,'2009-01-02 00:51:28',NULL),(19,'Address Region','Identifies consumer region questions',1,'2009-01-02 00:51:34',NULL),(21,'Job Location','Identifies job location and/or areas serviced',1,'2009-01-12 00:32:57',NULL),(22,'Supplementary Charge','Identifies question which can have a supplementary charge component assigned to them',1,'2009-01-23 04:34:19',NULL),(23,'Base Charge','Identifies question which can have a base charge component assigned to them',1,'2009-01-23 04:34:40',NULL),(24,'Calendar','Indicates that a UI element contains a calendar component',1,'2009-03-23 00:18:49',NULL),(25,'Other','Indicates that a UI element contains a text field component where the user can enter in free text classed as \"Other\"',1,'2009-03-23 03:59:14',NULL),(26,'Registration End Point','Indicates which options when selected should terminate the registration process',1,'2009-03-24 00:54:55',NULL),(27,'Consumer Primary Location','Identifies the consumers primary job location preference. This question does not apply to a provider and should not be used in Channels where only one job location is collected. Use the \"Job location\" tag instead.',0,'2009-03-30 02:09:33',NULL),(28,'Consumer Supplementary Location','Identifies the consumers supplementary job location preferences. This question does not apply to a provider and should not be used in Channels where only one job location is collected. Use the \"Job location\" tag instead.',0,'2009-03-30 02:09:33',NULL),(29,'Feedback','Identifies questions used for the feedback of a project',1,'2009-03-11 01:36:22',NULL),(30,'Provider List','Identifies questions which require a dynamic provider list to be provided as options',1,'2009-03-18 22:42:25',NULL),(31,'Feedback End Point','Identifies questions that are end nodes',1,'2009-03-19 05:48:33',NULL),(32,'Feedback Return Point','Identifies the question that is a return point for the feedback questions',1,'2009-03-19 05:54:15',NULL),(33,'Rated Provider','Identifies the question that signifies a rated provider',1,'2009-03-26 04:24:40',NULL),(34,'Rating','Identifies the question that is a feedback rating',1,'2009-03-26 04:29:53',NULL),(35,'Provider Chosen','Identifies the questions that illustrate a provider has been chosen from the feedback questions',1,'2009-03-26 05:05:59',NULL),(38,'Quality','Identifies \"Quality\" rating questions',1,'2008-12-22 03:01:15',NULL),(39,'Phone','Identifies a phone number submitted with a Consumer Request',1,'2009-04-02 03:04:13',NULL),(40,'Feedback Comment','Identifies a feedback comments question',1,'2008-12-22 03:01:15',NULL),(41,'Mobile','Identifies a mobile number submitted with a Consumer Request',1,'2009-06-18 07:06:15',NULL),(42,'Preferred Contact Time','Identifies consumer\'s preferred contact time',1,'2009-07-08 07:38:16',NULL),(43,'Feedback Lead Reminder','When chosen by a consumer, questions with this tag will generate a Lead Reminder Alert',1,'2009-07-08 07:38:16',NULL),(44,'Match Partial','Identifies questions used with Lep_Match_MatchPartial match rules',1,'2009-09-18 07:49:50',NULL),(45,'Lead Delay','Enables assigning lead delays to a question.',1,'2010-09-22 00:41:37',NULL);
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL auto_increment,
  `question_id` int(11) default NULL,
  `option_id` int(11) default NULL,
  `provider_id` int(11) NOT NULL,
  `campaign_id` int(11) default NULL,
  `job_spec_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `percentage` decimal(10,2) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_transaction_question` (`question_id`),
  KEY `fk_transaction_option` (`option_id`),
  KEY `fk_transaction_provider` (`provider_id`),
  KEY `fk_transaction_job_spec` (`job_spec_id`),
  KEY `fk_transaction_campaign_id` (`campaign_id`),
  CONSTRAINT `fk_transaction_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  CONSTRAINT `fk_transaction_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_option` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_provider` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unmatched_campaign`
--

DROP TABLE IF EXISTS `unmatched_campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unmatched_campaign` (
  `id` int(11) NOT NULL auto_increment,
  `provider_id` int(11) NOT NULL,
  `job_spec_id` int(11) NOT NULL,
  `campaign_id` int(11) default NULL,
  `reason` varchar(200) NOT NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `is_prospect` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `fk_unmatched_provider_job_spec` (`job_spec_id`),
  KEY `fk_unmatched_provider_campaign_id` (`campaign_id`),
  CONSTRAINT `fk_unmatched_provider_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  CONSTRAINT `fk_unmatched_provider_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4034943 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unmatched_campaign`
--

LOCK TABLES `unmatched_campaign` WRITE;
/*!40000 ALTER TABLE `unmatched_campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `unmatched_campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(255) NOT NULL default '',
  `password` varchar(255) NOT NULL default '',
  `real_name` varchar(255) default '',
  `email` varchar(250) default NULL,
  `status` enum('inactive','active') NOT NULL default 'active',
  `affiliate_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_username` USING BTREE (`username`),
  KEY `fk_user_affiliate_id` (`affiliate_id`),
  CONSTRAINT `fk_user_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'administrator','aa5af1a498a1227904e3e49dbbd0f4e60fa366c9ed9c9a0fb3036898eed506cd','Administrator',NULL,'active',NULL,'2011-01-31 06:04:02');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL default '',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (58,1,'administrator','2011-01-31 06:04:02'),(59,1,'channel-manager','2011-01-31 06:04:02');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vertical`
--

DROP TABLE IF EXISTS `vertical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertical` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `status` enum('active','inactive') NOT NULL default 'active',
  `feedback_days` int(11) NOT NULL default '0' COMMENT 'Number of days after a project submission when a feedback request email will be sent out',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `prospect_slots` int(11) default '0' COMMENT 'Number of slots made available for prospects',
  `force_prospects` tinyint(4) default '0' COMMENT 'Reserves slots in the final match for prospects regardless of coverage',
  `client_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`),
  KEY `fk_vertical_client_id` (`client_id`),
  KEY `idx_status` (`status`),
  KEY `idx_created_date` (`created_date`),
  CONSTRAINT `fk_vertical_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vertical`
--

LOCK TABLES `vertical` WRITE;
/*!40000 ALTER TABLE `vertical` DISABLE KEYS */;
/*!40000 ALTER TABLE `vertical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vertical_match_rule`
--

DROP TABLE IF EXISTS `vertical_match_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertical_match_rule` (
  `id` int(11) NOT NULL auto_increment,
  `match_rule_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL default '0',
  `vertical_id` int(11) default NULL,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `type` enum('provider','backfill','prospect') NOT NULL default 'provider',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uk_vertical_match_rule1` (`vertical_id`,`match_rule_id`,`type`),
  KEY `fk_channel_match_rule_match_rule` (`match_rule_id`),
  KEY `fk_channel_match_rule_vertical` (`vertical_id`),
  CONSTRAINT `fk_channel_match_rule_match_rule` FOREIGN KEY (`match_rule_id`) REFERENCES `match_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_channel_match_rule_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2543 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vertical_match_rule`
--

LOCK TABLES `vertical_match_rule` WRITE;
/*!40000 ALTER TABLE `vertical_match_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `vertical_match_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vertical_ranking_rule`
--

DROP TABLE IF EXISTS `vertical_ranking_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertical_ranking_rule` (
  `id` int(11) NOT NULL auto_increment,
  `ranking_rule_id` int(11) NOT NULL,
  `vertical_id` int(11) NOT NULL,
  `weight` int(11) NOT NULL COMMENT 'A weight from 1 to 100',
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `fk_vertical_ranking_attribute_ranking_attribute` (`ranking_rule_id`),
  KEY `fk_vertical_ranking_attribute_vertical` (`vertical_id`),
  CONSTRAINT `fk_vertical_ranking_attribute_ranking_rule` FOREIGN KEY (`ranking_rule_id`) REFERENCES `ranking_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vertical_ranking_attribute_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2071 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vertical_ranking_rule`
--

LOCK TABLES `vertical_ranking_rule` WRITE;
/*!40000 ALTER TABLE `vertical_ranking_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `vertical_ranking_rule` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-01-31 17:07:39
