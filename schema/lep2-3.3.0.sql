
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `RT Campaign Cap`;
/*!50001 DROP VIEW IF EXISTS `RT Campaign Cap`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `RT Campaign Cap` (
  `pid` int(11),
  `cid` int(11),
  `name` varchar(100),
  `reason` varchar(7)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `RT Provider Cap`;
/*!50001 DROP VIEW IF EXISTS `RT Provider Cap`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `RT Provider Cap` (
  `pid` int(11),
  `cid` int(11),
  `CAST(GROUP_CONCAT(vv.name) AS CHAR)` longtext,
  `reason` varchar(7)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `acl_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `acl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_acl_role_client_id` (`client_id`),
  CONSTRAINT `fk_acl_role_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `acl_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission` enum('allow','deny') NOT NULL,
  `privilege` enum('view','delete','insert','update','select') DEFAULT NULL,
  `acl_resource_id` int(11) DEFAULT NULL,
  `acl_role_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_acl_role_permission_acl_resource` (`acl_resource_id`),
  KEY `fk_acl_role_permission_acl_role` (`acl_role_id`),
  CONSTRAINT `fk_acl_role_permission_acl_resource` FOREIGN KEY (`acl_resource_id`) REFERENCES `acl_resource` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_acl_role_permission_acl_role` FOREIGN KEY (`acl_role_id`) REFERENCES `acl_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('billing','street') NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=383047 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `admin_user_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user_provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `affiliate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `client_id` int(11) NOT NULL,
  `publisher_key` varchar(50) DEFAULT NULL COMMENT 'Publisher key to access JSON services',
  `application_identity_id` int(11) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `status` enum('inactive','active') NOT NULL DEFAULT 'active',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_affiliate_client_id` (`client_id`),
  KEY `fk_affiliate_application_identity_id` (`application_identity_id`),
  CONSTRAINT `fk_affiliate_application_identity_id` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`),
  CONSTRAINT `fk_affiliate_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `affiliate_bucket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliate_bucket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_id` int(11) NOT NULL,
  `bucket_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_affiliate_bucket` (`bucket_id`,`affiliate_id`),
  KEY `affiliate_bucket_bucket_id` (`bucket_id`),
  KEY `affiliate_bucket_affiliate_id` (`affiliate_id`),
  CONSTRAINT `affiliate_bucket_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `affiliate_bucket_bucket_id` FOREIGN KEY (`bucket_id`) REFERENCES `bucket` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='Maps affiliates to buckets';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `affiliate_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliate_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT '',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_affiliate_role1` (`affiliate_id`,`role`),
  KEY `fk_affiliate_role_affiliate_id` (`affiliate_id`),
  CONSTRAINT `fk_affiliate_role_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `api_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nonce` varchar(250) NOT NULL,
  `application_identity_id` int(11) NOT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiry_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `nonce_key` (`nonce`),
  KEY `fk_application_identity` (`application_identity_id`),
  CONSTRAINT `fk_application_identity` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21104972 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `application_identity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_identity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `real_name` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_username` (`username`),
  KEY `fk_client_id` (`client_id`),
  KEY `idx_password` (`password`),
  CONSTRAINT `fk_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `application_identity_acl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_identity_acl_role` (
  `application_identity_id` int(11) DEFAULT NULL,
  `acl_role_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`application_identity_id`,`acl_role_id`),
  KEY `fk_application_identity_acl_role_application_identity` (`application_identity_id`),
  KEY `fk_application_identity_acl_role_acl_role` (`acl_role_id`),
  CONSTRAINT `fk_application_identity_acl_role_acl_role` FOREIGN KEY (`acl_role_id`) REFERENCES `acl_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_application_identity_acl_role_application_identity` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `application_identity_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_identity_channel` (
  `application_identity_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`application_identity_id`,`channel_id`),
  KEY `fk_application_identity_channel_application_identity` (`application_identity_id`),
  KEY `fk_application_identity_channel_channel` (`channel_id`),
  CONSTRAINT `fk_application_identity_channel_application_identity` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_application_identity_channel_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `applied_match_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applied_match_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `match_rule_id` int(11) NOT NULL,
  `job_spec_id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_match_rule` (`match_rule_id`),
  KEY `fk_job_spec` (`job_spec_id`),
  CONSTRAINT `fk_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_rule` FOREIGN KEY (`match_rule_id`) REFERENCES `match_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7835889 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `audit_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(100) NOT NULL,
  `parent_model` varchar(100) DEFAULT NULL,
  `model_id` int(11) NOT NULL,
  `parent_model_id` int(11) DEFAULT NULL,
  `field` varchar(100) NOT NULL,
  `old` varchar(255) DEFAULT NULL,
  `new` varchar(255) DEFAULT NULL,
  `app_identity` int(11) DEFAULT NULL,
  `audit_message` text,
  `is_model_change` tinyint(4) DEFAULT '1' COMMENT '1 = change to an actual model. 0 = a high level change.',
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_parent_model` (`parent_model`),
  KEY `idx_parent_model_id` (`parent_model_id`),
  KEY `idx_model` (`model`),
  KEY `idx_model_id` (`model_id`),
  KEY `idx_application_identity` (`app_identity`)
) ENGINE=InnoDB AUTO_INCREMENT=11400587 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `audit_log_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_log_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `field` varchar(100) NOT NULL,
  `old` varchar(255) DEFAULT NULL,
  `new` varchar(255) DEFAULT NULL,
  `app_identity` int(11) DEFAULT NULL,
  `audit_message` text,
  `is_model_change` tinyint(4) DEFAULT '1' COMMENT '1 = change to an actual model. 0 = a high level change.',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_model` (`model`),
  KEY `idx_model_id` (`model_id`),
  KEY `idx_application_identity` (`app_identity`)
) ENGINE=InnoDB AUTO_INCREMENT=11037459 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `backfill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backfill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_spec_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `job_spec_id` (`job_spec_id`),
  KEY `provider_id` (`provider_id`),
  KEY `fk_backfill_campaign_id` (`campaign_id`),
  CONSTRAINT `backfill_ibfk_1` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`),
  CONSTRAINT `backfill_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_backfill_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=327680 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `credit_card_id` int(11) DEFAULT NULL,
  `description` text,
  `type` enum('credit card','invoice') NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `fk_contact_id` (`contact_id`),
  KEY `fk_address_id` (`address_id`),
  KEY `fk_credit_card_id` (`credit_card_id`),
  KEY `fk_billing_group_client_id` (`client_id`),
  CONSTRAINT `fk_address_id` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `fk_billing_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`),
  CONSTRAINT `fk_credit_card_id` FOREIGN KEY (`credit_card_id`) REFERENCES `credit_card` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112958 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_group_contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_group_contact_media` (
  `billing_group_id` int(11) NOT NULL,
  `contact_media_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `fk_billing_group_id` (`billing_group_id`),
  KEY `fk_contact_media_id` (`contact_media_id`),
  CONSTRAINT `fk_biling_group_id` FOREIGN KEY (`billing_group_id`) REFERENCES `billing_group` (`id`),
  CONSTRAINT `fk_contact_media_id` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bucket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bucket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `status` enum('inactive','active') NOT NULL DEFAULT 'active',
  `inclusive` tinyint(4) NOT NULL DEFAULT '0',
  `client_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bucket_client_id` (`client_id`),
  CONSTRAINT `bucket_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='Distribution channels between an affiliate and channel forms';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bucket_cap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bucket_cap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket_id` int(11) DEFAULT NULL,
  `affiliate_id` int(11) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `daily_lead_cap` int(11) NOT NULL DEFAULT '0',
  `daily_lead_count` int(11) NOT NULL DEFAULT '0',
  `weekly_lead_cap` int(11) NOT NULL DEFAULT '0',
  `weekly_lead_count` int(11) NOT NULL DEFAULT '0',
  `monthly_lead_cap` int(11) NOT NULL DEFAULT '0',
  `monthly_lead_count` int(11) NOT NULL DEFAULT '0',
  `total_lead_cap` int(11) NOT NULL DEFAULT '0',
  `total_lead_count` int(11) NOT NULL DEFAULT '0',
  `total_spend_cap` decimal(7,2) NOT NULL DEFAULT '0.00',
  `total_spend` decimal(7,2) NOT NULL DEFAULT '0.00',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL DEFAULT NULL,
  `count_modified_date` timestamp NULL DEFAULT NULL COMMENT 'Last time counts were modified',
  PRIMARY KEY (`id`),
  KEY `bucket_cap_affiliate_id` (`affiliate_id`),
  KEY `bucket_cap_provider_id` (`provider_id`),
  KEY `bucket_cap_bucket_id` (`bucket_id`),
  CONSTRAINT `bucket_cap_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`),
  CONSTRAINT `bucket_cap_bucket_id` FOREIGN KEY (`bucket_id`) REFERENCES `bucket` (`id`),
  CONSTRAINT `bucket_cap_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Controls bucket caps for affiliates and providers';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `vertical_id` int(11) NOT NULL,
  `status` enum('inactive','pending','deleted','active') NOT NULL DEFAULT 'pending',
  `is_held` tinyint(4) NOT NULL DEFAULT '0',
  `provider_id` int(11) NOT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `lead_plan_id` int(11) DEFAULT NULL COMMENT 'Indicates campaign''s current lead plan. No lead plan means a la carte.',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `campaign_key` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_campaign_provider_id` (`provider_id`),
  KEY `fk_campaign_vertical_id` (`vertical_id`),
  KEY `fk_campaign_contact_id` (`contact_id`),
  KEY `fk_campaign_lead_plan_id` (`lead_plan_id`),
  CONSTRAINT `fk_campaign_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`),
  CONSTRAINT `fk_campaign_lead_plan_id` FOREIGN KEY (`lead_plan_id`) REFERENCES `lead_plan` (`id`),
  CONSTRAINT `fk_campaign_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_campaign_vertical_id` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134893 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `campaign_contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_contact_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_media_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `contact_media_contact_media_id_fk` (`contact_media_id`),
  KEY `campaign_campaign_id_fk` (`campaign_id`),
  CONSTRAINT `campaign_campaign_id_fk` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `contact_media_contact_media_id_fk` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13258 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `campaign_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `price_type` enum('%','$') DEFAULT NULL,
  `preference` decimal(10,2) DEFAULT '0.00',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `campaign_profile_campaign_id` (`campaign_id`),
  KEY `campaign_profile_option_id` (`option_id`),
  KEY `campaign_profile_question_id` (`question_id`),
  CONSTRAINT `campaign_profile_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  CONSTRAINT `campaign_profile_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`),
  CONSTRAINT `campaign_profile_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126087 DEFAULT CHARSET=latin1 COMMENT='Stores campaign lead prices & profile information';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `daily_lead_cap` int(11) NOT NULL DEFAULT '0',
  `daily_lead_count` int(11) NOT NULL DEFAULT '0',
  `provider_id` int(11) NOT NULL,
  `weekly_lead_cap` int(11) NOT NULL DEFAULT '0',
  `weekly_lead_count` int(11) NOT NULL DEFAULT '0',
  `monthly_lead_cap` int(11) NOT NULL DEFAULT '0',
  `monthly_lead_count` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL DEFAULT NULL,
  `count_modified_date` timestamp NULL DEFAULT NULL COMMENT 'Last time counts were modified',
  `total_lead_cap` int(11) NOT NULL DEFAULT '0',
  `total_lead_count` int(11) NOT NULL DEFAULT '0',
  `total_spend_cap` decimal(7,2) DEFAULT NULL,
  `free_lead_cap` int(11) NOT NULL DEFAULT '0',
  `free_lead_count` int(11) NOT NULL DEFAULT '0',
  `campaign_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_cap1` (`provider_id`,`campaign_id`),
  KEY `fk_provider_cap_provider` (`provider_id`),
  KEY `fk_cap_campaign_id` (`campaign_id`),
  CONSTRAINT `cap_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cap_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17358 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `carrier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carrier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address_domain` varchar(100) NOT NULL,
  `weight` smallint(11) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='List of mobile carriers';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `channel_group_id` int(11) DEFAULT NULL,
  `message_template_group_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `note` text COMMENT 'An internal note against the model',
  `status` enum('active','inactive','staging','template') NOT NULL DEFAULT 'staging',
  `parent_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `template_channel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`),
  KEY `fk_channel_group_id` (`channel_group_id`),
  KEY `fk_channel_message_template_group_id` (`message_template_group_id`),
  KEY `idx_created_date` (`created_date`),
  KEY `fk_channel_client_id` (`client_id`),
  KEY `idx_template_channel_id` (`template_channel_id`),
  CONSTRAINT `fk_channel_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_channel_group_id` FOREIGN KEY (`channel_group_id`) REFERENCES `channel_group` (`id`),
  CONSTRAINT `fk_channel_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9150 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_bucket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_bucket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_channel_bucket` (`bucket_id`,`channel_id`),
  KEY `channel_bucket_bucket_id` (`bucket_id`),
  KEY `channel_bucket_channel_id` (`channel_id`),
  CONSTRAINT `channel_bucket_bucket_id` FOREIGN KEY (`bucket_id`) REFERENCES `bucket` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `channel_bucket_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1024 DEFAULT CHARSET=latin1 COMMENT='Maps channels to buckets';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `params` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_channel_id_filter_id` (`filter_id`,`channel_id`),
  KEY `fk_filter_id` (`filter_id`),
  KEY `fk_channel_id` (`channel_id`),
  CONSTRAINT `fk_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_filter_id` FOREIGN KEY (`filter_id`) REFERENCES `registration_filter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8146 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `message_template_group_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`code`),
  KEY `fk_channel_group_message_template_group_id` (`message_template_group_id`),
  KEY `fk_channel_group_client_id` (`client_id`),
  CONSTRAINT `fk_channel_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_channel_group_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_channel_option_channel_id` (`channel_id`),
  KEY `fk_channel_option_option_id` (`option_id`),
  CONSTRAINT `fk_channel_option_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`),
  CONSTRAINT `fk_channel_option_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58086 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `description` text,
  `page` int(11) NOT NULL DEFAULT '1',
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `default_value` varchar(50) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_channel_question_channel` (`channel_id`),
  KEY `fk_channel_question_question` (`question_id`),
  CONSTRAINT `fk_channel_question_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_channel_question_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=77485 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_vertical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_vertical` (
  `vertical_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`vertical_id`,`channel_id`),
  KEY `fk_channel_vertical_vertical` (`vertical_id`),
  KEY `fk_channel_vertical_channel` (`channel_id`),
  CONSTRAINT `fk_channel_vertical_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_channel_vertical_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channel_widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `layout` varchar(50) NOT NULL,
  `default_question_id` int(11) DEFAULT NULL,
  `type` enum('lightbox','inline') NOT NULL DEFAULT 'inline' COMMENT 'Widget invocation type',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `channel_widget_channel_id` (`channel_id`),
  KEY `channel_widget_default_question_id` (`default_question_id`),
  CONSTRAINT `channel_widget_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `channel_widget_default_question_id` FOREIGN KEY (`default_question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `message_template_group_id` int(11) DEFAULT NULL,
  `country` varchar(100) NOT NULL COMMENT 'Country of client and its children',
  `currency_code` char(3) NOT NULL COMMENT 'Currency used by the client and its children',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_client_message_template_group_id` (`message_template_group_id`),
  CONSTRAINT `fk_client_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_key` varchar(45) NOT NULL,
  `config_value` varchar(255) NOT NULL,
  `model_name` varchar(45) NOT NULL,
  `model_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`config_key`,`model_name`,`model_id`),
  UNIQUE KEY `pk_id` (`id`),
  KEY `idx_config_key` (`config_key`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1 COMMENT='This table stores configuration values for various models';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `consumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) DEFAULT NULL,
  `comms_opt_out` tinyint(4) DEFAULT '0' COMMENT '1 = Consumer should not be sent any emails',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `consumer_key` varchar(32) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_consumer_key` (`consumer_key`),
  KEY `idx_email` (`email`),
  KEY `fk_consumer_client_id` (`client_id`),
  CONSTRAINT `fk_consumer_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=303582 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `consumer_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `consumer_id` int(11) NOT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `provider_review_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_cf_project_id` (`project_id`),
  KEY `fk_cf_consumer_id` (`consumer_id`),
  KEY `fk_cf_provider_id` (`provider_id`),
  KEY `fk_cf_provider_review_id` (`provider_review_id`),
  CONSTRAINT `fk_cf_consumer_id` FOREIGN KEY (`consumer_id`) REFERENCES `consumer` (`id`),
  CONSTRAINT `fk_cf_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `fk_cf_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_cf_provider_review_id` FOREIGN KEY (`provider_review_id`) REFERENCES `provider_review` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33423 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `consumer_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_key` varchar(32) NOT NULL,
  `status` enum('pending','inactive','active','unprocessed') NOT NULL DEFAULT 'unprocessed',
  `channel_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activation_date` timestamp NULL DEFAULT NULL COMMENT 'Date consumer request went active',
  `modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Assumed to be date final state was reached',
  `client_id` int(11) NOT NULL,
  `affiliate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_request_key` (`request_key`),
  KEY `idx_status` (`status`),
  KEY `fk_consumer_request_channel_id` (`channel_id`),
  KEY `idx_created_date` (`created_date`),
  KEY `fk_consumer_request_client_id` (`client_id`),
  KEY `idx_modified_date` (`modified_date`),
  KEY `fk_consumer_request_affiliate_id` (`affiliate_id`),
  KEY `idx_activation_date` (`activation_date`),
  CONSTRAINT `fk_consumer_request_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`),
  CONSTRAINT `fk_consumer_request_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`),
  CONSTRAINT `fk_consumer_request_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=336121 DEFAULT CHARSET=latin1 COMMENT='Once a consumer request reaches a final state of either acti';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `consumer_request_bucket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_request_bucket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_request_id` int(11) NOT NULL,
  `bucket_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_crb_consumer_request_id` (`consumer_request_id`),
  KEY `fk_crb_bucket_id` (`bucket_id`),
  CONSTRAINT `fk_crb_bucket_id` FOREIGN KEY (`bucket_id`) REFERENCES `bucket` (`id`),
  CONSTRAINT `fk_crb_consumer_request_id` FOREIGN KEY (`consumer_request_id`) REFERENCES `consumer_request` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Which buckets were applicable for a consumer request';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `consumer_request_filter_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_request_filter_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_request_id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `registration_filter_id` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_consumer_request_id` (`consumer_request_id`),
  KEY `fk_registration_filter_id` (`registration_filter_id`),
  CONSTRAINT `fk_consumer_request_id` FOREIGN KEY (`consumer_request_id`) REFERENCES `consumer_request` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registration_filter_id` FOREIGN KEY (`registration_filter_id`) REFERENCES `registration_filter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36775 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `type` enum('primary','billing','lead') NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_first_name` (`first_name`),
  KEY `idx_last_name` (`last_name`)
) ENGINE=InnoDB AUTO_INCREMENT=82847 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(50) NOT NULL,
  `is_displayable` tinyint(4) NOT NULL DEFAULT '1',
  `type` enum('mobile','fax','phone') NOT NULL,
  `carrier_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `purpose` enum('billing','primary','display','lead') NOT NULL DEFAULT 'primary',
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `fk_contact_media_carrier_id` (`carrier_id`),
  CONSTRAINT `fk_contact_media_carrier_id` FOREIGN KEY (`carrier_id`) REFERENCES `carrier` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=735866 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `credit_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('visa','mastercard','amex','diners') NOT NULL,
  `number` varchar(300) NOT NULL,
  `name` text NOT NULL,
  `expiry_month` smallint(2) NOT NULL,
  `expiry_year` smallint(4) NOT NULL,
  `ccv` varchar(5) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4260 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_identifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_identifier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(45) NOT NULL,
  `model_id` int(11) NOT NULL,
  `external_id` varchar(100) NOT NULL,
  `external_system` varchar(20) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65327 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `authority` decimal(5,2) NOT NULL DEFAULT '0.00',
  `source` varchar(20) NOT NULL,
  `teaser` varchar(255) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `provider_review_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_provider_id` (`provider_id`) USING BTREE,
  KEY `fk_pr_provider_preview_id` (`provider_review_id`),
  CONSTRAINT `fk_er_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_pr_provider_preview_id` FOREIGN KEY (`provider_review_id`) REFERENCES `provider_review` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Review collated about providers from external sources';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `feedback_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `option_code` varchar(10) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `consumer_feedback_id` int(11) DEFAULT NULL,
  `text_value` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_question_id` (`question_id`),
  KEY `fk_fa_option_id` (`option_id`),
  KEY `fk_fa_consumer_feedback_id` (`consumer_feedback_id`),
  CONSTRAINT `fk_fa_consumer_feedback_id` FOREIGN KEY (`consumer_feedback_id`) REFERENCES `consumer_feedback` (`id`),
  CONSTRAINT `fk_fa_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`),
  CONSTRAINT `fk_fa_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=185714 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `heartbeat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `heartbeat` (
  `id` int(11) NOT NULL,
  `ts` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Used for replication monitoring';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `job_spec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(50) DEFAULT NULL,
  `vertical_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `is_preview` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Flags whether the job spec was created in a preview function.',
  `is_exclusive_match` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Flags whether this is an exclusive match job spec',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `consumer_request_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_job_spec_vertical` (`vertical_id`),
  KEY `fk_consumer_request_id` (`consumer_request_id`),
  KEY `pk_id` (`id`),
  KEY `fk_job_spec_client_id` (`client_id`),
  CONSTRAINT `fk_consumer_request` FOREIGN KEY (`consumer_request_id`) REFERENCES `consumer_request` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_spec_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_job_spec_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=336682 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `job_spec_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_spec_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `job_spec_id` int(11) NOT NULL,
  `text_value` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_job_spec_detail_job_spec` (`job_spec_id`),
  KEY `idx_question_id` (`question_id`),
  KEY `idx_option_id` (`option_id`),
  CONSTRAINT `fk_job_spec_detail_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_spec_detail_option_id` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_spec_detail_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5920930 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `json_publisher_identity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `json_publisher_identity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_identity_id` int(11) NOT NULL,
  `publisher_id` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `publisher_id` (`publisher_id`) USING BTREE,
  KEY `fk_application_identity_id` (`application_identity_id`),
  CONSTRAINT `fk_application_identity_id` FOREIGN KEY (`application_identity_id`) REFERENCES `application_identity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `json_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `json_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(100) DEFAULT NULL,
  `type` enum('question','auth','referrer','form-view','conversion') NOT NULL,
  `data_key` varchar(50) DEFAULT NULL,
  `data` text,
  `ip_address` varchar(16) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_sessionid_key` (`session_id`,`data_key`),
  KEY `fk_json_session_channel_id` (`channel_id`),
  CONSTRAINT `fk_json_session_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1916107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `lead_credit_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead_credit_request` (
  `match_set_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('Declined','Approved','Pending') DEFAULT 'Pending',
  `comment` varchar(255) DEFAULT NULL,
  `description` text,
  `internal_description` text,
  `note` text COMMENT 'An internal note against the model',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Assumed to be date final state was reached',
  PRIMARY KEY (`match_set_id`),
  KEY `fk_match_set_id` (`match_set_id`),
  KEY `idx_status` (`status`),
  CONSTRAINT `fk_match_set_id` FOREIGN KEY (`match_set_id`) REFERENCES `match_set` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Once a credit request reaches a final state of either Approv';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `lead_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lead_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `activation_date` timestamp NULL DEFAULT NULL,
  `expiry_date` timestamp NULL DEFAULT NULL,
  `status` enum('pending','expired','cancelled','active') NOT NULL DEFAULT 'pending',
  `auto_renew` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Holds lead plan subscriptions';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(100) DEFAULT '',
  `alt_label` varchar(100) DEFAULT NULL COMMENT 'An alternative name for the location',
  `postcode` varchar(6) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `type` enum('postcode','area') NOT NULL DEFAULT 'area',
  `level` enum('suburb','postcode','region','metro','regional','state','country') DEFAULT NULL,
  `left_id` int(11) DEFAULT NULL,
  `right_id` int(11) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `points_encoded` blob,
  `levels_encoded` blob,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_name` (`name`),
  KEY `idx_postocde` (`postcode`),
  KEY `idx_loc_left_id` (`left_id`),
  KEY `idx_loc_level` (`level`),
  KEY `idx_loc_right_id` (`right_id`),
  KEY `idx_loc_parent_id` (`parent_id`),
  KEY `idx_label` (`label`),
  KEY `idx_alt_label` (`alt_label`),
  KEY `idx_type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=14314 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `match_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `status` enum('inactive','active') NOT NULL DEFAULT 'active',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `match_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `default_type` enum('provider','backfill','prospect','affiliate') NOT NULL DEFAULT 'provider' COMMENT 'Defines the default for which type of provider to filter on',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `match_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `job_spec_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `lead_plan_id` int(11) DEFAULT NULL,
  `type` enum('provider','prospect','affiliate') NOT NULL DEFAULT 'provider',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_matched` tinyint(4) NOT NULL DEFAULT '0',
  `ranking_final_score` decimal(5,2) DEFAULT '0.00',
  `is_prospect` tinyint(4) NOT NULL DEFAULT '0',
  `lead_delay` timestamp NULL DEFAULT NULL COMMENT 'Indicates until when the lead should be not seen by the provider',
  `is_free` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_match_set_campaign_id` (`campaign_id`),
  KEY `fk_match_set_lead_plan_id` (`lead_plan_id`),
  KEY `fk_match_set_job_spec_id` (`job_spec_id`),
  KEY `fk_match_set_provider_id` (`provider_id`),
  KEY `idx_created_date` (`created_date`),
  KEY `idx_lead_delay` (`lead_delay`),
  KEY `idx_type` (`type`),
  KEY `idx_is_free` (`is_free`),
  CONSTRAINT `fk2_match_set_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  CONSTRAINT `fk2_match_set_job_spec_id` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`),
  CONSTRAINT `fk2_match_set_lead_plan_id` FOREIGN KEY (`lead_plan_id`) REFERENCES `lead_plan` (`id`),
  CONSTRAINT `fk2_match_set_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=720182 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `message_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_event_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `delivery_method` enum('api','sms','fax','email') DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_message_audit_message_event` (`message_event_id`),
  CONSTRAINT `fk_message_audit_message_event` FOREIGN KEY (`message_event_id`) REFERENCES `message_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1100420 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `message_delivery_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_delivery_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_message_event_id` int(11) NOT NULL,
  `delivery_method` enum('fax','sms','email','api') NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `campaign_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_provider_message_delivery_system_message_event` (`system_message_event_id`),
  KEY `fk_message_delivery_option_campaign_id` (`campaign_id`),
  CONSTRAINT `fk_message_delivery_option_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  CONSTRAINT `fk_provider_message_delivery_system_message_event` FOREIGN KEY (`system_message_event_id`) REFERENCES `system_message_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30555 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `message_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_message_event_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `key_id` int(11) DEFAULT NULL,
  `status` enum('failed','processed','unprocessed') DEFAULT 'unprocessed',
  `failure_reason` varchar(200) DEFAULT NULL,
  `trigger_date` timestamp NULL DEFAULT NULL COMMENT 'Future date for when message should be sent',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_event_system_message_event` (`system_message_event_id`),
  KEY `fk_parent_id` (`parent_id`),
  KEY `fk_key_id` (`key_id`),
  KEY `idx_status` (`status`),
  KEY `idx_created_date` (`created_date`),
  KEY `idx_trigger_date` (`trigger_date`),
  KEY `fk_message_event_client_id` (`client_id`),
  CONSTRAINT `fk_message_event_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_message_event_system_message_event` FOREIGN KEY (`system_message_event_id`) REFERENCES `system_message_event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `message_event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=845551 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `message_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_template_group_id` int(11) NOT NULL,
  `system_message_event_id` int(11) NOT NULL,
  `delivery_method` enum('email','fax','sms') NOT NULL DEFAULT 'email',
  `template` text,
  `from_email` varchar(255) DEFAULT NULL,
  `from_name` varchar(100) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_message_template` (`system_message_event_id`,`message_template_group_id`,`delivery_method`),
  KEY `fk_system_message_event_id` (`system_message_event_id`),
  KEY `fk_message_template_group_id` (`message_template_group_id`),
  KEY `idx_created_date` (`created_date`),
  KEY `fk_message_template_client_id` (`client_id`),
  CONSTRAINT `fk_message_template_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_message_template_group_id` FOREIGN KEY (`message_template_group_id`) REFERENCES `message_template_group` (`id`),
  CONSTRAINT `fk_system_message_event_id` FOREIGN KEY (`system_message_event_id`) REFERENCES `system_message_event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `message_template_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_template_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_template_group_client_id` (`client_id`),
  CONSTRAINT `fk_message_template_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Message template groups defined in the system';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `model` varchar(100) NOT NULL,
  `type` enum('force_pending_reason','force_pending','remote_ip','key1','key2','remote_referrer','exclusive_match') NOT NULL,
  `data` text NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `metadata_client_id` (`client_id`),
  KEY `metadata_model_id` (`model_id`),
  KEY `metadata_model` (`model`),
  KEY `metadata_type` (`type`),
  CONSTRAINT `metadata_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23818 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `onhold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `onhold` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `description` text,
  `active` tinyint(1) DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `campaign_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_provider_id` (`provider_id`),
  KEY `fk_onhold_campaign_id` (`campaign_id`),
  CONSTRAINT `fk_onhold_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7960 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `real_value` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `code` varchar(20) NOT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parent_id` int(11) DEFAULT NULL,
  `provider_description` text,
  `hint` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `default_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `default_price_type` enum('$','%') NOT NULL DEFAULT '$',
  `lead_delay` varchar(20) DEFAULT NULL COMMENT 'Used to delay leads. Can be duration in hours, days, time or date time.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`code`),
  KEY `fk_options_question` (`question_id`),
  KEY `fk_parent_id_option_id` (`parent_id`),
  CONSTRAINT `fk_options_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_id_option_id` FOREIGN KEY (`parent_id`) REFERENCES `option` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `option_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `option_tag` (
  `tag_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`tag_id`,`option_id`),
  KEY `fk_option_tag_tag` (`tag_id`),
  KEY `fk_option_tag_option` (`option_id`),
  CONSTRAINT `fk_option_tag_option` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_option_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `cost` decimal(10,2) NOT NULL,
  `lead_count` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `client_id` int(11) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '0',
  `vertical_id` int(11) DEFAULT NULL COMMENT 'No vertical defined then package applies to all verticals',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_package_client_id` (`client_id`),
  KEY `fk_package_vertical_id` (`vertical_id`),
  CONSTRAINT `fk_package_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_package_vertical_id` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains all packages in the system';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `profile_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `profile_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `new_job` tinyint(4) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vertical_id` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vertical_id` (`vertical_id`),
  KEY `idx_status` (`status`),
  KEY `idx_name` (`name`),
  KEY `idx_created_date` (`created_date`),
  KEY `fk_profile_group_client_id` (`client_id`),
  CONSTRAINT `fk_profile_group_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_vertical_id` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=594 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `profile_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_question_id` int(11) NOT NULL,
  `target_option_id` int(11) DEFAULT NULL,
  `min_value` int(11) DEFAULT NULL,
  `max_value` int(11) DEFAULT NULL,
  `dependency` int(11) DEFAULT NULL,
  `type` enum('equal','sum') NOT NULL DEFAULT 'equal',
  `profile_group_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_profile_rule_question` (`target_question_id`),
  KEY `fk_profile_rule_option` (`target_option_id`),
  CONSTRAINT `fk_profile_rule_option` FOREIGN KEY (`target_option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3765 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `profile_rule_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_rule_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_rule_id` int(11) DEFAULT NULL,
  `source_question_id` int(11) DEFAULT NULL,
  `source_option_id` int(11) DEFAULT NULL,
  `filter_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_profile_rule_attribute_profile_rule` (`profile_rule_id`),
  KEY `fk_profile_rule_attribute_profile_question` (`source_question_id`),
  KEY `fk_profile_rule_attribute_profile_option` (`source_option_id`),
  KEY `fk_profile_rule_attribute_profile_filter` (`filter_id`),
  CONSTRAINT `fk_profile_rule_attribute_profile_filter` FOREIGN KEY (`filter_id`) REFERENCES `profile_filter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_rule_attribute_profile_option` FOREIGN KEY (`source_option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_rule_attribute_profile_question` FOREIGN KEY (`source_question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `profile_rule_attribute_ibfk_1` FOREIGN KEY (`profile_rule_id`) REFERENCES `profile_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7180 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_spec_id` int(11) NOT NULL,
  `project_key` varchar(32) NOT NULL,
  `status` enum('Matched','Sub-matched','Pending','Unmatched') DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `address_id` int(11) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_project_job_spec` (`job_spec_id`),
  KEY `fk_project_address` (`address_id`),
  KEY `pk_id` (`id`),
  KEY `idx_project_key` (`project_key`),
  KEY `idx_consumer_name_composite` (`first_name`,`last_name`),
  KEY `idx_status` (`status`),
  KEY `fk_consumer_id` (`consumer_id`),
  KEY `idx_created_date` (`created_date`),
  KEY `fk_project_client_id` (`client_id`),
  CONSTRAINT `consumer_id` FOREIGN KEY (`consumer_id`) REFERENCES `consumer` (`id`),
  CONSTRAINT `fk_project_address` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=334833 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `project_contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_contact_media` (
  `project_id` int(11) NOT NULL,
  `contact_media_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`project_id`,`contact_media_id`),
  KEY `fk_project_contact_media_project` (`project_id`),
  KEY `fk_project_contact_media_contact_media` (`contact_media_id`),
  CONSTRAINT `project_contact_media_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `project_contact_media_ibfk_2` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `is_held` tinyint(4) NOT NULL DEFAULT '0',
  `status` enum('active','deleted','cancelled','pending','suspended','backfill','prospect') NOT NULL,
  `type` enum('provider','backfill','prospect','affiliate') NOT NULL DEFAULT 'provider',
  `ranking_adjustment` decimal(5,2) NOT NULL DEFAULT '1.00',
  `name_string` varchar(100) DEFAULT NULL,
  `address_string` varchar(100) DEFAULT NULL,
  `phone_string` varchar(50) DEFAULT NULL,
  `comms_opt_out` tinyint(4) DEFAULT '0' COMMENT '1 = The provider will not receive any communications from LEP (including lead alerts)',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `note` text COMMENT 'An internal note against the model',
  `modified_date` timestamp NULL DEFAULT NULL,
  `match_group_id` int(11) DEFAULT NULL,
  `billing_group_id` int(11) DEFAULT NULL,
  `provider_key` varchar(32) NOT NULL,
  `rating_score` decimal(5,2) DEFAULT '0.00',
  `affiliate_id` int(11) DEFAULT NULL COMMENT 'Indicates provider was created by an affiliate',
  `client_id` int(11) NOT NULL,
  `buyer_affiliate_id` int(11) DEFAULT NULL COMMENT 'Indicates affiliate who is a buyer of leads',
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`),
  KEY `fk_billing_group_id` (`billing_group_id`),
  KEY `fk_match_group` (`match_group_id`),
  KEY `idx_status` (`status`),
  KEY `fk_provider_affiliate_id` (`affiliate_id`),
  KEY `idx_created_date` (`created_date`),
  KEY `idx_modified_date` (`modified_date`),
  KEY `fk_provider_client_id` (`client_id`),
  KEY `fk_provider_buyer_affiliate_id` (`buyer_affiliate_id`),
  CONSTRAINT `fk_provider_buyer_affiliate_id` FOREIGN KEY (`buyer_affiliate_id`) REFERENCES `affiliate` (`id`),
  CONSTRAINT `fk_billing_group_id` FOREIGN KEY (`billing_group_id`) REFERENCES `billing_group` (`id`),
  CONSTRAINT `fk_match_group` FOREIGN KEY (`match_group_id`) REFERENCES `match_group` (`id`),
  CONSTRAINT `fk_provider_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`),
  CONSTRAINT `fk_provider_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123020 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_address` (
  `provider_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`provider_id`,`address_id`),
  KEY `fk_provider_address_provider` (`provider_id`),
  KEY `fk_provider_address_address` (`address_id`),
  CONSTRAINT `provider_address_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `provider_address_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `provider_id` (`provider_id`),
  CONSTRAINT `provider_attribute_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Holds client custom attributes of a provider';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_bucket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_bucket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_provider_bucket` (`bucket_id`,`provider_id`),
  KEY `provider_bucket_bucket_id` (`bucket_id`),
  KEY `provider_bucket_provider_id` (`provider_id`),
  CONSTRAINT `provider_bucket_bucket_id` FOREIGN KEY (`bucket_id`) REFERENCES `bucket` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `provider_bucket_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Maps providers to buckets';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_charge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `billing_group_id` int(11) DEFAULT NULL,
  `provider_id` int(11) NOT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `type` enum('lead','lead-credit','lead-plan','misc','misc-credit','subscription','subscription-credit','lead-estimate','lead-credit-estimate') NOT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `description` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `key_id` int(11) DEFAULT NULL,
  `key_type` enum('job-spec','lead-plan') DEFAULT NULL,
  `invoiced` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_billing_group_id` (`billing_group_id`),
  KEY `idx_provider_id` (`provider_id`),
  KEY `idx_key_id` (`key_id`),
  KEY `fk_provider_charge_campaign_id` (`campaign_id`),
  CONSTRAINT `fk_provider_charge_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=507688 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_contact` (
  `provider_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`provider_id`,`contact_id`),
  KEY `fk_providers_contacts_providers` (`provider_id`),
  KEY `fk_providers_contacts_contacts` (`contact_id`),
  CONSTRAINT `provider_contact_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `provider_contact_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_contact_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_contact_media` (
  `contact_media_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`provider_id`,`contact_media_id`),
  KEY `fk_provider_contact_contact_media` (`contact_media_id`),
  KEY `fk_provider_contact_media_provider` (`provider_id`),
  CONSTRAINT `provider_contact_media_ibfk_1` FOREIGN KEY (`contact_media_id`) REFERENCES `contact_media` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `provider_contact_media_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_lead_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_lead_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `type` enum('$','%') NOT NULL DEFAULT '$',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_provider_lead_price_provider` (`provider_id`),
  KEY `fk_provider_lead_price_option` (`option_id`),
  KEY `fk_provider_lead_price_question` (`question_id`),
  CONSTRAINT `fk_provider_lead_price_option` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provider_lead_price_provider` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provider_lead_price_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=59299 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_message_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_message_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `type` varchar(25) NOT NULL,
  `dispatcher` varchar(100) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_provider_id` (`provider_id`),
  KEY `provider_message_api_provider_id` (`provider_id`),
  CONSTRAINT `provider_message_api_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_message_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_message_params` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) DEFAULT NULL,
  `params` text,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_provider_id` (`provider_id`),
  CONSTRAINT `fkprovider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `preference` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_provider_question_provider` (`provider_id`),
  KEY `fk_provider_question_question` (`question_id`),
  KEY `fk_provider_question_option` (`option_id`),
  CONSTRAINT `fk_provider_question_option` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provider_question_provider` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provider_question_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=99219 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('external','internal') NOT NULL,
  `provider_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `score` decimal(5,2) NOT NULL DEFAULT '0.00',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `comment` text NOT NULL,
  `response` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_pr_provider_id` (`provider_id`),
  KEY `fk_pr_project_id` (`project_id`),
  CONSTRAINT `fk_pr_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `fk_pr_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4694 DEFAULT CHARSET=latin1 COMMENT='An aggregate table of internal and external provider reviews';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `subscription_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `auto_renew` tinyint(1) NOT NULL DEFAULT '1',
  `cancellation_date` timestamp NULL DEFAULT NULL,
  `billing_period` enum('Annually','Monthly') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_service_id` (`service_id`),
  KEY `fk_providerid` (`provider_id`),
  CONSTRAINT `fk_providerid` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_service_id` FOREIGN KEY (`service_id`) REFERENCES `subscription_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_subscription_charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_subscription_charge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_subscription_id` int(11) NOT NULL,
  `charge_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_provider_subscription_id` (`provider_subscription_id`),
  KEY `fk_charge_id` (`charge_id`),
  CONSTRAINT `fk_chargeid` FOREIGN KEY (`charge_id`) REFERENCES `provider_charge` (`id`),
  CONSTRAINT `fk_charge_id` FOREIGN KEY (`charge_id`) REFERENCES `provider_charge` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `provider_vertical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_vertical` (
  `provider_id` int(11) NOT NULL,
  `vertical_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`vertical_id`,`provider_id`),
  KEY `fk_providers_verticals_providers` (`provider_id`),
  KEY `fk_providers_verticals_verticals` (`vertical_id`),
  CONSTRAINT `fk_providers_verticals_verticals` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `provider_vertical_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `description` text,
  `type` enum('checkbox','date','display','location','location-specific','password','radio','rating','select','slider','text','textarea') NOT NULL,
  `vertical_id` int(11) DEFAULT NULL,
  `is_mandatory` tinyint(4) NOT NULL DEFAULT '0',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `code` varchar(20) NOT NULL,
  `internal_description` text,
  `provider_description` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `min_value` int(11) DEFAULT NULL,
  `min_label` varchar(100) DEFAULT NULL,
  `max_value` int(11) DEFAULT NULL,
  `max_label` varchar(100) DEFAULT NULL,
  `help` text,
  `help_url` varchar(255) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`code`),
  KEY `fk_question_vertical` (`vertical_id`),
  KEY `pk_id` (`id`),
  KEY `idx_label` (`label`),
  KEY `idx_status` (`status`),
  KEY `idx_type` (`type`),
  KEY `idx_created_date` (`created_date`),
  KEY `fk_question_client_id` (`client_id`),
  CONSTRAINT `fk_question_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_question_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5403 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `question_option_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_option_tree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_option_id` int(11) DEFAULT NULL,
  `child_question_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_mandatory` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_question_option_tree_option` (`parent_option_id`),
  KEY `fk_question_option_tree_question` (`child_question_id`),
  KEY `fk_question_option_tree_channel` (`channel_id`),
  CONSTRAINT `fk_question_option_tree_channel` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_option_tree_option` FOREIGN KEY (`parent_option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_option_tree_question` FOREIGN KEY (`child_question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=112518 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `question_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_tag` (
  `question_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `composite_key` (`tag_id`,`question_id`),
  KEY `fk_question_tag_question` (`question_id`),
  KEY `fk_question_tag_tag` (`tag_id`),
  CONSTRAINT `fk_question_tag_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('processing','failed','processed','unprocessed') NOT NULL DEFAULT 'unprocessed',
  `notification_email` varchar(250) DEFAULT NULL,
  `data` longblob NOT NULL,
  `type` enum('KmlImport','ProviderImport') NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `failure_reason` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_queue_client_id` (`client_id`),
  CONSTRAINT `fk_queue_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Generic queue bucket for LEP';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ranking_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ranking_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `parent_match_rule_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `registration_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `remarket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remarket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vertical_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `delay` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remarket_vertical_id` (`vertical_id`),
  KEY `fk_remarket_channel_id` (`channel_id`),
  KEY `fk_remarket_client_id` (`client_id`),
  CONSTRAINT `fk_remarket_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`),
  CONSTRAINT `fk_remarket_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `fk_remarket_vertical_id` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Defines remarketing options between a vertical and channel';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `service_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique_location` (`provider_id`,`campaign_id`,`question_id`,`postcode`),
  KEY `fk_service_area_provider_id` (`provider_id`),
  KEY `fk_service_area_question_id` (`question_id`),
  KEY `idx_postcode` (`postcode`),
  KEY `provider_id_question_id` (`provider_id`,`question_id`),
  KEY `fk_service_area_campaign_id` (`campaign_id`),
  CONSTRAINT `fk_service_area_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  CONSTRAINT `fk_service_area_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  CONSTRAINT `fk_service_area_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4681960 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `id` char(32) NOT NULL,
  `modified` int(11) NOT NULL,
  `lifetime` int(11) NOT NULL,
  `data` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `subscription_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(20) NOT NULL,
  `attributes` text,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `system_message_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_message_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `applicable_delivery_methods` varchar(20) DEFAULT NULL COMMENT 'Comma delimited list of delivery methods applicable for this system message event',
  `token_description` text COMMENT 'Describes tokens which are available to message templates derived from this system message event',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `is_system_tag` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `fk_tag_client_id` (`client_id`),
  CONSTRAINT `fk_tag_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `provider_id` int(11) NOT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `job_spec_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `percentage` decimal(10,2) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_question` (`question_id`),
  KEY `fk_transaction_option` (`option_id`),
  KEY `fk_transaction_provider` (`provider_id`),
  KEY `fk_transaction_job_spec` (`job_spec_id`),
  KEY `fk_transaction_campaign_id` (`campaign_id`),
  CONSTRAINT `fk_transaction_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  CONSTRAINT `fk_transaction_job_spec` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_option` FOREIGN KEY (`option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_provider` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=343065 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `unmatched_campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unmatched_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) NOT NULL,
  `job_spec_id` int(11) NOT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `reason` varchar(200) NOT NULL,
  `type` enum('provider','prospect','affiliate') NOT NULL DEFAULT 'provider',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_prospect` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_type` (`type`),
  KEY `fk_unmatched_campaign_campaign_id` (`campaign_id`),
  KEY `fk_unmatched_campaign_job_spec_id` (`job_spec_id`),
  KEY `fk_unmatched_campaign_provider_id` (`provider_id`),
  CONSTRAINT `fk2_unmatched_campaign_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`),
  CONSTRAINT `fk2_unmatched_campaign_job_spec_id` FOREIGN KEY (`job_spec_id`) REFERENCES `job_spec` (`id`),
  CONSTRAINT `fk2_unmatched_campaign_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51206795 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `real_name` varchar(255) DEFAULT '',
  `email` varchar(250) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('inactive','active') NOT NULL DEFAULT 'active',
  `affiliate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_affiliate_id` (`affiliate_id`),
  CONSTRAINT `fk_user_affiliate_id` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT '',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `v_capped`;
/*!50001 DROP VIEW IF EXISTS `v_capped`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_capped` (
  `provider_id` int(11),
  `campaign_id` int(11),
  `capped` int(1)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `vertical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `feedback_days` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of days after a project submission when a feedback request email will be sent out',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_slots` int(11) DEFAULT '3' COMMENT 'Total number of match slots applicable to vertical',
  `prospect_slots` int(11) DEFAULT '0' COMMENT 'Number of slots made available for prospects',
  `force_prospects` tinyint(4) DEFAULT '0' COMMENT 'Reserves slots in the final match for prospects regardless of coverage',
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_status` (`status`),
  KEY `idx_created_date` (`created_date`),
  KEY `fk_vertical_client_id` (`client_id`),
  CONSTRAINT `fk_vertical_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vertical_match_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertical_match_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `match_rule_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `vertical_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` enum('provider','backfill','prospect','affiliate') NOT NULL DEFAULT 'provider',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_vertical_match_rule1` (`vertical_id`,`match_rule_id`,`type`),
  KEY `fk_channel_match_rule_match_rule` (`match_rule_id`),
  KEY `fk_channel_match_rule_vertical` (`vertical_id`),
  CONSTRAINT `fk_channel_match_rule_match_rule` FOREIGN KEY (`match_rule_id`) REFERENCES `match_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_channel_match_rule_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3453 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vertical_question_option_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertical_question_option_tree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_option_id` int(11) NOT NULL,
  `child_question_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_vertical_question_option_tree_option` (`parent_option_id`),
  KEY `fk_vertical_question_option_tree_question` (`child_question_id`),
  CONSTRAINT `fk_vertical_question_option_tree_option` FOREIGN KEY (`parent_option_id`) REFERENCES `option` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vertical_question_option_tree_question` FOREIGN KEY (`child_question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vertical_ranking_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vertical_ranking_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ranking_rule_id` int(11) NOT NULL,
  `vertical_id` int(11) NOT NULL,
  `weight` decimal(6,2) NOT NULL COMMENT 'A weight from 1 to 100',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_vertical_ranking_attribute_ranking_attribute` (`ranking_rule_id`),
  KEY `fk_vertical_ranking_attribute_vertical` (`vertical_id`),
  CONSTRAINT `fk_vertical_ranking_attribute_ranking_rule` FOREIGN KEY (`ranking_rule_id`) REFERENCES `ranking_rule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vertical_ranking_attribute_vertical` FOREIGN KEY (`vertical_id`) REFERENCES `vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2429 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50001 DROP TABLE IF EXISTS `RT Campaign Cap`*/;
/*!50001 DROP VIEW IF EXISTS `RT Campaign Cap`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `RT Campaign Cap` AS select `v`.`provider_id` AS `pid`,`v`.`campaign_id` AS `cid`,`vv`.`name` AS `name`,if(((`c`.`monthly_lead_cap` > 0) and (`c`.`monthly_lead_count` = `c`.`monthly_lead_cap`)),_utf8'monthly',if(((`c`.`weekly_lead_cap` > 0) and (`c`.`weekly_lead_count` = `c`.`weekly_lead_cap`)),_utf8'weekly',if(((`c`.`daily_lead_cap` > 0) and (`c`.`daily_lead_count` = `c`.`daily_lead_cap`)),_utf8'daily',if(((`c`.`total_lead_cap` > 0) and (`c`.`total_lead_count` = `c`.`total_lead_cap`)),_utf8'total',_utf8'spend')))) AS `reason` from ((((`v_capped` `v` join `provider` `p` on(((`p`.`id` = `v`.`provider_id`) and (`p`.`status` = _latin1'active') and (`p`.`type` = _latin1'provider')))) join `campaign` `cc` on((`cc`.`id` = `v`.`campaign_id`))) join `vertical` `vv` on((`vv`.`id` = `cc`.`vertical_id`))) join `cap` `c` on(((`c`.`provider_id` = `v`.`provider_id`) and (`c`.`campaign_id` = `v`.`campaign_id`)))) where ((`v`.`capped` = 1) and (`v`.`campaign_id` is not null)) order by `v`.`provider_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `RT Provider Cap`*/;
/*!50001 DROP VIEW IF EXISTS `RT Provider Cap`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `RT Provider Cap` AS select `v`.`provider_id` AS `pid`,`v`.`campaign_id` AS `cid`,cast(group_concat(`vv`.`name` separator ',') as char charset utf8) AS `CAST(GROUP_CONCAT(vv.name) AS CHAR)`,if(((`c`.`monthly_lead_cap` > 0) and (`c`.`monthly_lead_count` = `c`.`monthly_lead_cap`)),_utf8'monthly',if(((`c`.`weekly_lead_cap` > 0) and (`c`.`weekly_lead_count` = `c`.`weekly_lead_cap`)),_utf8'weekly',if(((`c`.`daily_lead_cap` > 0) and (`c`.`daily_lead_count` = `c`.`daily_lead_cap`)),_utf8'daily',if(((`c`.`total_lead_cap` > 0) and (`c`.`total_lead_count` = `c`.`total_lead_cap`)),_utf8'total',_utf8'spend')))) AS `reason` from ((((`v_capped` `v` join `provider` `p` on(((`p`.`id` = `v`.`provider_id`) and (`p`.`status` = _latin1'active') and (`p`.`type` = _latin1'provider')))) join `provider_vertical` `pv` on((`pv`.`provider_id` = `p`.`id`))) join `vertical` `vv` on((`vv`.`id` = `pv`.`vertical_id`))) join `cap` `c` on(((`c`.`provider_id` = `v`.`provider_id`) and isnull(`c`.`campaign_id`)))) where ((`v`.`capped` = 1) and isnull(`v`.`campaign_id`)) group by `p`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `v_capped`*/;
/*!50001 DROP VIEW IF EXISTS `v_capped`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`%`@`%` SQL SECURITY INVOKER */
/*!50001 VIEW `v_capped` AS select `cap`.`provider_id` AS `provider_id`,`cap`.`campaign_id` AS `campaign_id`,(((`cap`.`daily_lead_cap` > 0) and (`cap`.`daily_lead_count` >= `cap`.`daily_lead_cap`)) or ((`cap`.`weekly_lead_cap` > 0) and (`cap`.`weekly_lead_count` >= `cap`.`weekly_lead_cap`)) or ((`cap`.`monthly_lead_cap` > 0) and (`cap`.`monthly_lead_count` >= `cap`.`monthly_lead_cap`)) or ((`cap`.`total_lead_cap` > 0) and (`cap`.`total_lead_count` >= `cap`.`total_lead_cap`))) AS `capped` from `cap` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `acl_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `acl_resource` WRITE;
/*!40000 ALTER TABLE `acl_resource` DISABLE KEYS */;
INSERT INTO `acl_resource` VALUES (1,'GetQuestionsRQ','0000-00-00 00:00:00'),(2,'RegisterConsumerRequestRQ','0000-00-00 00:00:00'),(3,'GetConsumerRQ','0000-00-00 00:00:00'),(4,'GetConsumerRequestRQ','0000-00-00 00:00:00'),(5,'GetProviderRQ','2008-12-22 01:16:05'),(6,'SaveProviderRQ','2008-12-22 21:42:29'),(7,'GetProvidersRQ','2008-12-22 23:04:14'),(8,'UpdateProviderStatusRQ','2008-12-30 02:59:14'),(9,'GetSystemQuestionsRQ','2009-01-05 00:55:38'),(10,'GetChannelRQ','2009-01-05 22:18:32'),(11,'SaveChannelRQ','2009-01-06 04:39:52'),(12,'PublishChannelChangesRQ','2009-01-08 01:00:12'),(13,'UpdateChannelStatusRQ','2009-01-08 02:32:00'),(14,'GetChannelsRQ','2009-01-08 05:25:07'),(15,'GetProviderOnHoldDatesRQ','2009-01-13 01:15:09'),(16,'DuplicateQuestionRQ','2009-01-13 05:21:25'),(17,'DuplicateChannelRQ','2009-01-13 05:22:09'),(18,'PreviewConsumerRequestRQ','2009-01-14 04:04:57'),(19,'GetVerticalRQ','2009-01-15 06:14:01'),(20,'GetVerticalsRQ','2009-01-15 06:14:06'),(21,'SaveVerticalRQ','2009-01-15 22:18:04'),(22,'UpdateVerticalStatusRQ','2009-01-16 06:05:28'),(23,'GetSystemRankingRulesRQ','2009-01-19 00:36:24'),(24,'GetSystemMatchRulesRQ','2009-01-19 00:36:34'),(25,'GetApplicationIdentitiesRQ','2009-01-19 00:36:44'),(26,'GetSystemQuestionTagsRQ','2009-02-26 00:34:00'),(27,'GetLeadsRQ','2009-02-26 00:34:01'),(29,'GetFeedbackListRQ','2009-02-26 00:34:01'),(30,'GetQuestionOptionRQ','2009-04-14 00:25:57'),(31,'GetVolumeForecastRQ','2009-04-14 00:25:57'),(32,'SaveCreditRequestRQ','2009-03-04 22:02:42'),(33,'GetLocationListRQ','2009-04-14 00:25:58'),(34,'SaveConsumerFeedbackRQ','2009-04-14 00:25:57'),(35,'GetConsumerFeedbackRQ','2009-04-14 00:25:57'),(36,'GetConsumerProjectsRQ','2009-04-14 00:25:57'),(37,'GetConsumerByKeyRQ','2009-04-01 23:29:23'),(38,'GenerateMessageEventRQ','2009-04-01 23:29:34'),(39,'GetSystemQuestionTypesRQ','2009-04-03 02:58:32'),(40,'GetSystemTagsRQ','2009-04-03 03:49:57'),(41,'SaveQuestionOptionRQ','2009-04-06 01:02:15'),(42,'SaveQuestionRQ','2009-04-06 01:02:15'),(43,'DeleteQuestionOptionRQ','2009-04-06 02:17:50'),(44,'AssignTagsRQ','2009-04-06 02:17:58'),(45,'UnAssignTagsRQ','2009-04-06 04:36:12'),(46,'UpdateQuestionStatusRQ','2009-04-07 00:43:39'),(47,'GetProfileGroupsRQ','2009-04-08 00:10:51'),(48,'GetProviderSystemMessagesRQ','2009-04-14 00:26:04'),(49,'SaveProfileRuleRQ','2009-04-08 06:32:37'),(50,'DeleteProfileRuleRQ','2009-04-08 07:02:17'),(51,'DeleteProfileRuleSourceRQ','2009-04-09 02:29:07'),(52,'SaveProfileRuleSourceRQ','2009-04-09 02:41:23'),(53,'SaveProfileGroupRQ','2009-04-09 04:41:13'),(54,'ValidatePostcodeRQ','2009-04-21 00:35:50'),(55,'GetFeedbackQuestionsRQ','2009-05-06 01:27:08'),(56,'GetProviderChargesRQ','2009-05-06 01:27:09'),(57,'GetProjectsRQ','2009-05-06 01:27:09'),(58,'GetProjectRQ','2009-05-06 01:27:09'),(59,'AddProviderChargeRQ','2009-05-06 01:27:09'),(60,'GetConsumerRequestsRQ','2009-05-06 01:27:09'),(61,'UpdateConsumerRequestStatusRQ','2009-05-06 01:27:09'),(62,'GetCreditRequestsRQ','2009-05-06 01:27:09'),(63,'GetRegistrationFiltersRQ','2009-05-06 01:27:09'),(64,'AssignChannelFiltersRQ','2009-05-06 01:27:09'),(65,'GetMatchGroupsRQ','2009-05-21 22:54:47'),(66,'SaveMatchGroupRQ','2009-05-21 22:54:47'),(67,'GetMatchGroupRQ','2009-05-21 22:54:47'),(68,'VerifyProviderLeadStatusRQ','2009-05-21 22:54:47'),(69,'GetBillingGroupsRQ','2009-05-21 22:54:47'),(70,'GetBillingGroupChargesRQ','2009-05-21 22:54:47'),(71,'GetBillingGroupRQ','2009-05-21 22:54:47'),(72,'SaveBillingGroupRQ','2009-05-21 22:54:47'),(73,'SaveDefaultPriceRQ','2009-05-21 22:54:47'),(74,'GetProviderChangeHistoryRQ','2009-06-18 07:06:14'),(75,'GetDirectoryLocationsRQ','2009-07-08 07:38:17'),(76,'GetChannelGroupsRQ','2009-07-08 07:38:17'),(77,'UpdateExternalIdentifierRQ','2009-07-16 03:25:43'),(78,'SaveMessageEventRQ','2009-07-31 01:43:28'),(79,'GetProviderServiceAreasRQ','2009-08-28 03:04:56'),(80,'UpdateConsumerInputRQ','2009-10-06 04:18:27'),(81,'GetRegistrationChannelRQ','2009-10-06 04:18:27'),(82,'SaveHoldDatesRQ','2009-10-19 06:04:19'),(83,'DeleteHoldDatesRQ','2009-10-19 06:04:19'),(84,'GetMessageEventsRQ','2010-02-17 20:39:56'),(85,'GetSystemMessageEventsRQ','2010-02-17 20:39:56'),(86,'SaveConfigRQ','2010-03-09 20:56:20'),(87,'GetConfigRQ','2010-03-09 20:56:20'),(88,'GetProviderReviewsRQ','2010-03-09 20:56:20'),(89,'UpdateProviderReviewRQ','2010-03-09 20:56:21'),(90,'SaveExternalReviewRQ','2010-03-09 20:56:21'),(91,'GetExternalReviewRQ','2010-03-09 20:56:21'),(92,'GetFeedbackRQ','2010-03-09 20:56:22'),(93,'GetExternalReviewsRQ','2010-03-09 20:56:22'),(94,'SaveClientConfigRQ','2010-03-09 20:56:22'),(95,'GetClientConfigRQ','2010-03-09 20:56:22'),(97,'GetNonceRQ','2010-03-30 20:29:13'),(98,'GetProfileGroupRQ','2010-03-30 20:29:13'),(99,'GetMessageTemplateGroupsRQ','2010-05-27 22:00:38'),(100,'SaveQueueRQ','2010-05-27 22:00:38'),(101,'GetQueueListRQ','2010-05-27 22:00:38'),(102,'ExportLocationsRQ','2010-05-27 22:00:38'),(103,'GetMessageTemplateRQ','2010-06-22 21:31:59'),(104,'GetMessageTemplatesRQ','2010-06-22 21:31:59'),(105,'SaveMessageTemplateRQ','2010-06-22 21:31:59'),(106,'SaveMessageTemplateGroupRQ','2010-06-22 21:31:59'),(107,'SaveAttributesRQ','2010-06-22 21:31:59'),(108,'CommsOptOutRQ','2010-09-21 21:30:40'),(109,'GetRemarketingOptionsRQ','2010-09-21 21:30:40'),(110,'DeleteRemarketingOptionRQ','2010-09-21 21:30:40'),(111,'SaveRemarketingOptionRQ','2010-09-21 21:30:40'),(112,'DeleteCapRQ','2011-05-18 17:30:48'),(113,'SaveCapRQ','2011-05-18 17:30:48'),(114,'GetCapsRQ','2011-05-18 17:30:48'),(115,'GetHoldDatesRQ','2011-05-18 17:30:48'),(116,'SaveServiceAreaRQ','2011-05-18 17:30:48'),(117,'GetServiceAreasRQ','2011-05-18 17:30:48'),(118,'GetCampaignProfilesRQ','2011-05-18 17:30:48'),(119,'SaveCampaignProfileRQ','2011-05-18 17:30:48'),(120,'DeleteCampaignProfileRQ','2011-05-18 17:30:48'),(121,'GetCampaignRQ','2011-05-18 17:30:48'),(122,'SaveCampaignRQ','2011-05-18 17:30:48'),(123,'GetCampaignsRQ','2011-05-18 17:30:48'),(124,'SaveCampaignContactMediaRQ','2011-05-18 17:30:48'),(125,'SaveMessageDeliveryOptionsRQ','2011-05-18 17:30:48'),(126,'SaveContactRQ','2011-05-18 17:30:48'),(127,'GetContactRQ','2011-05-18 17:30:48'),(128,'GetContactsRQ','2011-05-18 17:30:48'),(129,'SaveCampaignProfilesRQ','2011-05-18 17:30:48'),(130,'DuplicateBillingGroupRQ','2011-05-18 17:30:48'),(131,'VerifyCampaignLeadStatusRQ','2011-05-18 17:30:48'),(132,'DuplicateCampaignRQ','2011-05-18 17:30:48'),(133,'SaveProviderAttributeRQ','2011-05-18 17:30:48'),(134,'SaveChannelWidgetRQ','2011-05-18 17:30:48'),(135,'GetChannelWidgetRQ','2011-05-18 17:30:48'),(136,'SaveLeadPlanRQ','2011-05-18 17:30:48'),(137,'GetPackagesRQ','2011-05-18 17:30:48'),(138,'GetLeadPlansRQ','2011-05-18 17:30:48'),(139,'CancelLeadPlanRQ','2011-05-18 17:30:48'),(140,'ActivateLeadPlanRQ','2011-05-18 17:30:48'),(141,'GetCarriersRQ','2011-05-18 17:30:48'),(142,'GetProviderAttributesRQ','2011-05-18 17:59:17'),(143,'PropagateQuestionRQ','2011-05-18 17:59:17'),(144,'SaveApplicationIdentityRQ','2011-05-18 17:59:17'),(145,'GetAclRolesRQ','2011-05-18 17:59:17'),(146,'GetAclResourcesRQ','2011-05-18 17:59:17'),(147,'SaveAclRoleRQ','2011-05-18 17:59:17'),(148,'UpdateCampaignStatusRQ','2011-05-18 17:59:17'),(149,'SaveRankingAdjustmentRQ','2011-05-18 17:59:18'),(150,'GetCampaignChangeHistoryRQ','2011-05-29 23:26:47'),(151,'SaveBucketRQ','2011-06-09 15:32:31'),(152,'GetBucketRQ','2011-06-09 15:32:31'),(153,'GetBucketsRQ','2011-06-09 15:32:31'),(154,'AssignBucketModelRQ','2011-06-09 15:32:31'),(155,'RemoveBucketModelRQ','2011-06-09 15:32:31'),(156,'SaveBucketCapRQ','2011-06-09 15:32:31'),(157,'GetBucketCapsRQ','2011-06-09 15:32:31'),(158,'CopyServiceAreasRQ','2011-06-29 16:33:48'),(159,'AssignChannelQuestionsRQ','2011-06-29 16:33:48'),(160,'GetAffiliateRQ','2011-10-12 19:41:12'),(161,'GetAffiliatesRQ','2011-10-12 19:41:12'),(162,'SaveAffiliateRQ','2011-10-12 19:41:12');
/*!40000 ALTER TABLE `acl_resource` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `match_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `default_type` enum('provider','backfill','prospect','affiliate') NOT NULL DEFAULT 'provider' COMMENT 'Defines the default for which type of provider to filter on',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `match_rule` WRITE;
/*!40000 ALTER TABLE `match_rule` DISABLE KEYS */;
INSERT INTO `match_rule` VALUES (1,'Lep_Match_Cap','Filter out providers based on their daily, weekly and monthly lead caps','provider','0000-00-00 00:00:00'),(3,'Lep_Match_RemovalLocations','Filter out providers who do not service the specified pick up and drop off locations','provider','0000-00-00 00:00:00'),(5,'Lep_Match_MatchExact','Filter out providers who haven\'t got the same options that are indicated by the consumer for the tagged questions.','provider','0000-00-00 00:00:00'),(6,'Lep_Match_JobLocation','Filter out providers who don\'t service the specified job location. To be used for verticals which can have only one job location','provider','0000-00-00 00:00:00'),(8,'Lep_Match_MultiLocations','Filter out providers who do not service at least one of the specified location. Note that this rule is used in conjuction with the location-specific and location-multiple questions (e.g.Finance locations).','provider','2009-03-30 01:50:15'),(9,'Lep_Match_JobProximity','Filter out providers who do not service within 20KM of the specified job location. Use this in conjuction with Lep_Rank_JobProximity to rank providers based on proximity.','backfill','2009-06-18 07:06:15'),(10,'Lep_Match_RemovalProximity','Filter out providers who do not service within 20KM of the pickup or dropoff locations. Use this in conjuction with Lep_Rank_JobProximity to rank providers based on proximity.','backfill','2009-07-31 01:43:30'),(11,'Lep_Match_MatchPartial','Filter out providers who haven\'t got at least 1 of the options that are indicated by the consumer. Note this filter assumes that if a provider has not selected an option, then all options will apply.','provider','2009-09-18 07:56:24'),(12,'Lep_Match_BucketCap','Filter out providers based on their cap settings for a specific bucket','provider','2011-06-09 15:32:43'),(13,'Lep_Match_JobPreference','Filter out providers who haven\'t got the same options that are indicated by the consumer and at least one high preference.','provider','2011-10-12 19:42:33');
/*!40000 ALTER TABLE `match_rule` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `ranking_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ranking_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `parent_match_rule_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `ranking_rule` WRITE;
/*!40000 ALTER TABLE `ranking_rule` DISABLE KEYS */;
INSERT INTO `ranking_rule` VALUES (1,'Lep_Rank_TimeLastMatch',NULL,'Rank providers based on last time they were matched','0000-00-00 00:00:00'),(2,'Lep_Rank_EffectiveLeadPrice',NULL,'Effective Lead Price:Average price per lead (minus credits) for the match criteria of the project being ranked','0000-00-00 00:00:00'),(3,'Lep_Rank_CampaignPreference',NULL,'Ranked based on a provider\'s preference to accept certail jobs','2008-12-18 02:07:46'),(4,'Lep_Rank_ProviderSuitability',NULL,'Rank providers based on their suitability score. i.e. Average Rating for same job types','2008-12-18 03:36:52'),(5,'Lep_Rank_RatingQuality',NULL,'Rank providers based on their average Quality Rating score','2008-12-22 02:59:58'),(6,'Lep_Rank_CurrentLeadPrice',NULL,'Rank providers based on their current BASE lead price','2009-01-28 02:20:37'),(7,'Lep_Rank_ConsumerLocationPreference',8,'Rank providers based on whether to service the consumer\'s preferred location or supplementary locations','2009-03-30 02:50:08'),(8,'Lep_Rank_ProviderAverageRating',NULL,'Rank providers based on their average rating. Average rating is a product of both consumer feedback ratings and external review scores','2010-03-09 20:57:07'),(9,'Lep_Rank_JobProximity',NULL,'Rank providers on their proximity to a job location. NOTE: This rule must be used in conjunction with either Lep_Match_JobProximity or Lep_Match_RemovalProximity.','2010-09-21 21:29:49'),(10,'Lep_Rank_CapLevel',NULL,'Rank providers based on their current caps and lead counts. The more restrictive the cap, the lower they are ranked.','2010-11-22 07:09:11'),(11,'Lep_Rank_FreeLeads',NULL,'Rank providers based on whether they have free leads or not. This is a simple binary ranking algorithm.','2010-11-22 07:09:11');
/*!40000 ALTER TABLE `ranking_rule` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `registration_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `registration_filter` WRITE;
/*!40000 ALTER TABLE `registration_filter` DISABLE KEYS */;
INSERT INTO `registration_filter` VALUES (1,'Lep_RegistrationFilter_Email','Checks for a valid email address.','2009-05-01 00:38:27'),(2,'Lep_RegistrationFilter_Name','Checks for valid first and last names.','2009-05-01 00:38:50'),(3,'Lep_RegistrationFilter_Phone','Checks for valid phone numbers.','2009-05-01 00:59:25'),(4,'Lep_RegistrationFilter_BlackList','Checks a registration event for black listed words.','2009-05-01 01:52:14'),(5,'Lep_RegistrationFilter_Duplicate','Checks for duplicate registrations.','2009-09-25 03:15:19'),(6,'Lep_RegistrationFilter_All','Filter all registrations indiscriminately.','2010-08-05 01:32:47'),(7,'Lep_RegistrationFilter_ConsumerIpAddress','Checks for duplicate registrations using the same IP address','2011-01-14 08:27:41'),(8,'Lep_RegistrationFilter_ForceToPending','Forces a registration to pending depending on the ForcePending flag','2011-01-14 08:27:41');
/*!40000 ALTER TABLE `registration_filter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `system_message_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_message_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `applicable_delivery_methods` varchar(20) DEFAULT NULL COMMENT 'Comma delimited list of delivery methods applicable for this system message event',
  `token_description` text COMMENT 'Describes tokens which are available to message templates derived from this system message event',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `system_message_event` WRITE;
/*!40000 ALTER TABLE `system_message_event` DISABLE KEYS */;
INSERT INTO `system_message_event` VALUES (1,'LeadAlert','Lead Alert',NULL,'email,sms,fax','<b>$vertical</b>  (Vertical name)\r\n<b>$provider_name</b>\r\n<b>$provider_key</b> (Identifying key of provider)\r\n<b>$lead_date</b> (Date and time lead was generated)\r\n<b>$lead_delay_date</b> (Date and time lead was delayed (if any))\r\n<b>$email</b> (Email address that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$fax</b> (Fax that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$mobile</b> (Mobile number that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$job_description</b> (Descriptive text for the project. Available in both the message body and message subject.)\r\n<b>$product_description</b> (Main product option of project. Available in both the message body and message subject.)\r\n<b>$consumer_inputs</b> (An array where each item consists of $question_label $option_label and $text_value)\r\n<b>$consumer_name</b> (Is available in both the message body and message subject)\r\n<b>$consumer_address1</b>\r\n<b>$consumer_address2</b>\r\n<b>$consumer_postcode</b>\r\n<b>$consumer_city</b>\r\n<b>$consumer_email</b>\r\n<b>$consumer_phone</b>\r\n<b>$consumer_mobile</b>\r\n<b>$best_contact_time</b>\r\n<b>$message_subject</b>','2009-01-22 06:42:11'),(2,'MatchResults','Match Results','Generates an email to the consumer in response registration matching','email','<b>$email</b>  (Consumer\'s email address)\r\n<b>$consumer_name</b>  (Consumer\'s name)\r\n<b>$consumer_key</b>  (Consumer\'s unique key)\r\n<b>$project_data</b>  (Array of projects. See below for description of indexes.)\r\n\r\nTokens available to a project\r\n<b>$vertical</b> (Vertical assigned to project)\r\n<b>$status</b> (Status of project)\r\n<b>$providers</b> (Array or providers assigned to project)\r\n<b>$backfill</b> (Array or backfill providers assigned to project)\r\n\r\nTokens available to a provider\r\n<b>$name</b> (Provider\'s display name)\r\n<b>$address</b> (Provider\'s display address)\r\n<b>$rating</b> (Provider\'s rating score)\r\n<b>$id</b> (Provider Id)\r\n<b>$contact_name</b>\r\n<b>$email</b>\r\n<b>$phone</b>\r\n<b>$fax</b>\r\n\r\nTokens available to a backfill provider\r\n<b>$name</b>\r\n<b>$address</b>\r\n<b>$phone</b>','2009-01-22 06:42:18'),(4,'24HourFollowUp','24 Hour Follow Up','Sends a \"Have you been contacted?\" email to the consumer 24 hours after their project was submitted.','email','<b>$email</b>  (Consumer\'s email address)\r\n<b>$consumer_key</b> (Consumer identifying key)\r\n<b>$consumer_name</b> (Consumer\'s name)','2009-07-08 07:38:16'),(5,'RatingRequest','Ratings Request','Sends a Ratings Request email to the consumer.','email','<b>$consumer_email_key</b>  (Consumer\'s email address hash)\r\n<b>$consumer_key</b> (Consumer identifying key)\r\n<b>$consumer_name</b> (Consumer\'s name)','2009-07-08 07:38:16'),(6,'RequestARating','Request A Rating','A provider initiated Ratings Request email to the consumer.','email','<b>$email</b>  (Consumer\'s email address)\r\n<b>$consumer_key</b> (Consumer identifying key)\r\n<b>$consumer_name</b> (Consumer\'s name)\r\n<b>$provider_name</b> (Name of provider who requested the rating)\r\n<b>$description</b> (Project description)','2009-07-08 07:38:16'),(8,'LeadCredit','Lead Credited','Email sent to a provider when a lead is credited.','email','<b>$description</b>  (Credit description)\r\n<b>$provider_key</b> (Identifying key of provider)\r\n<b>$provider_contact</b> (Name of provider contact)\r\n<b>$submitted</b> (Date & time the credit was submitted)\r\n<b>$status</b> (Status of the credit)\r\n<b>$comment</b> (Lead credit comment)\r\n<b>$internal_description</b> (Credit\'s internal description)\r\n<b>$email</b> (Provider\'s email address)\r\n<b>$id</b> (Lead Id which is assigned the credit. Available in both the message body and message subject.)\r\n<b>$consumer_name</b> (Name of consumer who submitted the project. Available in both the message body and message subject.)','2009-07-08 07:38:16'),(9,'LeadAlertReminder','Lead Alert Reminder','A reminder sent to a provider reminding them of a lead.','email,sms,fax','<b>$vertical</b>  (Vertical name)\r\n<b>$provider_name</b>\r\n<b>$provider_key</b> (Identifying key of provider)\r\n<b>$lead_date</b> (Date and time lead was generated)\r\n<b>$lead_delay_date</b> (Date and time lead was delayed (if any))\r\n<b>$email</b> (Email address that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$fax</b> (Fax that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$mobile</b> (Mobile number that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$job_description</b> (Descriptive text for the project. Available in both the message body and message subject.)\r\n<b>$product_description</b> (Main product option of project. Available in both the message body and message subject.)\r\n<b>$consumer_inputs</b> (An array where each item consists of $question_label $option_label and $text_value)\r\n<b>$consumer_name</b> (Is available in both the message body and message subject)\r\n<b>$consumer_address1</b>\r\n<b>$consumer_address2</b>\r\n<b>$consumer_postcode</b>\r\n<b>$consumer_city</b>\r\n<b>$consumer_email</b>\r\n<b>$consumer_phone</b>\r\n<b>$consumer_mobile</b>\r\n<b>$best_contact_time</b>\r\n<b>$message_subject</b>','2009-07-08 07:38:16'),(10,'NewAccount','New Account','An email sent to the provider when an account is created outside of Drupal','email','<b>$contact_name</b>  (Provider contact)\r\n<b>$provider_name</b>\r\n<b>$provider_key</b> (Provider\'s unique key)\r\n<b>$provider_id</b> \r\n<b>$hash</b> (Unique hash for email)','2009-07-08 07:38:16'),(11,'LeadProspectAlert','Lead Prospect Alert','Lead alerts sent to prospect','email,sms,fax','<b>$vertical</b>  (Vertical name)\r\n<b>$provider_name</b>\r\n<b>$provider_key</b> (Identifying key of provider)\r\n<b>$lead_date</b> (Date and time lead was generated)\r\n<b>$lead_delay_date</b> (Date and time lead was delayed (if any))\r\n<b>$email</b> (Email address that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$fax</b> (Fax that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$mobile</b> (Mobile number that the lead alert will be sent to, if applicable for delivery method)\r\n<b>$job_description</b> (Descriptive text for the project. Available in both the message body and message subject.)\r\n<b>$product_description</b> (Main product option of project. Available in both the message body and message subject.)\r\n<b>$consumer_inputs</b> (An array where each item consists of $question_label $option_label and $text_value)\r\n<b>$consumer_name</b> (Is available in both the message body and message subject)\r\n<b>$consumer_address1</b>\r\n<b>$consumer_address2</b>\r\n<b>$consumer_postcode</b>\r\n<b>$consumer_city</b>\r\n<b>$consumer_email</b>\r\n<b>$consumer_phone</b>\r\n<b>$consumer_mobile</b>\r\n<b>$best_contact_time</b>\r\n<b>$message_subject</b>\r\n<b>$lead_count</b>  (Number of leads assigned to the prospect)\r\n\r\n<b>$next_provider_name</b>  \r\nName of another random prospect that could be matched to future projects. Note that\r\nthis may not\r\n','2010-09-21 21:29:48'),(12,'Remarket','Remarketing Email','Remarketing email sent to consumer n days after a registration','email','<b>$consumer_key</b>  (Consumer\'s Key)\r\n<b>$consumer_name</b> (Is available in both the message body and message subject)\r\n<b>$project_date</b> (Date the initial project was submitted)\r\n<b>$vertical_id</b> (Vertical Id of project)\r\n<b>$vertical_name</b> (Name of vertical attached to the project)\r\n<b>$channel_id</b> (Channel Id of project)\r\n<b>$channel_name</b> (Name of channel attached to the project)\r\n\r\n<b>$remarket_channels</b>  (Array of remarketed channels. See below for description of indexes.)\r\n<b>$id</b> (Id of channel that is being marketed to the consumer)\r\n<b>$name</b> (Name of channel that is being marketed to the consumer)','2010-09-21 21:30:42'),(13,'TestSms','Test SMS','Fires off a test SMS message to the specified mobile number','sms','<b>$mobile</b> (Mobile number we are testing against)\n<b>$carrier</b> (Mobile number carrier)','2011-05-18 17:59:17');
/*!40000 ALTER TABLE `system_message_event` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `is_system_tag` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_id` (`id`),
  KEY `fk_tag_client_id` (`client_id`),
  CONSTRAINT `fk_tag_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (1,'Pick Up Location','Identifies removals pick up location questions',1,'0000-00-00 00:00:00',1),(2,'Drop Off Location','Identifies removals drop-off location questions',1,'0000-00-00 00:00:00',1),(3,'Job Default','Identifies questions or options which are preassigned to newly created campaigns',1,'2011-06-09 15:32:43',NULL),(4,'Match Exact','Identifies questions used with Lep_Match_MatchExact match rules',1,'0000-00-00 00:00:00',1),(5,'Email','Identifies consumer email questions',1,'0000-00-00 00:00:00',1),(6,'First Name','Identifies consumer first name questions',1,'0000-00-00 00:00:00',1),(7,'Last Name','Identifies consumer last name questions',1,'0000-00-00 00:00:00',1),(8,'Address Line 1','Identifies consumer address line 1 questions',1,'0000-00-00 00:00:00',1),(10,'Provider Profile','Identifies questions used when profiling a provider',1,'0000-00-00 00:00:00',1),(12,'Provider Preference','Identifies questions used when ranking a provider based on preference',1,'2008-12-18 02:32:31',1),(16,'Address Line 2','Identifies consumer address line 2 questions',1,'2009-01-02 00:51:16',1),(17,'Address City','Identifies consumer city questions',1,'2009-01-02 00:51:23',1),(18,'Address Postcode','Identifies consumer postcode questions',1,'2009-01-02 00:51:28',1),(19,'Address Region','Identifies consumer region questions',1,'2009-01-02 00:51:34',1),(21,'Job Location','Identifies job location and/or areas serviced',1,'2009-01-12 00:32:57',1),(22,'Supplementary Charge','Identifies question which can have a supplementary charge component assigned to them',1,'2009-01-23 04:34:19',1),(23,'Base Charge','Identifies question which can have a base charge component assigned to them',1,'2009-01-23 04:34:40',1),(24,'Calendar','Indicates that a UI element contains a calendar component',1,'2009-03-23 00:18:49',1),(25,'Other','Indicates that a UI element contains a text field component where the user can enter in free text classed as \"Other\"',1,'2009-03-23 03:59:14',1),(26,'Registration End Point','Indicates which options when selected should terminate the registration process',1,'2009-03-24 00:54:55',1),(27,'Consumer Primary Location','Identifies the consumers primary job location preference. This question does not apply to a provider and should not be used in Channels where only one job location is collected. Use the \"Job location\" tag instead.',0,'2009-03-30 02:09:33',1),(28,'Consumer Supplementary Location','Identifies the consumers supplementary job location preferences. This question does not apply to a provider and should not be used in Channels where only one job location is collected. Use the \"Job location\" tag instead.',0,'2009-03-30 02:09:33',1),(29,'Feedback','Identifies questions used for the feedback of a project',1,'2009-03-11 01:36:22',1),(30,'Provider List','Identifies questions which require a dynamic provider list to be provided as options',1,'2009-03-18 22:42:25',1),(31,'Feedback End Point','Identifies questions that are end nodes',1,'2009-03-19 05:48:33',1),(32,'Feedback Return Point','Identifies the question that is a return point for the feedback questions',1,'2009-03-19 05:54:15',1),(33,'Rated Provider','Identifies the question that signifies a rated provider',1,'2009-03-26 04:24:40',1),(34,'Rating','Identifies the question that is a feedback rating',1,'2009-03-26 04:29:53',1),(35,'Provider Chosen','Identifies the questions that illustrate a provider has been chosen from the feedback questions',1,'2009-03-26 05:05:59',1),(38,'Quality','Identifies \"Quality\" rating questions',1,'2008-12-22 03:01:15',1),(39,'Phone','Identifies a phone number submitted with a Consumer Request',1,'2009-04-02 03:04:13',1),(40,'Feedback Comment','Identifies a feedback comments question',1,'2008-12-22 03:01:15',1),(41,'Mobile','Identifies a mobile number submitted with a Consumer Request',1,'2009-06-18 07:06:15',1),(42,'Preferred Contact Time','Identifies consumer\'s preferred contact time',1,'2009-07-08 07:38:16',1),(43,'Feedback Lead Reminder','When chosen by a consumer, questions with this tag will generate a Lead Reminder Alert',1,'2009-07-08 07:38:16',1),(44,'Match Partial','Identifies questions used with Lep_Match_MatchPartial match rules',1,'2009-09-18 07:49:50',1),(45,'Lead Delay','Enables assigning lead delays to a question.',1,'2010-09-21 21:29:57',1),(46,'Job Preference','Identifies questions used with the Lep_Match_JobPreference rule',1,'2011-10-12 19:42:33',NULL);
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

#Default Data for a "clean" install
INSERT INTO `client` (id,name) VALUES (1,'Default Client');

INSERT INTO `acl_role` (id,name) VALUES (1,'Full Access');
INSERT INTO `acl_role_permission` (permission,acl_resource_id,acl_role_id)
SELECT 'allow',id,1 FROM `acl_resource`;

INSERT into `user` (id,username,password,real_name) VALUES (1,'administrator','aa5af1a498a1227904e3e49dbbd0f4e60fa366c9ed9c9a0fb3036898eed506cd','Administrator');
INSERT INTO `user_role` (user_id,role) VALUES (1,'administrator');
INSERT INTO `user_role` (user_id,role) VALUES (1,'channel-manager');

INSERT INTO `application_identity` (id,username,password,real_name,client_id) VALUES (1,'lep2-default','hUI3x92is92','Default User',1);
INSERT INTO `application_identity_acl_role` (application_identity_id,acl_role_id) VALUES (1,1);
