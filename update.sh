#!/bin/bash
border()
{
        echo "***********************************************"
        return

}

clear
cd /usr/local/release-scripts
border
echo "Environment to update:"
read environment
echo "
What to update (1 - 4):
1 - code base
2 - database patch
3 - complete database
4 - all of the above"
read update
case $update in
1)
	echo "updating the code base for $environment"; border; phing development-update -Dproperties=$environment;;
2)
	echo "applying the db-patch for $environment"; border; phing development-update-dbpatch -Dproperties=$environment;;
3)
	echo "applying the complete database for $environment"; border; phing development-update-db -Dproperties=$environment;;
4)
	echo "deploying all scripts for $environment"; border; phing development-update-all -Dproperties=$environment;;
*)
	echo "incorrect option. exiting."; border;;
esac
cd ~
